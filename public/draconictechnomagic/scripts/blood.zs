import crafttweaker.util.Math;

mods.thermalexpansion.Transposer.addFillRecipe(<bloodmagic:slate:4>, <bloodmagic:slate:3>, <liquid:lifeessence> * 25000, 25000);
mods.thermalexpansion.Transposer.addFillRecipe(<bloodmagic:slate:3>, <bloodmagic:slate:2>, <liquid:lifeessence> * 15000, 15000);
mods.thermalexpansion.Transposer.addFillRecipe(<bloodmagic:slate:2>, <bloodmagic:slate:1>, <liquid:lifeessence> * 5000, 5000);
mods.thermalexpansion.Transposer.addFillRecipe(<bloodmagic:slate:1>, <bloodmagic:slate>, <liquid:lifeessence> * 2000, 2000);
mods.thermalexpansion.Transposer.addFillRecipe(<bloodmagic:slate>, <minecraft:stone>, <liquid:lifeessence> * 1000, 1000);
mods.thermalexpansion.Transposer.addFillRecipe(<bloodarsenal:base_item:2>, <minecraft:glowstone_dust>, <liquid:lifeessence> * 2500, 2500);
mods.thermalexpansion.Transposer.addFillRecipe(<bloodarsenal:base_item:4>, <bloodarsenal:base_item:3>, <liquid:lifeessence> * 5000, 5000);
mods.thermalexpansion.Transposer.addFillRecipe(<bloodarsenal:blood_diamond:2>, <bloodarsenal:blood_diamond:1>, <liquid:lifeessence> * 100000, 50000);
mods.thermalexpansion.Transposer.addFillRecipe(<bloodmagic:activation_crystal>, <bloodmagic:lava_crystal>, <liquid:lifeessence> * 10000, 10000);

#Blood Diamond
recipes.addShaped(<bloodarsenal:blood_diamond:3>, [
	[<tconstruct:ingots:4>, <tconstruct:ingots:4>, <tconstruct:ingots:4>],
	[<tconstruct:ingots:4>, <bloodarsenal:blood_diamond:2>, <tconstruct:ingots:4>],
	[<tconstruct:ingots:4>, <tconstruct:ingots:4>, <tconstruct:ingots:4>]
]);

val monster_soul = <bloodmagic:monster_soul>;
val demon_crystal = <bloodmagic:item_demon_crystal>;

recipes.addShapeless("monster_soul_to_demon_crystal", demon_crystal,
    [monster_soul.marked("soul")],

    function(out, ins, cInfo) {
        val souls = Math.max(1.0, ins.soul.tag.souls.asDouble() as double);
        return demon_crystal * Math.floor(souls);
    },
    null
);

recipes.remove(<enderio:block_powered_spawner>);



recipes.addShaped(<extrautils2:angelring:5>, [
	[null, null, null],
	[<botania:quartz>, <extrautils2:angelring>, <botania:quartz>],
	[null, null, null]
]);

recipes.addShaped(<extrautils2:angelring:4>, [
	[null, null, null],
	[<botania:quartz:6>, <extrautils2:angelring>, <botania:quartz:6>], 
	[null, null, null]
]);
recipes.addShaped(<extrautils2:angelring:3>, [
	[null, null, null],
	[<botania:quartz:4>, <extrautils2:angelring>, <botania:quartz:4>], 
	[null, null, null]
]);

recipes.addShaped(<extrautils2:angelring:2>, [
	[null, null, null],
	[<botania:quartz:3>, <extrautils2:angelring>, <botania:quartz:3>],
	[null, null, null]
]);

recipes.addShaped(<extrautils2:angelring:1>, [
	[null, null, null],
	[<botania:quartz:1>, <extrautils2:angelring>, <botania:quartz:1>],
	[null, null, null]
]);

recipes.addShaped(<industrialforegoing:conveyor>*8, [
	[null, null, null],
	[<minecraft:rail>, <minecraft:rail>, <minecraft:rail>],
	[<minecraft:iron_ingot>, <minecraft:redstone>, <minecraft:iron_ingot>]
]);


#PoweredSpawner
mods.extendedcrafting.TableCrafting.addShaped(0, <enderio:block_powered_spawner>, [
	[<ic2:crafting:4>, <ore:ingotAstralStarmetal>, <ore:gaiaIngot>, <ore:ingotAstralStarmetal>, <ic2:crafting:4>], 
	[<ore:ingotVoid>, <ore:manaDiamond>, <ore:manaPearl>, <ore:manaDiamond>, <ore:ingotVoid>], 
	[<ore:ingotVoid>, <ore:blockElectricalSteel>, <ore:itemSoulMachineChassi>, <ore:blockElectricalSteel>, <ore:ingotVoid>], 
	[<ore:blockManyullyn>, <ore:ingotManasteel>, <bloodarsenal:blood_diamond:3>, <ore:ingotManasteel>, <ore:blockManyullyn>], 
	[<ore:itemVibrantCrystal>, <ore:blockRefinedGlowstone>, <threng:material:14>, <ore:blockRefinedGlowstone>, <ore:itemVibrantCrystal>]
]);

#Polymer Clay
mods.extendedcrafting.TableCrafting.addShaped(0, <deepmoblearning:polymer_clay>*16, [
	[null, null, <ore:ingotManyullyn>, <ore:ingotManasteel>, <ore:ingotEnderium>], 
	[null, <ore:ingotManyullyn>, <ore:ingotIridium>, <ore:ingotEnderium>, <ore:ingotManasteel>], 
	[<ore:ingotManyullyn>, <ic2:nuclear:2>, <ore:ingotEnderium>, <ic2:nuclear:2>, <ore:ingotManyullyn>], 
	[<ore:ingotManasteel>, <ore:ingotEnderium>, <ore:ingotIridium>, <ore:ingotManyullyn>, null], 
	[<ore:ingotEnderium>, <ore:ingotManasteel>, <ore:ingotManyullyn>, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <minecraft:dragon_breath> * 16, [
    [null, <thaumcraft:phial>, <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "alienis"}]}), <thaumcraft:ingot>, <thaumcraft:ingot>],
    [<bloodmagic:activation_crystal>, <threng:material:6>, <bloodmagic:slate:2>, <minecraft:nether_star>, <thaumcraft:ingot>],
    [<draconicevolution:draconium_block:1>, <bloodmagic:slate:2>, <draconicevolution:draconium_block:1>, <bloodmagic:slate:2>, <draconicevolution:draconium_block:1>],
    [<thaumcraft:ingot>, <minecraft:nether_star>, <bloodmagic:slate:2>, <threng:material:6>, <bloodmagic:activation_crystal>],
    [<thaumcraft:ingot>, <thaumcraft:ingot>, <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "alienis"}]}), <thaumcraft:phial>, null]
]);
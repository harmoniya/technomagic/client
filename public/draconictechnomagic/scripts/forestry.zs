recipes.remove(<gendustry:mutatron>);
recipes.remove(<gendustry:mutatron_advanced>);
recipes.remove(<gendustry:industrial_apiary>);
recipes.remove(<gendustry:mutagen_producer>);
recipes.remove(<gendustry:imprinter>);
recipes.remove(<gendustry:sampler>);
recipes.remove(<gendustry:transposer>);
recipes.remove(<gendustry:replicator>);
recipes.remove(<gendustry:power_module>);
recipes.remove(<gendustry:genetics_processor>);
recipes.remove(<gendustry:upgrade_frame>);
recipes.remove(<gendustry:env_processor>);
recipes.remove(<gendustry:labware>);

#Mutatron
mods.extendedcrafting.TableCrafting.addShaped(0, <gendustry:mutatron>, [
	[<mekanism:polyethene:2>, <forestry:impregnated_casing>, <ic2:nuclear:3>, <forestry:impregnated_casing>, <mekanism:polyethene:2>], 
	[<forestry:hardened_machine>, <gendustry:power_module>, <gendustry:climate_module>, <gendustry:power_module>, <forestry:hardened_machine>], 
	[<ic2:nuclear:3>, <gendustry:bee_receptacle>, <bloodmagic:slate:4>, <gendustry:bee_receptacle>, <ic2:nuclear:3>], 
	[<forestry:hardened_machine>, <gendustry:power_module>, <gendustry:mutagen_tank>, <gendustry:power_module>, <forestry:hardened_machine>], 
	[<mekanism:polyethene:2>, <forestry:impregnated_casing>, <ic2:nuclear:3>, <forestry:impregnated_casing>, <mekanism:polyethene:2>]
]);

#Advanced Mutatron
mods.extendedcrafting.TableCrafting.addShaped(0, <gendustry:mutatron_advanced>, [
	[<forestry:alveary.plain>, <gendustry:env_processor>, <bloodmagic:slate:4>, <gendustry:env_processor>, <forestry:alveary.plain>], 
	[<gendustry:genetics_processor>, <mekanism:polyethene:2>, <gendustry:power_module>, <mekanism:polyethene:2>, <gendustry:genetics_processor>], 
	[<bloodmagic:slate:4>, <gendustry:bee_receptacle>, <gendustry:mutatron>, <gendustry:bee_receptacle>, <bloodmagic:slate:4>], 
	[<gendustry:genetics_processor>, <mekanism:polyethene:2>, <gendustry:power_module>, <mekanism:polyethene:2>, <gendustry:genetics_processor>], 
	[<forestry:alveary.plain>, <gendustry:env_processor>, <bloodmagic:slate:4>, <gendustry:env_processor>, <forestry:alveary.plain>]
]);

#Mutagen Producer
mods.extendedcrafting.TableCrafting.addShaped(0, <gendustry:mutagen_producer>, [
	[<mekanism:basicblock:8>, <mekanism:polyethene:2>, <forestry:impregnated_casing>, <mekanism:polyethene:2>, <mekanism:basicblock:8>], 
	[<ic2:crafting:4>, <forestry:hardened_machine>, <gendustry:genetics_processor>, <forestry:hardened_machine>, <ic2:crafting:4>], 
	[<forestry:impregnated_casing>, <gendustry:power_module>, <bloodmagic:slate:4>, <gendustry:power_module>, <forestry:impregnated_casing>], 
	[<ic2:crafting:4>, <forestry:hardened_machine>, <gendustry:genetics_processor>, <forestry:hardened_machine>, <ic2:crafting:4>], 
	[<mekanism:basicblock:8>, <mekanism:polyethene:2>, <forestry:impregnated_casing>, <mekanism:polyethene:2>, <mekanism:basicblock:8>]
]);
#Apiary
mods.extendedcrafting.TableCrafting.addShaped(0, <gendustry:industrial_apiary>, [
	[<mekanism:polyethene:2>, <forestry:alveary.plain>, <gendustry:genetics_processor>, <forestry:alveary.plain>, <mekanism:polyethene:2>], 
	[<forestry:alveary.plain>, <gendustry:power_module>, <gendustry:bee_receptacle>, <gendustry:power_module>, <forestry:alveary.plain>], 
	[<gendustry:env_processor>, <bloodmagic:slate:4>, <ore:craftingSunnarium>, <bloodmagic:slate:4>, <gendustry:env_processor>], 
	[<forestry:alveary.plain>, <gendustry:power_module>, <gendustry:bee_receptacle>, <gendustry:power_module>, <forestry:alveary.plain>], 
	[<mekanism:polyethene:2>, <forestry:alveary.plain>, <gendustry:genetics_processor>, <forestry:alveary.plain>, <mekanism:polyethene:2>]
]);

#Imprinter
mods.extendedcrafting.TableCrafting.addShaped(0, <gendustry:imprinter>, [
	[<mekanism:polyethene:2>, <bloodmagic:slate>, <gendustry:power_module>, <bloodmagic:slate>, <mekanism:polyethene:2>], 
	[<bloodmagic:slate>, <forestry:impregnated_casing>, <mekanism:polyethene:2>, <forestry:hardened_machine>, <bloodmagic:slate>], 
	[<gendustry:power_module>, <gendustry:bee_receptacle>, <gendustry:genetics_processor>, <gendustry:bee_receptacle>, <gendustry:power_module>], 
	[<bloodmagic:slate>, <forestry:hardened_machine>, <mekanism:polyethene:2>, <forestry:impregnated_casing>, <bloodmagic:slate>], 
	[<mekanism:polyethene:2>, <bloodmagic:slate>, <gendustry:power_module>, <bloodmagic:slate>, <mekanism:polyethene:2>]
]);

#Sampler
mods.extendedcrafting.TableCrafting.addShaped(0, <gendustry:sampler>, [
	[<mekanism:polyethene:2>, <bloodmagic:slate:1>, <gendustry:power_module>, <bloodmagic:slate:1>, <mekanism:polyethene:2>], 
	[<forestry:impregnated_casing>, <mekanism:polyethene:2>, <ore:gearBronze>, <mekanism:polyethene:2>, <forestry:impregnated_casing>], 
	[<gendustry:power_module>, <gendustry:bee_receptacle>, <gendustry:genetics_processor>, <ore:blockDiamond>, <gendustry:power_module>], 
	[<forestry:hardened_machine>, <mekanism:polyethene:2>, <ore:gearBronze>, <mekanism:polyethene:2>, <forestry:hardened_machine>], 
	[<mekanism:polyethene:2>, <bloodmagic:slate:1>, <gendustry:power_module>, <bloodmagic:slate:1>, <mekanism:polyethene:2>]
]);

#Translocator
mods.extendedcrafting.TableCrafting.addShaped(0, <gendustry:transposer>, [
	[<mekanism:polyethene:2>, <gendustry:power_module>, <gendustry:genetics_processor>, <gendustry:power_module>, <mekanism:polyethene:2>], 
	[<gendustry:power_module>, <mekanism:polyethene:2>, <forestry:impregnated_casing>, <mekanism:polyethene:2>, <gendustry:power_module>], 
	[<gendustry:genetics_processor>, <forestry:hardened_machine>, <mekanism:basicblock:8>, <forestry:hardened_machine>, <gendustry:genetics_processor>], 
	[<gendustry:power_module>, <mekanism:polyethene:2>, <forestry:impregnated_casing>, <mekanism:polyethene:2>, <gendustry:power_module>], 
	[<mekanism:polyethene:2>, <gendustry:power_module>, <gendustry:genetics_processor>, <gendustry:power_module>, <mekanism:polyethene:2>]
]);

#Replicator
mods.extendedcrafting.TableCrafting.addShaped(0, <gendustry:replicator>, [
	[<mekanism:polyethene:2>, <gendustry:power_module>, <forestry:hardened_machine>, <gendustry:power_module>, <mekanism:polyethene:2>], 
	[<gendustry:power_module>, <mekanism:polyethene:2>, <gendustry:bee_receptacle>, <mekanism:polyethene:2>, <gendustry:power_module>], 
	[<gendustry:genetics_processor>, <mekanism:machineblock2:11>, <mekanism:basicblock:8>, <mekanism:machineblock2:11>, <gendustry:genetics_processor>], 
	[<gendustry:power_module>, <mekanism:polyethene:2>, <gendustry:bee_receptacle>, <mekanism:polyethene:2>, <gendustry:power_module>], 
	[<mekanism:polyethene:2>, <gendustry:power_module>, <forestry:hardened_machine>, <gendustry:power_module>, <mekanism:polyethene:2>]
]);

recipes.addShaped(<gendustry:power_module>, [
    [<mekanism:polyethene:2>, <minecraft:redstone_block>, <mekanism:polyethene:2>],
    [<mekanism:polyethene:2>, <ore:battery>, <mekanism:polyethene:2>], 
    [<mekanism:polyethene:2>, <minecraft:redstone_block>, <mekanism:polyethene:2>]
]);

recipes.addShaped(<gendustry:genetics_processor>, [
    [<mekanism:polyethene:2>, <appliedenergistics2:material:23>, <mekanism:tierinstaller:3>],
    [<mekanism:controlcircuit>, <mekanism:compressedcarbon>, <mekanism:controlcircuit>], 
    [<mekanism:tierinstaller:3>, <appliedenergistics2:material:23>, <mekanism:polyethene:2>]
]);

recipes.addShaped(<gendustry:upgrade_frame>, [
    [<mekanism:polyethene:2>, <gendustry:genetics_processor>, <mekanism:polyethene:2>],
    [<appliedenergistics2:material:24>, <appliedenergistics2:material:22>, <appliedenergistics2:material:24>], 
    [<mekanism:polyethene:2>, <gendustry:env_processor>, <mekanism:polyethene:2>]
]);

recipes.addShaped(<gendustry:env_processor>, [
    [<mekanism:polyethene:2>, <appliedenergistics2:material:22>, <mekanism:tierinstaller:3>],
    [<mekanism:controlcircuit>, <mekanism:compressedcarbon>, <mekanism:controlcircuit>], 
    [<mekanism:tierinstaller:3>, <appliedenergistics2:material:22>, <mekanism:polyethene:2>]
]);

recipes.addShaped(<gendustry:labware>*16, [
    [null, <minecraft:diamond>, null],
    [null, <mekanism:crystal:4>, null], 
    [null, <mekanism:nugget:6>, null]
]);
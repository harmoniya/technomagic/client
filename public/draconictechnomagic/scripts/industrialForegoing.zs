recipes.remove(<industrialforegoing:plastic>);
recipes.remove(<industrialforegoing:dryrubber>);
recipes.remove(<industrialforegoing:laser_base>);
recipes.remove(<industrialforegoing:laser_drill>);
recipes.remove(<industrialforegoing:mob_relocator>);
recipes.remove(<industrialforegoing:water_resources_collector>);
recipes.remove(<industrialforegoing:tree_fluid_extractor>);
recipes.remove(<industrialforegoing:latex_processing_unit>);
recipes.remove(<industrialforegoing:lava_fabricator>);
recipes.remove(<industrialforegoing:plant_interactor>);
recipes.remove(<industrialforegoing:laser_drill>);
recipes.remove(<industrialforegoing:laser_base>);

#Laser Drill
mods.extendedcrafting.TableCrafting.addShaped(0, <industrialforegoing:laser_drill>, [
	[<industrialforegoing:plastic>, <ore:blockCrystallinePinkSlime>, <mekanism:polyethene:2>, <bloodarsenal:blood_diamond:3>, <mekanism:polyethene:2>, <ore:blockCrystallinePinkSlime>, <industrialforegoing:plastic>], 
	[<ore:blockCrystallinePinkSlime>, <avaritia:resource:4>, <ore:blockDraconiumAwakened>, <mekanism:polyethene:2>, <ore:blockDraconiumAwakened>, <avaritia:resource:4>, <ore:blockCrystallinePinkSlime>], 
	[<mekanism:polyethene:2>, <ore:blockDraconiumAwakened>, <ic2:mining_laser:26>, <ore:ingotOrichalcos>, <ic2:mining_laser:26>, <ore:blockDraconiumAwakened>, <mekanism:polyethene:2>], 
	[<bloodarsenal:blood_diamond:3>, <mekanism:polyethene:2>, <ore:ingotOrichalcos>, <draconicevolution:awakened_core>, <ore:ingotOrichalcos>, <mekanism:polyethene:2>, <bloodarsenal:blood_diamond:3>], 
	[<mekanism:polyethene:2>, <ore:blockDraconiumAwakened>, <ic2:mining_laser:26>, <ore:ingotOrichalcos>, <ic2:mining_laser:26>, <ore:blockDraconiumAwakened>, <mekanism:polyethene:2>], 
	[<ore:blockCrystallinePinkSlime>, <avaritia:resource:4>, <ore:blockDraconiumAwakened>, <mekanism:polyethene:2>, <ore:blockDraconiumAwakened>, <avaritia:resource:4>, <ore:blockCrystallinePinkSlime>], 
	[<industrialforegoing:plastic>, <ore:blockCrystallinePinkSlime>, <mekanism:polyethene:2>, <bloodarsenal:blood_diamond:3>, <mekanism:polyethene:2>, <ore:blockCrystallinePinkSlime>, <industrialforegoing:plastic>]
]);

#Laser Base
mods.extendedcrafting.TableCrafting.addShaped(0, <industrialforegoing:laser_base>, [
	[<bloodarsenal:blood_diamond:3>, <ore:blockStellarAlloy>, <ore:blockDraconiumAwakened>, <ore:blockCrystallinePinkSlime>, <ore:blockDraconiumAwakened>, <ore:blockCrystallinePinkSlime>, <ore:blockDraconiumAwakened>, <ore:blockStellarAlloy>, <bloodarsenal:blood_diamond:3>], 
	[<ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <bloodmagic:slate:4>, <avaritia:block_resource>, <botania:storage:1>, <avaritia:block_resource>, <bloodmagic:slate:4>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>], 
	[<ore:blockDraconiumAwakened>, <bloodmagic:slate:4>, <draconicevolution:awakened_core>, <ore:ingotKnightslime>, <forestry:crafting_material:5>, <ore:ingotKnightslime>, <draconicevolution:awakened_core>, <bloodmagic:slate:4>, <ore:blockDraconiumAwakened>], 
	[<ore:blockCrystallinePinkSlime>, <avaritia:block_resource>, <ore:ingotKnightslime>, <botania:storage:1>, <ore:ingotOrichalcos>, <botania:storage:1>, <ore:ingotKnightslime>, <avaritia:block_resource>, <ore:blockCrystallinePinkSlime>], 
	[<ore:blockDraconiumAwakened>, <botania:storage:1>, <forestry:crafting_material:5>, <ore:ingotOrichalcos>, <draconicevolution:chaotic_core>, <ore:ingotOrichalcos>, <forestry:crafting_material:5>, <botania:storage:1>, <ore:blockDraconiumAwakened>], 
	[<ore:blockCrystallinePinkSlime>, <avaritia:block_resource>, <ore:ingotKnightslime>, <botania:storage:1>, <ore:ingotOrichalcos>, <botania:storage:1>, <ore:ingotKnightslime>, <avaritia:block_resource>, <ore:blockCrystallinePinkSlime>], 
	[<ore:blockDraconiumAwakened>, <bloodmagic:slate:4>, <draconicevolution:awakened_core>, <ore:ingotKnightslime>, <forestry:crafting_material:5>, <ore:ingotKnightslime>, <draconicevolution:awakened_core>, <bloodmagic:slate:4>, <ore:blockDraconiumAwakened>], 
	[<ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <bloodmagic:slate:4>, <avaritia:block_resource>, <botania:storage:1>, <avaritia:block_resource>, <bloodmagic:slate:4>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>], 
	[<bloodarsenal:blood_diamond:3>, <ore:blockStellarAlloy>, <ore:blockDraconiumAwakened>, <ore:blockCrystallinePinkSlime>, <ore:blockDraconiumAwakened>, <ore:blockCrystallinePinkSlime>, <ore:blockDraconiumAwakened>, <ore:blockStellarAlloy>, <bloodarsenal:blood_diamond:3>]
]);

#Plastic
recipes.addShaped(<industrialforegoing:plastic>,
	([
        [<industrialforegoing:dryrubber>, <minecraft:lava_bucket>, <industrialforegoing:dryrubber>], 
        [<ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquiddeuterium", Amount: 1000}}), <mekanism:polyethene:2>, <ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquidtritium", Amount: 1000}})], 
        [<industrialforegoing:dryrubber>, <minecraft:water_bucket>, <industrialforegoing:dryrubber>]
    ])); 

#Rubber
recipes.addShaped(<industrialforegoing:dryrubber>, 
    ([
        [null, <industrialforegoing:tinydryrubber>, null], 
        [<industrialforegoing:tinydryrubber>, null, <industrialforegoing:tinydryrubber>], 
        [null, <industrialforegoing:tinydryrubber>, null]
    ]));

#Mob Crusher
recipes.addShaped(<industrialforegoing:mob_relocator>, 
    ([
        [<industrialforegoing:plastic>, <draconicevolution:grinder>, <industrialforegoing:plastic>], 
        [<minecraft:diamond_sword>, <enderio:item_material:54>, <minecraft:diamond_sword>], 
        [<thermalfoundation:material:295>, <extrautils2:spike_diamond>, <thermalfoundation:material:295>]
    ]));


recipes.addShaped(<industrialforegoing:water_resources_collector>, 
    ([
        [<industrialforegoing:plastic>, <mysticalagriculture:supremium_fishing_rod>, <industrialforegoing:plastic>], 
        [<thermalfoundation:tool.fishing_rod_diamond>, <enderio:item_material:54>, <minecraft:fishing_rod>], 
        [<thermalfoundation:material:295>, <enderio:block_alloy_endergy:3>, <thermalfoundation:material:295>]
    ]));

#Tree Fluid Extractor
recipes.addShaped(<industrialforegoing:tree_fluid_extractor>, 
    ([
        [<ic2:electric_treetap>.withTag({charge: 10000.0}), <enderio:block_alloy_endergy:3>, <ic2:electric_treetap>.withTag({charge: 10000.0})], 
        [<bigreactors:blockcyanite>, <ore:itemEnhancedMachineChassi>, <bigreactors:blockcyanite>], 
        [<thermalfoundation:material:295>, <ic2:te:45>, <thermalfoundation:material:295>]
    ]));

#Latex Process Unit
recipes.addShaped(<industrialforegoing:latex_processing_unit>, 
    ([
        [<forge:bucketfilled>.withTag({FluidName: "latex", Amount: 1000}), <enderio:block_alloy_endergy:3>, <forge:bucketfilled>.withTag({FluidName: "latex", Amount: 1000})], 
        [<enderio:block_alloy_endergy:3>, <ore:itemEnhancedMachineChassi>, <enderio:block_alloy_endergy:3>], 
        [<thermalfoundation:material:295>, <extrautils2:simpledecorative>, <thermalfoundation:material:295>]
    ]));

#Lava Fabricator
recipes.addShaped(<industrialforegoing:lava_fabricator>, 
    ([
        [<industrialforegoing:plastic>, <bigreactors:blockludicrite>, <industrialforegoing:plastic>], 
        [<minecraft:lava_bucket>, <ore:itemEnhancedMachineChassi>, <minecraft:lava_bucket>], 
        [<thermalfoundation:material:295>, <extrautils2:simpledecorative>, <thermalfoundation:material:295>]
    ]));

#Plant Interractor
recipes.addShaped(<industrialforegoing:plant_interactor>, 
    ([
        [<industrialforegoing:plastic>, <minecraft:iron_axe>, <industrialforegoing:plastic>], 
        [<minecraft:shears>, <ore:itemEnhancedMachineChassi>, <minecraft:iron_hoe>], 
        [<thermalfoundation:material:295>, <extrautils2:ingredients:12>, <thermalfoundation:material:295>]
    ]));

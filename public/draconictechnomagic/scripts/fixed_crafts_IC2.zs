/*
recipes.addShaped(null, [
	[null, null, null],
	[null, null, null],
	[null, null, null]
]);

recipes.remove(null);
*/


recipes.remove(<ic2:re_battery:26>);
recipes.remove(<ic2:cable:4>.withTag({type: 4 as byte, insulation: 1 as byte}));
recipes.remove(<ic2:cable>.withTag({type: 0 as byte, insulation: 1 as byte}));
recipes.remove(<ic2:crafting:1>);
recipes.remove(<ic2:lapotron_crystal:26>.withTag({}));
recipes.remove(<ic2:crafting:6>);
recipes.remove(<ic2:crafting:5>);
recipes.remove(<ic2:crafting:2>);



recipes.addShaped(<ic2:re_battery:26>.withTag({}), [
	[null, <ic2:cable:4>.withTag({type: 4 as byte, insulation: 1 as byte}), null],
	[<ic2:casing:6>, <minecraft:redstone>, <ic2:casing:6>],
	[<ic2:casing:6>, <minecraft:redstone>, <ic2:casing:6>]
]);

recipes.addShapeless(<ic2:cable:4>.withTag({type: 4 as byte, insulation: 1 as byte}),
	[<ic2:cable:4>.withTag({type: 4 as byte, insulation: 0 as byte}), <ic2:crafting>]
);

recipes.addShapeless(<ic2:cable>.withTag({type: 0 as byte, insulation: 1 as byte}),
	[<ic2:cable>.withTag({type: 0 as byte, insulation: 0 as byte}), <ic2:crafting>]
);

recipes.addShaped(<ic2:crafting:1>, [
	[<ic2:cable>.withTag({type: 0 as byte, insulation: 1 as byte}), <ic2:cable>.withTag({type: 0 as byte, insulation: 1 as byte}), <ic2:cable>.withTag({type: 0 as byte, insulation: 1 as byte})],
	[<minecraft:redstone>, <ore:plateIron>, <minecraft:redstone>],
	[<ic2:cable>.withTag({type: 0 as byte, insulation: 1 as byte}), <ic2:cable>.withTag({type: 0 as byte, insulation: 1 as byte}), <ic2:cable>.withTag({type: 0 as byte, insulation: 1 as byte})]
]);

recipes.addShaped(<ic2:lapotron_crystal:26>.withTag({}), [
	[<ore:dustLapis>, <ore:circuitAdvanced>, <ore:dustLapis>],
	[<ore:dustLapis>, <ore:energyCrystal>, <ore:dustLapis>],
	[<ore:dustLapis>, <ore:circuitAdvanced>, <ic2:dust:9>]
]);

recipes.addShaped(<ic2:crafting:6>, [
	[null, <ic2:casing:6>, null],
	[<ic2:crafting:5>, <minecraft:iron_ingot>, <ic2:crafting:5>],
	[null, <ic2:casing:6>, null]
]);

recipes.addShaped(<ic2:crafting:5>, [
	[<ic2:cable>.withTag({type: 0 as byte, insulation: 0 as byte}), <ic2:cable>.withTag({type: 0 as byte, insulation: 0 as byte}), <ic2:cable>.withTag({type: 0 as byte, insulation: 0 as byte})],
	[<ic2:cable>.withTag({type: 0 as byte, insulation: 0 as byte}), <minecraft:iron_ingot>, <ic2:cable>.withTag({type: 0 as byte, insulation: 0 as byte})],
	[<ic2:cable>.withTag({type: 0 as byte, insulation: 0 as byte}), <ic2:cable>.withTag({type: 0 as byte, insulation: 0 as byte}), <ic2:cable>.withTag({type: 0 as byte, insulation: 0 as byte})]
]);

recipes.addShaped(<ic2:crafting:2>, [
	[<minecraft:redstone>, <minecraft:dye:4>, <minecraft:redstone>],
	[<minecraft:glowstone_dust>, <ore:circuitBasic>, <minecraft:glowstone_dust>],
	[<minecraft:redstone>, <minecraft:dye:4>, <minecraft:redstone>]
]);


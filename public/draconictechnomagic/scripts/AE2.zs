import mods.ic2.Macerator;

recipes.remove(<appliedenergistics2:controller>);
recipes.remove(<appliedenergistics2:part:521>);
recipes.remove(<appliedenergistics2:part:522>);
recipes.remove(<appliedenergistics2:entropy_manipulator>);
recipes.remove(<appliedenergistics2:material:61>);
recipes.remove(<appliedenergistics2:material:59>);
recipes.remove(<appliedenergistics2:material:58>);
recipes.remove(<appliedenergistics2:material:60>);
recipes.remove(<appliedenergistics2:wireless_crafting_terminal>.withTag({}));

mods.ic2.Macerator.addRecipe(<appliedenergistics2:material:3>, <minecraft:quartz>);

mods.extendedcrafting.TableCrafting.addShaped(0, <appliedenergistics2:controller>, [
	[<ore:ingotRefinedObsidian>, <appliedenergistics2:smooth_sky_stone_block>, <ore:ingotRefinedGlowstone>, <appliedenergistics2:smooth_sky_stone_block>, <ore:ingotRefinedObsidian>], 
	[<appliedenergistics2:smooth_sky_stone_block>, <appliedenergistics2:material:47>, <appliedenergistics2:material:23>, <appliedenergistics2:material:47>, <appliedenergistics2:smooth_sky_stone_block>], 
	[<ore:ingotRefinedGlowstone>, <threng:material:6>, <appliedenergistics2:energy_acceptor>, <threng:material:6>, <ore:ingotRefinedGlowstone>], 
	[<appliedenergistics2:smooth_sky_stone_block>, <appliedenergistics2:material:47>, <appliedenergistics2:material:23>, <appliedenergistics2:material:47>, <appliedenergistics2:smooth_sky_stone_block>], 
	[<ore:ingotRefinedObsidian>, <appliedenergistics2:smooth_sky_stone_block>, <ore:ingotRefinedGlowstone>, <appliedenergistics2:smooth_sky_stone_block>, <ore:ingotRefinedObsidian>]
]);

recipes.addShaped(<ae2utilities:enhanced_interface_tier3>, [
	[<minecraft:iron_block>, <appliedenergistics2:material:24>, <minecraft:iron_block>],
	[<appliedenergistics2:material:44>, <ae2utilities:enhanced_interface_tier2>, <appliedenergistics2:material:43>], 
	[<minecraft:iron_block>, <appliedenergistics2:material:24>, <minecraft:iron_block>]
]);

recipes.addShaped(<ae2utilities:enhanced_interface_tier2>, [
	[<minecraft:iron_block>, <appliedenergistics2:material:23>, <minecraft:iron_block>],
	[<appliedenergistics2:material:44>, <ae2utilities:enhanced_interface_tier1>, <appliedenergistics2:material:43>],
	[<minecraft:iron_block>, <appliedenergistics2:material:23>, <minecraft:iron_block>]
]);

recipes.addShaped(<ae2utilities:enhanced_interface_tier1>, [
	[<minecraft:iron_block>, <appliedenergistics2:material:22>, <minecraft:iron_block>],
	[<appliedenergistics2:material:44>, <appliedenergistics2:interface>, <appliedenergistics2:material:43>], 
	[<minecraft:iron_block>, <appliedenergistics2:material:22>, <minecraft:iron_block>]
]);


recipes.addShaped(<extendedae:infinity_cell>.withTag({r: {Craft: 0 as byte, Cnt: 1 as long, ForgeCaps: {"customnpcs:itemscripteddata": {}}, id: "minecraft:cobblestone", Count: 1, Damage: 0 as short, Req: 0 as long}, t: "i"}), [
	[<extrautils2:compressedcobblestone:4>, <appliedenergistics2:material:47>, <extrautils2:compressedcobblestone:4>],
	[<appliedenergistics2:material:47>, <appliedenergistics2:material:39>, <appliedenergistics2:material:47>],
	[<extrautils2:compressedcobblestone:4>, <appliedenergistics2:material:47>, <extrautils2:compressedcobblestone:4>]
]);

recipes.addShaped(<extendedae:infinity_cell>.withTag({r: {Craft: 0 as byte, Cnt: 1 as long, ForgeCaps: {"customnpcs:itemscripteddata": {}}, id: "minecraft:dirt", Count: 1 as byte, Damage: 0 as short, Req: 0 as long}, t: "i"}), 
    ([
        [<extrautils2:compresseddirt:3>, <appliedenergistics2:material:47>, <extrautils2:compresseddirt:3>], 
        [<appliedenergistics2:material:47>, <extracells:storage.component>, <appliedenergistics2:material:47>], 
        [<extrautils2:compresseddirt:3>, <appliedenergistics2:material:47>, <extrautils2:compresseddirt:3>]
    ]));

recipes.addShaped(<extendedae:infinity_cell>.withTag({r: {FluidName: "water", Craft: 0 as byte, Cnt: 1 as long, Count: 0 as byte, Req: 0 as long}, t: "f"}), 
    ([
        [<extrautils2:drum:2>.withTag({Fluid: {FluidName: "water", Amount: 4096000}}), <appliedenergistics2:material:47>, <extrautils2:drum:2>.withTag({Fluid: {FluidName: "water", Amount: 4096000}})], 
        [<appliedenergistics2:material:47>, <extracells:storage.component>, <appliedenergistics2:material:47>], 
        [<extrautils2:drum:2>.withTag({Fluid: {FluidName: "water", Amount: 4096000}}), <appliedenergistics2:material:47>, <extrautils2:drum:2>.withTag({Fluid: {FluidName: "water", Amount: 4096000}})]
    ]));
	
recipes.addShaped(<extendedae:infinity_cell>.withTag({r: {FluidName: "lava", Craft: 0 as byte, Cnt: 1 as long, Count: 0 as byte, Req: 0 as long}, t: "f"}), 
    ([
        [<extrautils2:drum:2>.withTag({Fluid: {FluidName: "lava", Amount: 4096000}}), <appliedenergistics2:material:47>, <extrautils2:drum:2>.withTag({Fluid: {FluidName: "lava", Amount: 4096000}})], 
        [<appliedenergistics2:material:47>, <extracells:storage.component>, <appliedenergistics2:material:47>], 
        [<extrautils2:drum:2>.withTag({Fluid: {FluidName: "lava", Amount: 4096000}}), <appliedenergistics2:material:47>, <extrautils2:drum:2>.withTag({Fluid: {FluidName: "lava", Amount: 4096000}})]
    ]));
	

import mods.thermalexpansion.InductionSmelter;

recipes.remove(<gravisuite:vajra>);
recipes.remove(<advanced_solar_panels:machines>);
recipes.remove(<gravisuite:gravichestplate:26>);
recipes.remove(<ic2:te:61>);
recipes.remove(<ic2:te:61>);
recipes.remove(<ic2:te:63>);
recipes.remove(<ic2:te:64>);
recipes.remove(<advanced_solar_panels:crafting:13>);

#Vajra
recipes.addShaped(<gravisuite:vajra>, [
	[<appliedenergistics2:material:47>, <advanced_solar_panels:crafting:13>, <appliedenergistics2:material:47>],
	[<mekanism:teleportationcore>, <gravisuite:crafting:5>, <mekanism:teleportationcore>], 
	[<gravisuite:crafting:3>, <mekanism:atomicdisassembler>, <gravisuite:crafting:3>]
]);

#Steel
mods.thermalexpansion.InductionSmelter.addRecipe(<ic2:ingot:5>, <minecraft:iron_ingot>, <ic2:dust:2>*2, 1500, <ic2:misc_resource:5>, 5);

#MT transformer
recipes.addShaped(<advanced_solar_panels:machines>, [
	[<ic2:resource:13>, <bloodmagic:slate:4>, <ic2:resource:13>],
	[<thaumcraft:ingot>, <advanced_solar_panels:crafting:12>, <botania:manaresource:4>], 
	[<ic2:resource:13>, <ic2:te:80>, <ic2:resource:13>]
]);

#GraviSuite
mods.extendedcrafting.TableCrafting.addShaped(0, <gravisuite:gravichestplate:26>, [
	[null, <ic2:crafting:4>, null, <ic2:crafting:4>, null], 
	[<ic2:crafting:4>, <ic2:crafting:4>, <bloodmagic:slate:4>, <ic2:crafting:4>, <ic2:crafting:4>], 
	[<gravisuite:crafting:1>, <gravisuite:crafting:1>, <ic2:quantum_chestplate:26>, <gravisuite:crafting:1>, <gravisuite:crafting:1>], 
	[<ic2:crafting:4>, <gravisuite:crafting:6>, <gravisuite:ultimatelappack:26>, <gravisuite:crafting:6>, <ic2:crafting:4>], 
	[null, <gravisuite:crafting:3>, null, <gravisuite:crafting:3>, null]
]);

#MaterGenerator
mods.extendedcrafting.TableCrafting.addShaped(0, <ic2:te:61>, [
	[<ic2:crafting:4>, <ore:machineBlockAdvanced>, <ore:circuitUltimate>, <ore:machineBlockAdvanced>, <ic2:crafting:4>], 
	[<ore:machineBlockAdvanced>, <ore:plateDenseSteel>, <ore:ingotThaumium>, <ore:plateDenseSteel>, <ore:machineBlockAdvanced>], 
	[<ore:circuitUltimate>, <industrialforegoing:pink_slime_ingot>, <ore:lapotronCrystal>, <industrialforegoing:pink_slime_ingot>, <ore:circuitUltimate>], 
	[<ore:machineBlockAdvanced>, <ore:plateDenseSteel>, <ore:ingotThaumium>, <ore:plateDenseSteel>, <ore:machineBlockAdvanced>], 
	[<ic2:crafting:4>, <ore:machineBlockAdvanced>, <ore:circuitUltimate>, <ore:machineBlockAdvanced>, <ic2:crafting:4>]
]);

#Replicator
mods.extendedcrafting.TableCrafting.addShaped(0, <ic2:te:63>, [
	[<ore:machineBlockAdvanced>, <ic2:resource:11>, <ic2:crafting:4>, <ic2:resource:11>, <ore:machineBlockAdvanced>], 
	[<ore:machineBlockAdvanced>, <mekanism:polyethene:2>, <bloodmagic:slate:4>, <mekanism:polyethene:2>, <ore:machineBlockAdvanced>], 
	[<mekanism:polyethene:2>, <ic2:te:39>, <ore:circuitUltimate>, <ic2:te:39>, <mekanism:polyethene:2>], 
	[<ic2:crafting:4>, <ore:circuitUltimate>, <ic2:te:39>, <ore:circuitUltimate>, <ic2:crafting:4>], 
	[<ore:machineBlockAdvanced>, <ic2:te:79>, <ore:lapotronCrystal>, <ic2:te:79>, <ore:machineBlockAdvanced>]
]);

#Scaner
mods.extendedcrafting.TableCrafting.addShaped(0, <ic2:te:64>, [
	[<ore:machineBlockAdvanced>, <ic2:resource:11>, <ic2:glass>, <ic2:resource:11>, <ore:machineBlockAdvanced>], 
	[<ic2:resource:11>, <ic2:glass>, <ic2:te:36>, <ic2:glass>, <ic2:resource:11>], 
	[<ic2:crafting:6>, <ic2:crafting:6>, <bloodmagic:slate:2>, <ic2:crafting:6>, <ic2:crafting:6>], 
	[<mekanism:polyethene:2>, <ore:circuitUltimate>, <ore:circuitUltimate>, <ore:circuitUltimate>, <mekanism:polyethene:2>], 
	[<ore:machineBlockAdvanced>, <ic2:crafting:4>, <ic2:crafting:4>, <ic2:crafting:4>, <ore:machineBlockAdvanced>]
]);

#Quant Core
recipes.addShaped(<advanced_solar_panels:crafting:13>, [
    [<thaumcraft:ingot>, <advanced_solar_panels:crafting:5>, <thaumcraft:ingot>],
    [<enderio:item_alloy_endergy_ingot:3>, <mekanism:atomicalloy>, <enderio:item_alloy_endergy_ingot:3>], 
    [<advanced_solar_panels:crafting:5>, <enderio:item_alloy_endergy_ingot:3>, <advanced_solar_panels:crafting:5>]
]);

#########################################################
#---------------Solar Panels-Rare dirt------------------#
#########################################################

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:raredirt_sp>, [
	[<ore:machineBlockAdvanced>, <ic2:glass>, <ic2:glass>, <ic2:glass>, <ore:machineBlockAdvanced>], 
	[<ore:plateAdvancedAlloy>, <ore:blockAmber>, <ore:blockDiamond>, <ore:blockAmber>, <ore:plateAdvancedAlloy>], 
	[<ic2:crafting:4>, <advanced_solar_panels:machines:5>, <advanced_solar_panels:crafting:13>, <advanced_solar_panels:machines:5>, <ic2:crafting:4>], 
	[<ore:ingotThaumium>, <ic2:crafting:4>, <advanced_solar_panels:machines:5>, <ic2:crafting:4>, <ore:ingotThaumium>], 
	[<ore:machineBlockAdvanced>, <ore:plateAdvancedAlloy>, <ic2:crafting:4>, <ore:plateAdvancedAlloy>, <ore:machineBlockAdvanced>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:raredirt_sp:1>, [
	[<ore:machineBlockAdvanced>, <mekanism:polyethene:2>, <ore:ingotTerrasteel>, <mekanism:polyethene:2>, <ore:machineBlockAdvanced>], 
	[<ore:ingotEndSteel>, <ore:battery>, <ore:dustLithium>, <ore:battery>, <ore:ingotEndSteel>], 
	[<ic2:crafting:4>, <advanced_solar_panels:machines:5>, <advanced_solar_panels:crafting:13>, <advanced_solar_panels:machines:5>, <ic2:crafting:4>], 
	[<ore:blockBrass>, <ic2:crafting:4>, <hrm-industrial-plus:raredirt_sp>, <ic2:crafting:4>, <ore:blockBrass>], 
	[<ore:machineBlockAdvanced>, <ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquidlithium", Amount: 1000}}), <ore:dustLithium>, <ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquidlithium", Amount: 1000}}), <ore:machineBlockAdvanced>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:raredirt_sp:2>, [
	[<ore:machineBlockAdvanced>, <thermalfoundation:glass:6>, <thermalfoundation:glass:6>, <thermalfoundation:glass:6>, <ore:machineBlockAdvanced>], 
	[<ore:plateAdvancedAlloy>, <gravisuite:crafting:1>, <advanced_solar_panels:crafting:13>, <gravisuite:crafting:1>, <ore:plateAdvancedAlloy>], 
	[<ore:blockThaumium>, <advanced_solar_panels:crafting:9>, <hrm-industrial-plus:raredirt_sp:1>, <advanced_solar_panels:crafting:9>, <ore:blockThaumium>], 
	[<ore:plateAdvancedAlloy>, <extrabees:honey_comb:69>, <ic2:crafting:4>, <extrabees:honey_comb:69>, <ore:plateAdvancedAlloy>], 
	[<ore:machineBlockAdvanced>, <ic2:crafting:4>, <ore:battery>, <ic2:crafting:4>, <ore:machineBlockAdvanced>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:raredirt_sp:3>, [
	[<ore:blockBloodInfusedIron>, <bloodarsenal:blood_diamond>, <bloodmagic:blood_shard>, <bloodarsenal:blood_diamond>, <ore:blockBloodInfusedIron>], 
	[<ore:machineBlockAdvanced>, <bloodmagic:blood_shard>, <draconicevolution:wyvern_core>, <bloodmagic:blood_shard>, <ore:machineBlockAdvanced>], 
	[<ic2:crafting:4>, <bloodarsenal:blood_diamond>, <hrm-industrial-plus:raredirt_sp:2>, <bloodarsenal:blood_diamond>, <ic2:crafting:4>], 
	[<ore:machineBlockAdvanced>, <extrabees:honey_comb:71>, <advanced_solar_panels:machines:5>, <extrabees:honey_comb:71>, <ore:machineBlockAdvanced>], 
	[<ore:blockBloodInfusedIron>, <ore:blockBloodInfusedIron>, <ic2:crafting:4>, <ore:blockBloodInfusedIron>, <ore:blockBloodInfusedIron>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:raredirt_sp:4>, [
	[<ic2:crafting:4>, <ore:blockBloodInfusedIron>, <ore:ingotOrichalcos>, <advanced_solar_panels:crafting:5>, <ore:ingotOrichalcos>, <ore:blockBloodInfusedIron>, <ic2:crafting:4>], 
	[<ore:ingotOrichalcos>, <gravisuite:crafting:2>, <ic2:crafting:4>, <ore:blockBloodInfusedIron>, <ic2:crafting:4>, <gravisuite:crafting:2>, <ore:ingotOrichalcos>], 
	[<ic2:iridium_reflector>, <ic2:crafting:4>, <advanced_solar_panels:crafting:13>, <hrm-industrial-plus:raredirt_sp:3>, <advanced_solar_panels:crafting:13>, <ic2:crafting:4>, <ic2:iridium_reflector>], 
	[<extrabees:honey_comb:56>, <ore:blockLudicrite>, <draconicevolution:draconic_core>, <advanced_solar_panels:crafting:13>, <draconicevolution:draconic_core>, <ore:blockLudicrite>, <extrabees:honey_comb:56>], 
	[<ic2:iridium_reflector>, <draconicevolution:draconic_core>, <extrabees:honey_comb:27>, <hrm-industrial-plus:raredirt_sp:2>, <extrabees:honey_comb:27>, <draconicevolution:draconic_core>, <ic2:iridium_reflector>], 
	[<ore:craftingSunnarium>, <ore:ingotEnderium>, <ore:ingotTerrasteel>, <draconicevolution:wyvern_core>, <ore:ingotTerrasteel>, <ore:ingotEnderium>, <ore:craftingSunnarium>], 
	[<extrabees:honey_comb:45>, <enderio:item_capacitor_stellar>, <ore:craftingSunnarium>, <ic2:crafting:4>, <ore:craftingSunnarium>, <enderio:item_capacitor_stellar>, <extrabees:honey_comb:45>]
]);

#########################################################
#------------------Solar Panels-Mineral-----------------#
#########################################################

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:mineral_sp>, [
	[<ore:machineBlockAdvanced>, <ic2:crafting:4>, <ore:nuggetCosmicNeutronium>, <extendedcrafting:singularity:3>, <ore:nuggetCosmicNeutronium>, <ic2:crafting:4>, <ore:machineBlockAdvanced>], 
	[<ic2:crafting:4>, <enderio:item_capacitor_stellar>, <ore:blockPlatinum>, <advanced_solar_panels:crafting:5>, <ore:blockPlatinum>, <enderio:item_capacitor_stellar>, <ic2:crafting:4>], 
	[<extrabees:honey_comb:46>, <ore:nuggetCosmicNeutronium>, <advanced_solar_panels:crafting:5>, <advanced_solar_panels:machines:5>, <advanced_solar_panels:crafting:5>, <ore:nuggetCosmicNeutronium>, <extrabees:honey_comb:46>], 
	[<ore:ingotOrichalcos>, <ic2:iridium_reflector>, <advanced_solar_panels:machines:5>, <hrm-industrial-plus:raredirt_sp:2>, <advanced_solar_panels:machines:5>, <ic2:iridium_reflector>, <ore:ingotOrichalcos>], 
	[<extrabees:honey_comb:15>, <ore:nuggetCosmicNeutronium>, <advanced_solar_panels:crafting:5>, <advanced_solar_panels:machines:5>, <advanced_solar_panels:crafting:5>, <ore:nuggetCosmicNeutronium>, <extrabees:honey_comb:15>], 
	[<mekanism:tierinstaller:3>, <ic2:crafting:4>, <gravisuite:crafting:2>, <draconicevolution:draconic_core>, <gravisuite:crafting:2>, <ic2:crafting:4>, <mekanism:tierinstaller:3>], 
	[<ore:machineBlockAdvanced>, <thermaldynamics:duct_0:5>, <enderio:item_capacitor_stellar>, <gravisuite:crafting:2>, <enderio:item_capacitor_stellar>, <thermaldynamics:duct_0:5>, <ore:machineBlockAdvanced>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:mineral_sp:1>, [
	[<gravisuite:crafting:2>, <gravisuite:crafting:3>, <ore:ingotOrichalcos>, <bloodarsenal:blood_infused_glowstone>, <ore:ingotOrichalcos>, <gravisuite:crafting:3>, <gravisuite:crafting:2>], 
	[<ore:craftingSunnarium>, <extendedcrafting:singularity:4>, <enderio:item_capacitor_stellar>, <ore:ingotTerrasteel>, <enderio:item_capacitor_stellar>, <extendedcrafting:singularity:4>, <ore:craftingSunnarium>], 
	[<advanced_solar_panels:crafting:9>, <ore:blockBloodInfusedIron>, <advanced_solar_panels:crafting:13>, <hrm-industrial-plus:mineral_sp>, <advanced_solar_panels:crafting:13>, <ore:blockBloodInfusedIron>, <advanced_solar_panels:crafting:9>], 
	[<extrabees:honey_comb:16>, <draconicevolution:draconic_core>, <advanced_solar_panels:machines:5>, <extendedcrafting:singularity:1>, <advanced_solar_panels:machines:5>, <draconicevolution:draconic_core>, <extrabees:honey_comb:16>], 
	[<advanced_solar_panels:crafting:9>, <ic2:iridium_reflector>, <advanced_solar_panels:crafting:13>, <advanced_solar_panels:machines:5>, <advanced_solar_panels:crafting:13>, <ic2:iridium_reflector>, <advanced_solar_panels:crafting:9>], 
	[<extrabees:honey_comb:21>, <ore:nuggetCosmicNeutronium>, <enderio:item_capacitor_stellar>, <draconicevolution:draconic_core>, <enderio:item_capacitor_stellar>, <ore:nuggetCosmicNeutronium>, <extrabees:honey_comb:21>], 
	[<gravisuite:crafting:2>, <ic2:crafting:4>, <extrabees:honey_comb:17>, <ic2:crafting:4>, <extrabees:honey_comb:17>, <ic2:crafting:4>, <gravisuite:crafting:2>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:mineral_sp:2>, [
	[<ore:nuggetCosmicNeutronium>, <ore:blockBloodInfusedIron>, <extendedcrafting:singularity:6>, <draconicevolution:draconic_core>, <extendedcrafting:singularity:6>, <ore:blockBloodInfusedIron>, <ore:nuggetCosmicNeutronium>], 
	[<ic2:iridium_reflector>, <ore:ingotTerrasteel>, <enderio:item_capacitor_stellar>, <extrabees:honey_comb:52>, <enderio:item_capacitor_stellar>, <ore:ingotTerrasteel>, <ic2:iridium_reflector>], 
	[<ore:ingotOrichalcos>, <ore:ingotDemonicMetal>, <extrabees:honey_comb:21>, <extendedcrafting:singularity:7>, <extrabees:honey_comb:21>, <ore:ingotDemonicMetal>, <ore:ingotOrichalcos>], 
	[<ic2:crafting:4>, <extrabees:honey_comb:18>, <ore:nuggetCosmicNeutronium>, <hrm-industrial-plus:mineral_sp:1>, <ore:nuggetCosmicNeutronium>, <extrabees:honey_comb:18>, <ic2:crafting:4>], 
	[<gravisuite:crafting:2>, <advanced_solar_panels:crafting:13>, <extrabees:honey_comb:21>, <advanced_solar_panels:crafting:9>, <extrabees:honey_comb:21>, <advanced_solar_panels:crafting:13>, <gravisuite:crafting:2>], 
	[<ore:ingotOrichalcos>, <ore:ingotTerrasteel>, <draconicevolution:wyvern_core>, <extrabees:honey_comb:52>, <draconicevolution:wyvern_core>, <ore:ingotTerrasteel>, <ore:ingotOrichalcos>], 
	[<ic2:crafting:4>, <ore:blockBloodInfusedIron>, <ore:ingotTerrasteel>, <ore:nuggetCosmicNeutronium>, <ore:ingotTerrasteel>, <ore:blockBloodInfusedIron>, <ic2:crafting:4>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:mineral_sp:3>, [
	[<ore:blockThaumium>, <ore:ingotOrichalcos>, <ore:ingotOrichalcos>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:ingotOrichalcos>, <ore:ingotOrichalcos>, <ore:blockThaumium>], 
	[<ore:ingotOrichalcos>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:blockBloodInfusedIron>, <ore:essenceSupremium>, <ore:blockBloodInfusedIron>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:ingotOrichalcos>], 
	[<ore:ingotOrichalcos>, <ore:nuggetCosmicNeutronium>, <ic2:crafting:4>, <ic2:iridium_reflector>, <draconicevolution:draconic_core>, <ic2:iridium_reflector>, <ic2:crafting:4>, <ore:nuggetCosmicNeutronium>, <ore:ingotOrichalcos>], 
	[<ore:nuggetCosmicNeutronium>, <ic2:crafting:4>, <ore:ingotTerrasteel>, <draconicevolution:wyvern_core>, <ic2:iridium_reflector>, <draconicevolution:wyvern_core>, <ore:ingotTerrasteel>, <ic2:crafting:4>, <ore:nuggetCosmicNeutronium>], 
	[<ore:nuggetCosmicNeutronium>, <advanced_solar_panels:crafting:5>, <extrabees:honey_comb:81>, <enderio:item_capacitor_stellar>, <hrm-industrial-plus:mineral_sp:2>, <enderio:item_capacitor_stellar>, <extrabees:honey_comb:81>, <advanced_solar_panels:crafting:5>, <ore:nuggetCosmicNeutronium>], 
	[<ore:nuggetCosmicNeutronium>, <ic2:crafting:4>, <extendedcrafting:singularity:1>, <draconicevolution:wyvern_core>, <extendedcrafting:singularity:5>, <draconicevolution:wyvern_core>, <extendedcrafting:singularity:1>, <ic2:crafting:4>, <ore:nuggetCosmicNeutronium>], 
	[<ore:ingotOrichalcos>, <ore:nuggetCosmicNeutronium>, <advanced_solar_panels:crafting:5>, <enderio:item_capacitor_stellar>, <draconicevolution:draconic_core>, <enderio:item_capacitor_stellar>, <advanced_solar_panels:crafting:5>, <ore:nuggetCosmicNeutronium>, <ore:ingotOrichalcos>], 
	[<ore:ingotOrichalcos>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:ingotIntermedium>, <ore:blockBloodInfusedIron>, <ore:ingotIntermedium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:ingotOrichalcos>], 
	[<ore:blockThaumium>, <ore:ingotOrichalcos>, <ore:ingotOrichalcos>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:ingotOrichalcos>, <ore:ingotOrichalcos>, <ore:blockThaumium>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:mineral_sp:4>, [
	[<draconicevolution:chaos_shard:2>, <ic2:crafting:4>, <ore:ingotTerrasteel>, <gravisuite:crafting:2>, <extendedcrafting:singularity:25>, <gravisuite:crafting:2>, <ore:ingotTerrasteel>, <ic2:crafting:4>, <draconicevolution:chaos_shard:2>], 
	[<ic2:crafting:4>, <draconicevolution:draconic_core>, <extendedcrafting:singularity:23>, <ic2:iridium_reflector>, <draconicevolution:chaos_shard:1>, <ic2:iridium_reflector>, <extendedcrafting:singularity:23>, <draconicevolution:draconic_core>, <ic2:crafting:4>], 
	[<enderio:item_capacitor_stellar>, <extrabees:honey_comb:85>, <ore:blockIntermediumEssence>, <ore:nuggetCosmicNeutronium>, <bloodarsenal:blood_diamond:3>, <ore:nuggetCosmicNeutronium>, <ore:blockIntermediumEssence>, <extrabees:honey_comb:85>, <enderio:item_capacitor_stellar>], 
	[<ore:blockVoid>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <draconicevolution:wyvern_core>, <hrm-industrial-plus:mineral_sp:3>, <draconicevolution:wyvern_core>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:blockVoid>], 
	[<gravisuite:crafting:2>, <ore:ingotCosmicNeutronium>, <botania:storage:1>, <enderio:item_capacitor_stellar>, <advanced_solar_panels:crafting:13>, <enderio:item_capacitor_stellar>, <botania:storage:1>, <ore:ingotCosmicNeutronium>, <gravisuite:crafting:2>], 
	[<ore:blockVoid>, <ore:nuggetCosmicNeutronium>, <extrabotany:blockorichalcos>, <draconicevolution:wyvern_core>, <ore:blockBloodInfusedIron>, <draconicevolution:wyvern_core>, <extrabotany:blockorichalcos>, <ore:nuggetCosmicNeutronium>, <ore:blockVoid>], 
	[<advanced_solar_panels:crafting:5>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <botania:storage:1>, <draconicevolution:wyvern_core>, <botania:storage:1>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <advanced_solar_panels:crafting:5>], 
	[<ic2:crafting:4>, <ore:blockIntermedium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:blockIntermedium>, <ic2:crafting:4>], 
	[<draconicevolution:chaos_shard:2>, <ic2:crafting:4>, <advanced_solar_panels:crafting:5>, <bloodmagic:blood_shard>, <gravisuite:crafting:2>, <bloodmagic:blood_shard>, <advanced_solar_panels:crafting:5>, <ic2:crafting:4>, <draconicevolution:chaos_shard:2>]
]);


#########################################################
#------------------Solar Panels-Gods--------------------#
#########################################################
mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:gods_sp>, [
	[<ore:blockVoid>, <ic2:crafting:4>, <extrabees:honey_comb:32>, <ore:blockSupremiumEssence>, <extendedcrafting:singularity:25>, <ore:blockSupremiumEssence>, <extrabees:honey_comb:32>, <ic2:crafting:4>, <ore:blockVoid>], 
	[<ic2:crafting:4>, <bloodmagic:blood_shard>, <botania:storage:1>, <draconicevolution:chaos_shard:2>, <draconicevolution:chaos_shard:1>, <draconicevolution:chaos_shard:2>, <botania:storage:1>, <bloodmagic:blood_shard>, <ic2:crafting:4>], 
	[<advanced_solar_panels:crafting:5>, <botania:storage:1>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <botania:storage:1>, <advanced_solar_panels:crafting:5>], 
	[<ore:blockThaumium>, <gravisuite:crafting:2>, <ore:nuggetCosmicNeutronium>, <draconicevolution:wyvern_core>, <enderio:item_capacitor_stellar>, <draconicevolution:wyvern_core>, <ore:nuggetCosmicNeutronium>, <gravisuite:crafting:2>, <ore:blockThaumium>], 
	[<advanced_solar_panels:crafting:5>, <extrabotany:blockorichalcos>, <ore:ingotCosmicNeutronium>, <draconicevolution:draconic_core>, <hrm-industrial-plus:mineral_sp:2>, <draconicevolution:draconic_core>, <ore:ingotCosmicNeutronium>, <extrabotany:blockorichalcos>, <advanced_solar_panels:crafting:5>], 
	[<ore:blockThaumium>, <gravisuite:crafting:2>, <ore:nuggetCosmicNeutronium>, <draconicevolution:wyvern_core>, <enderio:item_capacitor_stellar>, <draconicevolution:wyvern_core>, <ore:nuggetCosmicNeutronium>, <gravisuite:crafting:2>, <ore:blockThaumium>], 
	[<extendedcrafting:singularity:23>, <ic2:iridium_reflector>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <ic2:iridium_reflector>, <extendedcrafting:singularity:23>], 
	[<ic2:crafting:4>, <bloodarsenal:blood_diamond:3>, <ic2:iridium_reflector>, <draconicevolution:chaos_shard:2>, <draconicevolution:chaos_shard:1>, <draconicevolution:chaos_shard:2>, <ic2:iridium_reflector>, <bloodarsenal:blood_diamond:3>, <ic2:crafting:4>], 
	[<ore:blockVoid>, <ic2:crafting:4>, <extrabees:honey_comb:36>, <ore:blockSupremiumEssence>, <bloodarsenal:blood_diamond:3>, <ore:blockSupremiumEssence>, <extrabees:honey_comb:36>, <ic2:crafting:4>, <ore:blockVoid>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:gods_sp:1>, [
	[<thaumictinkerer:ichor_block>, <draconicevolution:draconic_core>, <ore:blockPlatinum>, <ic2:iridium_reflector>, <draconicevolution:wyvern_core>, <ic2:iridium_reflector>, <ore:blockPlatinum>, <draconicevolution:draconic_core>, <thaumictinkerer:ichor_block>], 
	[<draconicevolution:chaos_shard:1>, <advanced_solar_panels:crafting:5>, <ore:ingotCosmicNeutronium>, <extendedcrafting:singularity:18>, <ore:nuggetCosmicNeutronium>, <extendedcrafting:singularity:18>, <ore:ingotCosmicNeutronium>, <advanced_solar_panels:crafting:5>, <draconicevolution:chaos_shard:1>], 
	[<advanced_solar_panels:crafting:5>, <draconicevolution:wyvern_core>, <thaumictinkerer:kamiresource:2>, <ore:nuggetCosmicNeutronium>, <extendedcrafting:singularity:32>, <ore:nuggetCosmicNeutronium>, <thaumictinkerer:kamiresource:2>, <draconicevolution:wyvern_core>, <advanced_solar_panels:crafting:5>], 
	[<ic2:crafting:4>, <enderio:item_capacitor_stellar>, <extrabotany:blockorichalcos>, <thaumictinkerer:kamiresource:2>, <ore:blockVoid>, <thaumictinkerer:kamiresource:2>, <extrabotany:blockorichalcos>, <enderio:item_capacitor_stellar>, <ic2:crafting:4>], 
	[<ore:blockPlatinum>, <draconicevolution:awakened_core>, <ore:blockVoid>, <extendedcrafting:singularity:6>, <hrm-industrial-plus:gods_sp>, <extendedcrafting:singularity:6>, <ore:blockVoid>, <draconicevolution:awakened_core>, <ore:blockPlatinum>], 
	[<gravisuite:crafting:2>, <enderio:item_capacitor_stellar>, <extrabotany:blockorichalcos>, <ore:blockThaumium>, <ore:ingotOrichalcos>, <ore:blockThaumium>, <extrabotany:blockorichalcos>, <enderio:item_capacitor_stellar>, <gravisuite:crafting:2>], 
	[<ic2:crafting:4>, <botania:storage:1>, <ore:ingotCosmicNeutronium>, <extrabees:honey_comb:85>, <extendedcrafting:singularity:35>, <extrabees:honey_comb:85>, <ore:ingotCosmicNeutronium>, <botania:storage:1>, <ic2:crafting:4>], 
	[<draconicevolution:chaos_shard:2>, <ore:blockSupremiumEssence>, <ore:ingotCosmicNeutronium>, <ore:blockSupremium>, <extendedcrafting:singularity:22>, <ore:blockSupremium>, <ore:ingotCosmicNeutronium>, <ore:blockSupremiumEssence>, <draconicevolution:chaos_shard:2>], 
	[<ic2:crafting:4>, <draconicevolution:chaos_shard:2>, <ore:blockSupremiumEssence>, <draconicevolution:chaos_shard:1>, <ore:ingotOrichalcos>, <draconicevolution:chaos_shard:1>, <ore:blockSupremiumEssence>, <draconicevolution:chaos_shard:2>, <ic2:crafting:4>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:gods_sp:2>, [
	[<ore:blockVoid>, <thaumictinkerer:ichor_block>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <thaumictinkerer:ichor_block>, <ore:blockVoid>], 
	[<thaumictinkerer:ichor_block>, <thaumictinkerer:kamiresource:2>, <advanced_solar_panels:crafting:5>, <draconicevolution:wyvern_core>, <draconicevolution:chaos_shard>, <draconicevolution:wyvern_core>, <advanced_solar_panels:crafting:5>, <thaumictinkerer:kamiresource:2>, <thaumictinkerer:ichor_block>], 
	[<ore:ingotCosmicNeutronium>, <advanced_solar_panels:crafting:5>, <enderio:item_capacitor_stellar>, <botania:storage:1>, <extendedcrafting:singularity>, <botania:storage:1>, <enderio:item_capacitor_stellar>, <advanced_solar_panels:crafting:5>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <draconicevolution:wyvern_core>, <botania:storage:1>, <extendedcrafting:singularity:26>, <hrm-industrial-plus:gods_sp:1>, <extendedcrafting:singularity:26>, <botania:storage:1>, <draconicevolution:wyvern_core>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <extendedcrafting:singularity:22>, <extrabotany:blockorichalcos>, <draconicevolution:awakened_core>, <extendedcrafting:singularity_ultimate>, <draconicevolution:awakened_core>, <extrabotany:blockorichalcos>, <extendedcrafting:singularity:22>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <draconicevolution:wyvern_core>, <botania:storage:1>, <ore:ingotOrichalcos>, <hrm-industrial-plus:raredirt_sp:4>, <ore:ingotOrichalcos>, <botania:storage:1>, <draconicevolution:wyvern_core>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <draconicevolution:chaos_shard:1>, <ore:blockSuperium>, <draconicevolution:awakened_core>, <ore:ingotOrichalcos>, <draconicevolution:awakened_core>, <ore:blockSuperium>, <draconicevolution:chaos_shard:1>, <ore:ingotCosmicNeutronium>], 
	[<thaumictinkerer:ichor_block>, <draconicevolution:draconic_energy_core>, <draconicevolution:chaos_shard:1>, <ore:blockSuperium>, <draconicevolution:chaotic_core>, <ore:blockSuperium>, <draconicevolution:chaos_shard:1>, <draconicevolution:draconic_energy_core>, <thaumictinkerer:ichor_block>], 
	[<ore:blockVoid>, <thaumictinkerer:ichor_block>, <ore:ingotCosmicNeutronium>, <draconicevolution:chaos_shard:2>, <ore:ingotTerrasteel>, <draconicevolution:chaos_shard:2>, <ore:ingotCosmicNeutronium>, <thaumictinkerer:ichor_block>, <ore:blockVoid>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:gods_sp:3>, [
	[<ore:essenceSupremium>, <thaumictinkerer:ichor_block>, <ore:blockCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <bloodarsenal:blood_diamond:3>, <ore:ingotCosmicNeutronium>, <ore:blockCosmicNeutronium>, <thaumictinkerer:ichor_block>, <ore:essenceSupremium>], 
	[<thaumictinkerer:ichor_block>, <extendedcrafting:singularity:1>, <draconicevolution:wyvern_core>, <ore:blockPlatinum>, <extendedcrafting:singularity:24>, <ore:blockPlatinum>, <draconicevolution:wyvern_core>, <extendedcrafting:singularity:1>, <thaumictinkerer:ichor_block>], 
	[<ore:ingotCosmicNeutronium>, <ic2:crafting:4>, <avaritia:resource:5>, <draconicevolution:chaos_shard>, <draconicevolution:awakened_core>, <draconicevolution:chaos_shard>, <avaritia:resource:5>, <ic2:crafting:4>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <extrabotany:blockorichalcos>, <draconicevolution:chaos_shard>, <extendedcrafting:singularity:3>, <hrm-industrial-plus:gods_sp:2>, <extendedcrafting:singularity:3>, <draconicevolution:chaos_shard>, <extrabotany:blockorichalcos>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <advanced_solar_panels:crafting:5>, <extrabotany:blockorichalcos>, <draconicevolution:awakened_core>, <avaritia:resource:5>, <draconicevolution:awakened_core>, <extrabotany:blockorichalcos>, <advanced_solar_panels:crafting:5>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <extrabotany:blockorichalcos>, <draconicevolution:chaos_shard>, <extendedcrafting:singularity:2>, <hrm-industrial-plus:mineral_sp:4>, <extendedcrafting:singularity:2>, <draconicevolution:chaos_shard>, <extrabotany:blockorichalcos>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <advanced_solar_panels:crafting:5>, <botania:storage:1>, <ore:blockSupremium>, <extendedcrafting:singularity:5>, <ore:blockSupremium>, <botania:storage:1>, <advanced_solar_panels:crafting:5>, <ore:ingotCosmicNeutronium>], 
	[<thaumictinkerer:ichor_block>, <ore:blockVoid>, <bloodarsenal:blood_diamond:3>, <botania:storage:1>, <draconicevolution:chaotic_core>, <botania:storage:1>, <bloodarsenal:blood_diamond:3>, <ore:blockVoid>, <thaumictinkerer:ichor_block>], 
	[<ore:essenceSupremium>, <thaumictinkerer:ichor_block>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <thaumictinkerer:ichor_block>, <ore:essenceSupremium>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:gods_sp:4>, [
	[<ore:blockVoid>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <draconicevolution:chaos_shard>, <ore:blockSupremium>, <draconicevolution:chaos_shard>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:blockVoid>], 
	[<ore:ingotCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockBrass>, <draconicevolution:chaotic_core>, <avaritia:resource:5>, <draconicevolution:chaotic_core>, <ore:blockBrass>, <ore:blockCosmicNeutronium>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <draconicevolution:awakened_core>, <avaritia:resource:5>, <thaumictinkerer:ichor_block>, <botania:storage:1>, <thaumictinkerer:ichor_block>, <avaritia:resource:5>, <draconicevolution:awakened_core>, <ore:ingotCosmicNeutronium>], 
	[<extrabotany:blockorichalcos>, <advanced_solar_panels:crafting:5>, <thaumictinkerer:ichor_block>, <extrabotany:blockorichalcos>, <bloodarsenal:blood_diamond:3>, <extrabotany:blockorichalcos>, <thaumictinkerer:ichor_block>, <advanced_solar_panels:crafting:5>, <extrabotany:blockorichalcos>], 
	[<draconicevolution:chaos_shard>, <avaritia:resource:5>, <botania:storage:1>, <bloodarsenal:blood_diamond:3>, <hrm-industrial-plus:gods_sp:3>, <bloodarsenal:blood_diamond:3>, <botania:storage:1>, <avaritia:resource:5>, <draconicevolution:chaos_shard>], 
	[<extrabotany:blockorichalcos>, <advanced_solar_panels:crafting:5>, <thaumictinkerer:ichor_block>, <extrabotany:blockorichalcos>, <bloodarsenal:blood_diamond:3>, <extrabotany:blockorichalcos>, <thaumictinkerer:ichor_block>, <advanced_solar_panels:crafting:5>, <extrabotany:blockorichalcos>], 
	[<ore:ingotCosmicNeutronium>, <draconicevolution:awakened_core>, <avaritia:resource:5>, <thaumictinkerer:ichor_block>, <botania:storage:1>, <thaumictinkerer:ichor_block>, <avaritia:resource:5>, <draconicevolution:awakened_core>, <ore:ingotCosmicNeutronium>], 
	[<ore:ingotCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockSupremiumEssence>, <draconicevolution:awakened_core>, <avaritia:resource:5>, <draconicevolution:awakened_core>, <ore:blockSupremiumEssence>, <ore:blockCosmicNeutronium>, <ore:ingotCosmicNeutronium>], 
	[<ore:blockVoid>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:ingotSupremium>, <draconicevolution:chaos_shard>, <ore:ingotSupremium>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <ore:blockVoid>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:gods_sp:5>, [
	[<ore:blockSupremium>, <draconicevolution:chaos_shard>, <thaumictinkerer:ichor_block>, <botania:storage:1>, <ore:blockVoid>, <botania:storage:1>, <thaumictinkerer:ichor_block>, <draconicevolution:chaos_shard>, <ore:blockSupremium>], 
	[<draconicevolution:awakened_core>, <extendedcrafting:singularity:1>, <ore:blockVoid>, <ore:blockCosmicNeutronium>, <ore:ingotInfinity>, <ore:blockCosmicNeutronium>, <ore:blockVoid>, <extendedcrafting:singularity:66>, <draconicevolution:awakened_core>], 
	[<extendedcrafting:singularity:17>, <botania:storage:1>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <draconicevolution:chaotic_core>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <botania:storage:1>, <extendedcrafting:singularity:17>], 
	[<draconicevolution:awakened_core>, <ore:blockSupremiumEssence>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <hrm-industrial-plus:gods_sp:4>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <ore:blockSupremiumEssence>, <draconicevolution:awakened_core>], 
	[<extendedcrafting:singularity:6>, <extrabotany:blockorichalcos>, <ore:ingotInfinity>, <draconicevolution:chaotic_core>, <draconicadditions:chaos_stabilizer_core>, <draconicevolution:chaotic_core>, <ore:ingotInfinity>, <extrabotany:blockorichalcos>, <extendedcrafting:singularity:6>], 
	[<draconicevolution:awakened_core>, <ore:blockSupremiumEssence>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <hrm-industrial-plus:gods_sp>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <ore:blockSupremiumEssence>, <draconicevolution:awakened_core>], 
	[<extendedcrafting:singularity:2>, <botania:storage:1>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <draconicevolution:chaotic_core>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <botania:storage:1>, <extendedcrafting:singularity:2>], 
	[<draconicevolution:awakened_core>, <extendedcrafting:singularity:66>, <ore:blockVoid>, <ore:blockCosmicNeutronium>, <ore:ingotInfinity>, <ore:blockCosmicNeutronium>, <ore:blockVoid>, <extendedcrafting:singularity:1>, <draconicevolution:awakened_core>], 
	[<ore:blockSupremium>, <draconicevolution:chaos_shard>, <thaumictinkerer:ichor_block>, <botania:storage:1>, <ore:blockVoid>, <botania:storage:1>, <thaumictinkerer:ichor_block>, <draconicevolution:chaos_shard>, <ore:blockSupremium>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <hrm-industrial-plus:gods_sp:6>, [
	[<ore:blockInfinity>, <thaumictinkerer:ichor_block>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <thaumictinkerer:ichor_block>, <ore:blockInfinity>], 
	[<thaumictinkerer:ichor_block>, <ore:ingotInfinity>, <ore:blockSupremiumEssence>, <draconicevolution:chaotic_core>, <ore:ingotInfinity>, <draconicevolution:chaotic_core>, <extendedcrafting:singularity_ultimate>, <ore:ingotInfinity>, <thaumictinkerer:ichor_block>], 
	[<ore:blockCosmicNeutronium>, <ore:blockSupremiumEssence>, <ore:ingotInfinity>, <ore:blockSupremiumEssence>, <avaritia:resource:5>, <extendedcrafting:singularity_ultimate>, <ore:ingotInfinity>, <extendedcrafting:singularity_ultimate>, <ore:blockCosmicNeutronium>], 
	[<ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockSupremiumEssence>, <avaritia:resource:5>, <draconicevolution:chaotic_core>, <avaritia:resource:5>, <extendedcrafting:singularity_ultimate>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>], 
	[<avaritia:resource:5>, <ore:ingotInfinity>, <avaritia:resource:5>, <draconicevolution:chaotic_core>, <hrm-industrial-plus:gods_sp:5>, <draconicevolution:chaotic_core>, <avaritia:resource:5>, <ore:ingotInfinity>, <avaritia:resource:5>], 
	[<ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <extrabotany:blockorichalcos>, <avaritia:resource:5>, <draconicevolution:chaotic_core>, <avaritia:resource:5>, <ore:blockSupremium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>], 
	[<ore:blockCosmicNeutronium>, <extrabotany:blockorichalcos>, <ore:ingotInfinity>, <extrabotany:blockorichalcos>, <avaritia:resource:5>, <ore:blockSupremium>, <ore:ingotInfinity>, <ore:blockSupremium>, <ore:blockCosmicNeutronium>], 
	[<thaumictinkerer:ichor_block>, <ore:ingotInfinity>, <extrabotany:blockorichalcos>, <draconicevolution:chaotic_core>, <ore:ingotInfinity>, <draconicevolution:chaotic_core>, <ore:blockSupremium>, <ore:ingotInfinity>, <thaumictinkerer:ichor_block>], 
	[<ore:blockInfinity>, <thaumictinkerer:ichor_block>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <thaumictinkerer:ichor_block>, <ore:blockInfinity>]
]);
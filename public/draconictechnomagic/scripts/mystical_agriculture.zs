import mods.thermalexpansion.Insolator;

recipes.remove(<mysticalagriculture:crafting:32>);
recipes.remove(<mysticalagriculture:master_infusion_crystal>);
recipes.remove(<mysticalagriculture:crafting:21>);
recipes.remove(<mysticalagriculture:crafting:20>);
recipes.remove(<mysticalagriculture:crafting:19>);
recipes.remove(<mysticalagriculture:crafting:18>);
recipes.remove(<mysticalagriculture:crafting:17>);
recipes.remove(<mysticalagriculture:crafting:16>);
recipes.remove(<mysticalagriculture:tier1_inferium_seeds>);

#Seeds
recipes.remove(<mysticalagriculture:guardian_seeds>);
recipes.remove(<mysticalagriculture:zombie_seeds>);
recipes.remove(<mysticalagriculture:chicken_seeds>);
recipes.remove(<mysticalagriculture:cow_seeds>);
recipes.remove(<mysticalagriculture:sheep_seeds>);
recipes.remove(<mysticalagriculture:slime_seeds>);
recipes.remove(<mysticalagriculture:pig_seeds>);
recipes.remove(<mysticalagriculture:uranium_238_seeds>);
recipes.remove(<mysticalagriculture:iridium_seeds>);

#Base Ingot
mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:crafting:32>, [
	[null, null, <ore:shardProsperity>, <ore:shardProsperity>, <ore:shardProsperity>],
	[null, <ore:shardProsperity>, <ic2:containment_plating>, <ore:ingotIridium>, <ore:shardProsperity>],
	[<ore:shardProsperity>, <ic2:containment_plating>, <bloodarsenal:base_item:3>, <ic2:containment_plating>, <ore:shardProsperity>],
	[<ore:shardProsperity>, <ore:ingotIridium>, <ic2:containment_plating>, <ore:shardProsperity>, null],
	[<ore:shardProsperity>, <ore:shardProsperity>, <ore:shardProsperity>, null, null]
]);

#Infusion Crystal
mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:master_infusion_crystal>, [
	[null, null, <ore:blockSuperiumEssence>, <ore:nuggetCosmicNeutronium>, <bloodarsenal:blood_diamond:3>, <ore:nuggetCosmicNeutronium>, <ore:blockSuperiumEssence>, null, null],
	[null, <ore:blockSuperiumEssence>, <ore:itemPollen>, <extracells:storage.component:3>, <ore:itemRubber>, <extracells:storage.component:3>, <ore:itemPollen>, <ore:blockSuperiumEssence>, null],
	[null, <ic2:crafting:38>, <ore:itemRubber>, <advanced_solar_panels:crafting:13>, <ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquidfusionfuel", Amount: 1000}}), <advanced_solar_panels:crafting:13>, <ore:itemRubber>, <ic2:crafting:38>, null],
	[null, <ore:nuggetCosmicNeutronium>, <ore:lapotronCrystal>, <ore:beeComb>, <ore:dropRoyalJelly>, <ore:beeComb>, <ore:lapotronCrystal>, <ore:nuggetCosmicNeutronium>, null],
	[null, <bloodarsenal:blood_diamond:3>, <ore:blockStellarAlloy>, <ore:dropRoyalJelly>, <extrabotany:blockorichalcos>, <ore:dropRoyalJelly>, <ore:blockStellarAlloy>, <bloodarsenal:blood_diamond:3>, null],
	[null, <ore:nuggetCosmicNeutronium>, <ore:lapotronCrystal>, <ore:beeComb>, <ore:dropRoyalJelly>, <ore:beeComb>, <ore:lapotronCrystal>, <ore:nuggetCosmicNeutronium>, null],
	[null, <ic2:crafting:38>, <ore:itemRubber>, <advanced_solar_panels:crafting:13>, <ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquidfusionfuel", Amount: 1000}}), <advanced_solar_panels:crafting:13>, <ore:itemRubber>, <ic2:crafting:38>, null],
	[null, <ore:blockSuperiumEssence>, <ore:itemPollen>, <extracells:storage.component:3>, <ore:itemRubber>, <extracells:storage.component:3>, <ore:itemPollen>, <ore:blockSuperiumEssence>, null],
	[null, null, <ore:blockSuperiumEssence>, <ore:nuggetCosmicNeutronium>, <bloodarsenal:blood_diamond:3>, <ore:nuggetCosmicNeutronium>, <ore:blockSuperiumEssence>, null, null]
]);

#Tier 5 seed
mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:crafting:21>, [
	[<ore:blockDraconiumAwakened>, <ore:blockSupremiumEssence>, <ore:blockBloodInfusedIron>, <forge:bucketfilled>.withTag({FluidName: "molten_blood_infused_iron", Amount: 1000}), <ore:blockDraconiumAwakened>, <forge:bucketfilled>.withTag({FluidName: "molten_blood_infused_iron", Amount: 1000}), <ore:blockBloodInfusedIron>, <ore:blockSupremiumEssence>, <ore:blockDraconiumAwakened>],
	[<ore:blockSupremiumEssence>, <ore:blockBloodInfusedIron>, <ore:blockDraconiumAwakened>, <bloodarsenal:blood_diamond:3>, <ore:blockSupremiumEssence>, <bloodarsenal:blood_diamond:3>, <ore:blockDraconiumAwakened>, <ore:blockBloodInfusedIron>, <ore:blockSupremiumEssence>],
	[<ore:blockBloodInfusedIron>, <ore:blockDraconiumAwakened>, <ore:blockSupremiumEssence>, <ore:blockRedstoneAlloy>, <forge:bucketfilled>.withTag({FluidName: "refined_life_essence", Amount: 1000}), <ore:blockRedstoneAlloy>, <ore:blockSupremiumEssence>, <ore:blockDraconiumAwakened>, <ore:blockBloodInfusedIron>],
	[<forge:bucketfilled>.withTag({FluidName: "molten_blood_infused_iron", Amount: 1000}), <bloodarsenal:blood_diamond:3>, <ore:blockRedstoneAlloy>, <draconicevolution:draconic_energy_core>, <ic2:nuclear:10>, <draconicevolution:draconic_energy_core>, <ore:blockRedstoneAlloy>, <bloodarsenal:blood_diamond:3>, <forge:bucketfilled>.withTag({FluidName: "molten_blood_infused_iron", Amount: 1000})],
	[<ore:blockDraconiumAwakened>, <ore:blockSupremiumEssence>, <forge:bucketfilled>.withTag({FluidName: "refined_life_essence", Amount: 1000}), <ic2:nuclear:10>, <mysticalagriculture:crafting:20>, <ic2:nuclear:10>, <forge:bucketfilled>.withTag({FluidName: "refined_life_essence", Amount: 1000}), <ore:blockSupremiumEssence>, <ore:blockDraconiumAwakened>],
	[<forge:bucketfilled>.withTag({FluidName: "molten_blood_infused_iron", Amount: 1000}), <bloodarsenal:blood_diamond:3>, <ore:blockRedstoneAlloy>, <draconicevolution:draconic_energy_core>, <ic2:nuclear:10>, <draconicevolution:draconic_energy_core>, <ore:blockRedstoneAlloy>, <bloodarsenal:blood_diamond:3>, <forge:bucketfilled>.withTag({FluidName: "molten_blood_infused_iron", Amount: 1000})],
	[<ore:blockBloodInfusedIron>, <ore:blockDraconiumAwakened>, <ore:blockSupremiumEssence>, <ore:blockRedstoneAlloy>, <forge:bucketfilled>.withTag({FluidName: "refined_life_essence", Amount: 1000}), <ore:blockRedstoneAlloy>, <ore:blockSupremiumEssence>, <ore:blockDraconiumAwakened>, <ore:blockBloodInfusedIron>],
	[<ore:blockSupremiumEssence>, <ore:blockBloodInfusedIron>, <ore:blockDraconiumAwakened>, <bloodarsenal:blood_diamond:3>, <ore:blockSupremiumEssence>, <bloodarsenal:blood_diamond:3>, <ore:blockDraconiumAwakened>, <ore:blockBloodInfusedIron>, <ore:blockSupremiumEssence>],
	[<ore:blockDraconiumAwakened>, <ore:blockSupremiumEssence>, <ore:blockBloodInfusedIron>, <forge:bucketfilled>.withTag({FluidName: "molten_blood_infused_iron", Amount: 1000}), <ore:blockDraconiumAwakened>, <forge:bucketfilled>.withTag({FluidName: "molten_blood_infused_iron", Amount: 1000}), <ore:blockBloodInfusedIron>, <ore:blockSupremiumEssence>, <ore:blockDraconiumAwakened>]
]);

#Tier 4 seed
mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:crafting:20>, [
	[<ore:blockVividAlloy>, <ore:blockSuperiumEssence>, <ore:blockCrystalMatrix>, <ore:blockBlutonium>, <ore:blockSuperiumEssence>, <ore:blockBlutonium>, <ore:blockCrystalMatrix>, <ore:blockSuperiumEssence>, <ore:blockVividAlloy>],
	[<ore:blockSuperiumEssence>, <ore:plateDenseLapis>, <ore:blockVividAlloy>, <appliedenergistics2:material:47>, <ore:blockBlutonium>, <appliedenergistics2:material:47>, <ore:blockVividAlloy>, <ore:plateDenseLapis>, <ore:blockSuperiumEssence>],
	[<ore:blockCrystalMatrix>, <ore:blockVividAlloy>, <ore:blockSuperiumEssence>, <forestry:crafting_material:5>, <forge:bucketfilled>.withTag({FluidName: "ic2hot_water", Amount: 1000}), <forestry:crafting_material:5>, <ore:blockSuperiumEssence>, <ore:blockVividAlloy>, <ore:blockCrystalMatrix>],
	[<ore:blockBlutonium>, <appliedenergistics2:material:47>, <forestry:crafting_material:5>, <forge:bucketfilled>.withTag({FluidName: "cryotheum", Amount: 1000}), <forestry:chipsets>, <forge:bucketfilled>.withTag({FluidName: "cryotheum", Amount: 1000}), <forestry:crafting_material:5>, <appliedenergistics2:material:47>, <ore:blockBlutonium>],
	[<ore:blockSuperiumEssence>, <ore:blockBlutonium>, <forge:bucketfilled>.withTag({FluidName: "ic2hot_water", Amount: 1000}), <forestry:chipsets>, <mysticalagriculture:crafting:19>, <forestry:chipsets>, <forge:bucketfilled>.withTag({FluidName: "ic2hot_water", Amount: 1000}), <ore:blockBlutonium>, <ore:blockSuperiumEssence>],
	[<ore:blockBlutonium>, <appliedenergistics2:material:47>, <forestry:crafting_material:5>, <forge:bucketfilled>.withTag({FluidName: "cryotheum", Amount: 1000}), <forestry:chipsets>, <forge:bucketfilled>.withTag({FluidName: "cryotheum", Amount: 1000}), <forestry:crafting_material:5>, <appliedenergistics2:material:47>, <ore:blockBlutonium>],
	[<ore:blockCrystalMatrix>, <ore:blockVividAlloy>, <ore:blockSuperiumEssence>, <forestry:crafting_material:5>, <forge:bucketfilled>.withTag({FluidName: "ic2hot_water", Amount: 1000}), <forestry:crafting_material:5>, <ore:blockSuperiumEssence>, <ore:blockVividAlloy>, <ore:blockCrystalMatrix>],
	[<ore:blockSuperiumEssence>, <ore:plateDenseLapis>, <ore:blockVividAlloy>, <appliedenergistics2:material:47>, <ore:blockBlutonium>, <appliedenergistics2:material:47>, <ore:blockVividAlloy>, <ore:plateDenseLapis>, <ore:blockSuperiumEssence>],
	[<ore:blockVividAlloy>, <ore:blockSuperiumEssence>, <ore:blockCrystalMatrix>, <ore:blockBlutonium>, <ore:blockSuperiumEssence>, <ore:blockBlutonium>, <ore:blockCrystalMatrix>, <ore:blockSuperiumEssence>, <ore:blockVividAlloy>]
]);

#Tier 3 seed
mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:crafting:19>, [
	[<ore:ingotDraconiumAwakened>, <ore:blockIntermediumEssence>, <ore:blockEvilMetal>, <ore:blockEndSteel>, <ore:blockEvilMetal>, <ore:blockIntermediumEssence>, <ore:ingotDraconiumAwakened>],
	[<ore:blockIntermediumEssence>, <advanced_solar_panels:crafting:2>, <ore:craftingSunnarium>, <ore:blockRefinedGlowstone>, <ore:craftingSunnarium>, <advanced_solar_panels:crafting:2>, <ore:blockIntermediumEssence>],
	[<ore:blockEvilMetal>, <ore:craftingSunnarium>, <ore:blockIntermediumEssence>, <bloodarsenal:blood_orange>, <ore:blockIntermediumEssence>, <ore:craftingSunnarium>, <ore:blockEvilMetal>],
	[<ore:blockEndSteel>, <ore:blockRefinedGlowstone>, <bloodarsenal:blood_orange>, <mysticalagriculture:crafting:18>, <bloodarsenal:blood_orange>, <ore:blockRefinedGlowstone>, <ore:blockEndSteel>],
	[<ore:blockEvilMetal>, <ore:craftingSunnarium>, <ore:blockIntermediumEssence>, <bloodarsenal:blood_orange>, <ore:blockIntermediumEssence>, <ore:craftingSunnarium>, <ore:blockEvilMetal>],
	[<ore:blockIntermediumEssence>, <advanced_solar_panels:crafting:2>, <ore:craftingSunnarium>, <ore:blockRefinedGlowstone>, <ore:craftingSunnarium>, <advanced_solar_panels:crafting:2>, <ore:blockIntermediumEssence>],
	[<ore:ingotDraconiumAwakened>, <ore:blockIntermediumEssence>, <ore:blockEvilMetal>, <ore:blockEndSteel>, <ore:blockEvilMetal>, <ore:blockIntermediumEssence>, <ore:ingotDraconiumAwakened>]
]);

#Tier 2 seed
mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:crafting:18>, [
	[<forge:bucketfilled>.withTag({FluidName: "xpjuice", Amount: 1000}), <ore:blockPulsatingIron>, <ore:slimecrystal>, <forge:bucketfilled>.withTag({FluidName: "refined_biofuel", Amount: 1000}), <ore:slimecrystal>, <ore:blockPulsatingIron>, <forge:bucketfilled>.withTag({FluidName: "xpjuice", Amount: 1000})],
	[<ore:blockPulsatingIron>, <ic2:crafting:38>, <ore:blockEnchantedMetal>, <advanced_solar_panels:crafting:5>, <ore:blockEnchantedMetal>, <ic2:crafting:38>, <ore:blockPulsatingIron>],
	[<ore:slimecrystal>, <ore:blockEnchantedMetal>, <ore:blockPrudentiumEssence>, <botania:storage:1>, <ore:blockPrudentiumEssence>, <ore:blockEnchantedMetal>, <ore:slimecrystal>],
	[<forge:bucketfilled>.withTag({FluidName: "refined_biofuel", Amount: 1000}), <advanced_solar_panels:crafting:5>, <botania:storage:1>, <mysticalagriculture:crafting:17>, <botania:storage:1>, <advanced_solar_panels:crafting:5>, <forge:bucketfilled>.withTag({FluidName: "refined_biofuel", Amount: 1000})],
	[<ore:slimecrystal>, <ore:blockEnchantedMetal>, <ore:blockPrudentiumEssence>, <botania:storage:1>, <ore:blockPrudentiumEssence>, <ore:blockEnchantedMetal>, <ore:slimecrystal>],
	[<ore:blockPulsatingIron>, <ic2:crafting:38>, <ore:blockEnchantedMetal>, <advanced_solar_panels:crafting:5>, <ore:blockEnchantedMetal>, <ic2:crafting:38>, <ore:blockPulsatingIron>],
	[<forge:bucketfilled>.withTag({FluidName: "xpjuice", Amount: 1000}), <ore:blockPulsatingIron>, <ore:slimecrystal>, <forge:bucketfilled>.withTag({FluidName: "refined_biofuel", Amount: 1000}), <ore:slimecrystal>, <ore:blockPulsatingIron>, <forge:bucketfilled>.withTag({FluidName: "xpjuice", Amount: 1000})]
]);

#Tier 1 seed
mods.extendedcrafting.TableCrafting.addShaped(0, <mysticalagriculture:crafting:17>, [
	[<forestry:crafting_material:1>, <ore:ingotEnchantedMetal>, <ore:ingotTerrasteel>, <ore:ingotEnchantedMetal>, <forestry:crafting_material:1>],
	[<advanced_solar_panels:crafting:4>, <ore:blockInferiumEssence>, <ore:blockProsperity>, <ore:blockInferiumEssence>, <advanced_solar_panels:crafting:4>],
	[<ore:ingotTerrasteel>, <ore:blockProsperity>, <ore:seedWheat>, <ore:blockProsperity>, <ore:ingotTerrasteel>],
	[<advanced_solar_panels:crafting:4>, <ore:blockInferiumEssence>, <ore:blockProsperity>, <ore:blockInferiumEssence>, <advanced_solar_panels:crafting:4>],
	[<forestry:crafting_material:1>, <ore:ingotEnchantedMetal>, <ore:ingotTerrasteel>, <ore:ingotEnchantedMetal>, <forestry:crafting_material:1>]
]);

#Seeds
/*
recipes.addShaped(,
    ([
        [, <mysticalagriculture:crafting:1>, ],
        [<mysticalagriculture:crafting:1>, <mysticalagriculture:crafting:18>, <mysticalagriculture:crafting:1>],
        [, <mysticalagriculture:crafting:1>, ]
    ]));
*/

recipes.addShaped(<mysticalagriculture:zombie_seeds>,
    ([
        [<minecraft:skull:2>, <mysticalagriculture:crafting>, <minecraft:skull:2>],
        [<mysticalagriculture:crafting>, <mysticalagriculture:crafting:17>, <mysticalagriculture:crafting>],
        [<minecraft:skull:2>, <mysticalagriculture:crafting>, <minecraft:skull:2>]
    ]));

recipes.addShaped(<mysticalagriculture:chicken_seeds>,
    ([
        [<minecraft:egg>, <mysticalagriculture:crafting:1>, <minecraft:egg>],
        [<mysticalagriculture:crafting:1>, <mysticalagriculture:crafting:18>, <mysticalagriculture:crafting:1>],
        [<minecraft:egg>, <mysticalagriculture:crafting:1>, <minecraft:egg>]
    ]));

recipes.addShaped(<mysticalagriculture:cow_seeds>,
    ([
        [<minecraft:leather>, <mysticalagriculture:crafting:1>, <minecraft:leather>],
        [<mysticalagriculture:crafting:1>, <mysticalagriculture:crafting:18>, <mysticalagriculture:crafting:1>],
        [<minecraft:leather>, <mysticalagriculture:crafting:1>, <minecraft:leather>]
    ]));

recipes.addShaped(<mysticalagriculture:sheep_seeds>,
    ([
        [<minecraft:wool>, <mysticalagriculture:crafting:1>, <minecraft:wool>],
        [<mysticalagriculture:crafting:1>, <mysticalagriculture:crafting:18>, <mysticalagriculture:crafting:1>],
        [<minecraft:wool>, <mysticalagriculture:crafting:1>, <minecraft:wool>]
    ]));

recipes.addShaped(<mysticalagriculture:pig_seeds>,
    ([
        [<minecraft:porkchop>, <mysticalagriculture:crafting:1>, <minecraft:porkchop>],
        [<mysticalagriculture:crafting:1>, <mysticalagriculture:crafting:18>, <mysticalagriculture:crafting:1>],
        [<minecraft:porkchop>, <mysticalagriculture:crafting:1>, <minecraft:porkchop>]
    ]));

recipes.addShaped(<mysticalagriculture:slime_seeds>,
    ([
        [<minecraft:slime>, <mysticalagriculture:crafting:1>, <minecraft:slime>],
        [<mysticalagriculture:crafting:1>, <mysticalagriculture:crafting:18>, <mysticalagriculture:crafting:1>],
        [<minecraft:slime>, <mysticalagriculture:crafting:1>, <minecraft:slime>]
    ]));

recipes.addShaped(<mysticalagriculture:spider_seeds>,
    ([
        [<minecraft:spider_eye>, <mysticalagriculture:crafting:2>, <minecraft:spider_eye>],
        [<mysticalagriculture:crafting:2>, <mysticalagriculture:crafting:19>, <mysticalagriculture:crafting:2>],
        [<minecraft:spider_eye>, <mysticalagriculture:crafting:2>, <minecraft:spider_eye>]
    ]));

recipes.addShaped(<mysticalagriculture:skeleton_seeds>,
    ([
        [<minecraft:skull>, <mysticalagriculture:crafting:2>, <minecraft:skull>],
        [<mysticalagriculture:crafting:2>, <mysticalagriculture:crafting:19>, <mysticalagriculture:crafting:2>],
        [<minecraft:skull>, <mysticalagriculture:crafting:2>, <minecraft:skull>]
    ]));

recipes.addShaped(<mysticalagriculture:blizz_seeds>,
    ([
        [<thermalfoundation:material:2048>, <mysticalagriculture:crafting:2>, <thermalfoundation:material:2048>],
        [<mysticalagriculture:crafting:2>, <mysticalagriculture:crafting:19>, <mysticalagriculture:crafting:2>],
        [<thermalfoundation:material:2048>, <mysticalagriculture:crafting:2>, <thermalfoundation:material:2048>]
    ]));

recipes.addShaped(<mysticalagriculture:creeper_seeds>,
    ([
        [<minecraft:skull:4>, <mysticalagriculture:crafting:2>, <minecraft:skull:4>],
        [<mysticalagriculture:crafting:2>, <mysticalagriculture:crafting:19>, <mysticalagriculture:crafting:2>],
        [<minecraft:skull:4>, <mysticalagriculture:crafting:2>, <minecraft:skull:4>]
    ]));

recipes.addShaped(<mysticalagriculture:blitz_seeds>,
    ([
        [<thermalfoundation:material:2050>, <mysticalagriculture:crafting:2>, <thermalfoundation:material:2050>],
        [<mysticalagriculture:crafting:2>, <mysticalagriculture:crafting:19>, <mysticalagriculture:crafting:2>],
        [<thermalfoundation:material:2050>, <mysticalagriculture:crafting:2>, <thermalfoundation:material:2050>]
    ]));

recipes.addShaped(<mysticalagriculture:basalz_seeds>,
    ([
        [<thermalfoundation:material:2052>, <mysticalagriculture:crafting:2>, <thermalfoundation:material:2052>],
        [<mysticalagriculture:crafting:2>, <mysticalagriculture:crafting:19>, <mysticalagriculture:crafting:2>],
        [<thermalfoundation:material:2052>, <mysticalagriculture:crafting:2>, <thermalfoundation:material:2052>]
    ]));

recipes.addShaped(<mysticalagriculture:rabbit_seeds>,
    ([
        [<minecraft:rabbit_foot>, <mysticalagriculture:crafting:2>, <minecraft:rabbit_foot>],
        [<mysticalagriculture:crafting:2>, <mysticalagriculture:crafting:19>, <mysticalagriculture:crafting:2>],
        [<minecraft:rabbit_foot>, <mysticalagriculture:crafting:2>, <minecraft:rabbit_foot>]
    ]));

recipes.addShaped(<mysticalagriculture:ghast_seeds>,
    ([
        [<minecraft:ghast_tear>, <mysticalagriculture:crafting:3>, <minecraft:ghast_tear>],
        [<mysticalagriculture:crafting:3>, <mysticalagriculture:crafting:20>, <mysticalagriculture:crafting:3>],
        [<minecraft:ghast_tear>, <mysticalagriculture:crafting:3>, <minecraft:ghast_tear>]
    ]));

recipes.addShaped(<mysticalagriculture:enderman_seeds>,
    ([
        [<enderio:block_enderman_skull>, <mysticalagriculture:crafting:3>, <enderio:block_enderman_skull>],
        [<mysticalagriculture:crafting:3>, <mysticalagriculture:crafting:20>, <mysticalagriculture:crafting:3>],
        [<enderio:block_enderman_skull>, <mysticalagriculture:crafting:3>, <enderio:block_enderman_skull>]
    ]));

recipes.addShaped(<mysticalagriculture:experience_seeds>,
    ([
        [<minecraft:experience_bottle>, <mysticalagriculture:crafting:3>, <minecraft:experience_bottle>],
        [<mysticalagriculture:crafting:3>, <mysticalagriculture:crafting:20>, <mysticalagriculture:crafting:3>],
        [<minecraft:experience_bottle>, <mysticalagriculture:crafting:3>, <minecraft:experience_bottle>]
    ]));

recipes.addShaped(<mysticalagriculture:blaze_seeds>,
    ([
        [<minecraft:blaze_rod>, <mysticalagriculture:crafting:3>, <minecraft:blaze_rod>],
        [<mysticalagriculture:crafting:3>, <mysticalagriculture:crafting:20>, <mysticalagriculture:crafting:3>],
        [<minecraft:blaze_rod>, <mysticalagriculture:crafting:3>, <minecraft:blaze_rod>]
    ]));

recipes.addShaped(<mysticalagriculture:wither_skeleton_seeds>,
    ([
        [<minecraft:skull:1>, <mysticalagriculture:crafting:4>, <minecraft:skull:1>],
        [<mysticalagriculture:crafting:4>, <mysticalagriculture:crafting:21>, <mysticalagriculture:crafting:4>],
        [<minecraft:skull:1>, <mysticalagriculture:crafting:4>, <minecraft:skull:1>]
    ]));



#Fitogen Reductor
//mods.thermalexpansion.Insolator.addRecipe(IItemStack primaryOutput, IItemStack primaryInput, IItemStack secondaryInput, int energy, @Optional IItemStack secondaryOutput, @Optional int secondaryChance, @Optional int water);

#mods.thermalexpansion.Insolator.addRecipe(seedoutput, seedinput, <thermalfoundation:fertilizer:1>, 10000, output, 100, 100);

#T1 Seeds
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:ice_seeds>, <mysticalagriculture:ice_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:ice_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:zombie_seeds>, <mysticalagriculture:zombie_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:zombie_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:water_seeds>, <mysticalagriculture:water_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:water_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:dirt_seeds>, <mysticalagriculture:dirt_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:dirt_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:stone_seeds>, <mysticalagriculture:stone_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:stone_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:nature_seeds>, <mysticalagriculture:nature_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:nature_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:wood_seeds>, <mysticalagriculture:wood_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:wood_essence>*8, 100, 100);

#T2 Seeds
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:dye_seeds>, <mysticalagriculture:dye_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:dye_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:aluminum_brass_seeds>, <mysticalagriculture:aluminum_brass_seeds>, <thermalfoundation:fertilizer:1>, 10000, <tconstruct:ingots:5>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:rubber_seeds>, <mysticalagriculture:rubber_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:rubber_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:silicon_seeds>, <mysticalagriculture:silicon_seeds>, <thermalfoundation:fertilizer:1>, 10000, <appliedenergistics2:material:5>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:aluminum_seeds>, <mysticalagriculture:aluminum_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:132>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:fire_seeds>, <mysticalagriculture:fire_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:fire_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:grains_of_infinity_seeds>, <mysticalagriculture:grains_of_infinity_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_material:20>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:sulfur_seeds>, <mysticalagriculture:sulfur_seeds>, <thermalfoundation:fertilizer:1>, 10000, <ic2:dust:16>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:copper_seeds>, <mysticalagriculture:copper_seeds>, <thermalfoundation:fertilizer:1>, 10000, <ic2:ingot:2>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:coal_seeds>, <mysticalagriculture:coal_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:coal>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:mystical_flower_seeds>, <mysticalagriculture:mystical_flower_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:mystical_flower_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:chicken_seeds>, <mysticalagriculture:chicken_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:chicken_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:cow_seeds>, <mysticalagriculture:cow_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:cow_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:pig_seeds>, <mysticalagriculture:pig_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:pig_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:sheep_seeds>, <mysticalagriculture:sheep_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:sheep_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:slime_seeds>, <mysticalagriculture:slime_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:slime_ball>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:nether_seeds>, <mysticalagriculture:nether_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:nether_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:marble_seeds>, <mysticalagriculture:marble_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:marble_essence>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:apatite_seeds>, <mysticalagriculture:apatite_seeds>, <thermalfoundation:fertilizer:1>, 10000, <forestry:apatite>*8, 100, 100);

#T3 Seeds
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:graphite_seeds>, <mysticalagriculture:graphite_seeds>, <thermalfoundation:fertilizer:1>, 10000, <bigreactors:ingotgraphite>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:conductive_iron_seeds>, <mysticalagriculture:conductive_iron_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_alloy_ingot:4>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:spider_seeds>, <mysticalagriculture:spider_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:spider_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:nether_quartz_seeds>, <mysticalagriculture:nether_quartz_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:quartz>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:manasteel_seeds>, <mysticalagriculture:manasteel_seeds>, <thermalfoundation:fertilizer:1>, 10000, <botania:manaresource>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:skeleton_seeds>, <mysticalagriculture:skeleton_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:skeleton_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:glowstone_seeds>, <mysticalagriculture:glowstone_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:glowstone>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:tin_seeds>, <mysticalagriculture:tin_seeds>, <thermalfoundation:fertilizer:1>, 10000, <ic2:ingot:6>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:bronze_seeds>, <mysticalagriculture:bronze_seeds>, <thermalfoundation:fertilizer:1>, 10000, <forestry:ingot_bronze>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:blizz_seeds>, <mysticalagriculture:blizz_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:2048>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:knightslime_seeds>, <mysticalagriculture:knightslime_seeds>, <thermalfoundation:fertilizer:1>, 10000, <tconstruct:ingots:3>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:saltpeter_seeds>, <mysticalagriculture:saltpeter_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:772>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:creeper_seeds>, <mysticalagriculture:creeper_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:creeper_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:certus_quartz_seeds>, <mysticalagriculture:certus_quartz_seeds>, <thermalfoundation:fertilizer:1>, 10000, <appliedenergistics2:material>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:electrical_steel_seeds>, <mysticalagriculture:electrical_steel_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_alloy_ingot>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:aquamarine_seeds>, <mysticalagriculture:aquamarine_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:aquamarine_essence>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:lead_seeds>, <mysticalagriculture:lead_seeds>, <thermalfoundation:fertilizer:1>, 10000, <ic2:ingot:3>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:silver_seeds>, <mysticalagriculture:silver_seeds>, <thermalfoundation:fertilizer:1>, 10000, <ic2:ingot:4>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:blitz_seeds>, <mysticalagriculture:blitz_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:2050>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:basalz_seeds>, <mysticalagriculture:basalz_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:2052>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:redstone_seeds>, <mysticalagriculture:redstone_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:redstone_ore>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:iron_seeds>, <mysticalagriculture:iron_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:iron_ingot>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:ardite_seeds>, <mysticalagriculture:ardite_seeds>, <thermalfoundation:fertilizer:1>, 10000, <tconstruct:ingots:1>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:rabbit_seeds>, <mysticalagriculture:rabbit_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:rabbit_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:redstone_alloy_seeds>, <mysticalagriculture:redstone_alloy_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_alloy_ingot:3>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:sky_stone_seeds>, <mysticalagriculture:sky_stone_seeds>, <thermalfoundation:fertilizer:1>, 10000, <appliedenergistics2:sky_stone_block>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:obsidian_seeds>, <mysticalagriculture:obsidian_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:obsidian>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:thaumium_seeds>, <mysticalagriculture:thaumium_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thaumcraft:ingot>*2, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:quicksilver_seeds>, <mysticalagriculture:quicksilver_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thaumcraft:quicksilver>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:brass_seeds>, <mysticalagriculture:brass_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thaumcraft:ingot:2>*4, 100, 100);

#T4 Seeds
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:elementium_seeds>, <mysticalagriculture:elementium_seeds>, <thermalfoundation:fertilizer:1>, 10000, <botania:manaresource:7>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:nickel_seeds>, <mysticalagriculture:nickel_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:133>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:cobalt_seeds>, <mysticalagriculture:cobalt_seeds>, <thermalfoundation:fertilizer:1>, 10000, <tconstruct:ingots>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:energetic_alloy_seeds>, <mysticalagriculture:energetic_alloy_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_alloy_ingot:1>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:lumium_seeds>, <mysticalagriculture:lumium_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:166>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:constantan_seeds>, <mysticalagriculture:constantan_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:164>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:end_seeds>, <mysticalagriculture:end_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:end_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:electrum_seeds>, <mysticalagriculture:electrum_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:161>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:gold_seeds>, <mysticalagriculture:gold_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:gold_ingot>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:osmium_seeds>, <mysticalagriculture:osmium_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mekanism:ingot:1>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:ghast_seeds>, <mysticalagriculture:ghast_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:ghast_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:enderman_seeds>, <mysticalagriculture:enderman_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:enderman_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:invar_seeds>, <mysticalagriculture:invar_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:162>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:dark_steel_seeds>, <mysticalagriculture:dark_steel_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_alloy_ingot:6>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:glowstone_ingot_seeds>, <mysticalagriculture:glowstone_ingot_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mekanism:ingot:3>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:experience_seeds>, <mysticalagriculture:experience_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mysticalagriculture:experience_essence>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:lapis_lazuli_seeds>, <mysticalagriculture:lapis_lazuli_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:dye:4>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:soularium_seeds>, <mysticalagriculture:soularium_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_alloy_ingot:7>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:steel_seeds>, <mysticalagriculture:steel_seeds>, <thermalfoundation:fertilizer:1>, 10000, <ic2:ingot:5>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:blaze_seeds>, <mysticalagriculture:blaze_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:blaze_rod>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:signalum_seeds>, <mysticalagriculture:signalum_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:165>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:mithril_seeds>, <mysticalagriculture:mithril_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:136>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:fluix_seeds>, <mysticalagriculture:fluix_seeds>, <thermalfoundation:fertilizer:1>, 10000, <appliedenergistics2:material:7>*12, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:pulsating_iron_seeds>, <mysticalagriculture:pulsating_iron_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_alloy_ingot:5>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:void_metal_seeds>, <mysticalagriculture:void_metal_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thaumcraft:ingot:1>*2, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:amber_seeds>, <mysticalagriculture:amber_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thaumcraft:amber>*4, 100, 100);

#T5 Seeds
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:enderium_seeds>, <mysticalagriculture:enderium_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:167>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:emerald_seeds>, <mysticalagriculture:emerald_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:emerald>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:manyullyn_seeds>, <mysticalagriculture:manyullyn_seeds>, <thermalfoundation:fertilizer:1>, 10000, <tconstruct:ingots:2>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:terrasteel_seeds>, <mysticalagriculture:terrasteel_seeds>, <thermalfoundation:fertilizer:1>, 10000, <botania:manaresource:4>, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:draconium_seeds>, <mysticalagriculture:draconium_seeds>, <thermalfoundation:fertilizer:1>, 10000, <draconicevolution:draconium_ingot>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:refined_obsidian_seeds>, <mysticalagriculture:refined_obsidian_seeds>, <thermalfoundation:fertilizer:1>, 10000, <mekanism:ingot>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:platinum_seeds>, <mysticalagriculture:platinum_seeds>, <thermalfoundation:fertilizer:1>, 10000, <thermalfoundation:material:134>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:wither_skeleton_seeds>, <mysticalagriculture:wither_skeleton_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:skull:1>, 25, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:end_steel_seeds>, <mysticalagriculture:end_steel_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_alloy_ingot:8>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:yellorium_seeds>, <mysticalagriculture:yellorium_seeds>, <thermalfoundation:fertilizer:1>, 10000, <bigreactors:ingotyellorium>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:rock_crystal_seeds>, <mysticalagriculture:rock_crystal_seeds>, <thermalfoundation:fertilizer:1>, 10000, <astralsorcery:blockcustomore>, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:uranium_seeds>, <mysticalagriculture:uranium_seeds>, <thermalfoundation:fertilizer:1>, 10000, <ic2:ingot:8>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:starmetal_seeds>, <mysticalagriculture:starmetal_seeds>, <thermalfoundation:fertilizer:1>, 10000, <astralsorcery:itemcraftingcomponent:1>, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:diamond_seeds>, <mysticalagriculture:diamond_seeds>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:diamond>*4, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<mysticalagriculture:vibrant_alloy_seeds>, <mysticalagriculture:vibrant_alloy_seeds>, <thermalfoundation:fertilizer:1>, 10000, <enderio:item_alloy_ingot:2>*4, 100, 100);

#Other seeds
mods.thermalexpansion.Insolator.addRecipe(<thaumicbases:plaxseed>, <thaumicbases:plaxseed>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:string>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<thaumicbases:sweedseed>, <thaumicbases:sweedseed>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:sugar>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<thaumicbases:metalleatseed>, <thaumicbases:metalleatseed>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:iron_nugget>*16, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<thaumicbases:lazulliaseed>, <thaumicbases:lazulliaseed>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:dye:4>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<thaumicbases:glieoniaseed>, <thaumicbases:glieoniaseed>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:glowstone_dust>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<thaumicbases:voidseed>, <thaumicbases:voidseed>, <thermalfoundation:fertilizer:1>, 10000, <thaumcraft:void_seed>*2, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<thaumicbases:briar>, <thaumicbases:briar>, <thermalfoundation:fertilizer:1>, 10000, <thaumicbases:briar_seedbag>*2, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<thaumicbases:redlonseed>, <thaumicbases:redlonseed>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:redstone>*8, 100, 100);
mods.thermalexpansion.Insolator.addRecipe(<thaumicbases:lucriteseed>, <thaumicbases:lucriteseed>, <thermalfoundation:fertilizer:1>, 10000, <minecraft:gold_nugget>*16, 100, 100);
recipes.remove(<draconicevolution:draconic_core>);
recipes.remove(<draconicevolution:wyvern_core>);
recipes.remove(<draconicevolution:wyvern_energy_core>);
recipes.remove(<draconicevolution:draconic_energy_core>);
recipes.remove(<draconicevolution:wyvern_shovel>);
recipes.remove(<draconicevolution:wyvern_axe>);
recipes.remove(<draconicevolution:wyvern_pick>);
recipes.remove(<draconicevolution:wyvern_sword>);
recipes.remove(<draconicevolution:magnet>);
recipes.remove(<draconicevolution:magnet:1>);

#DracSword
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_sword>, [
	[null, null, null, null, null, null, null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>], 
	[null, null, null, null, null, null, <ore:ingotDraconiumAwakened>, <botania:starsword>, <ore:ingotDraconiumAwakened>], 
	[null, null, null, null, null, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, null], 
	[null, null, null, null, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, null, null], 
	[null, <draconicevolution:draconic_energy_core>, null, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, null, null, null], 
	[null, null, <ore:blockDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, null, null, null, null], 
	[null, null, <draconicevolution:awakened_core>, <ore:blockDraconiumAwakened>, null, null, null, null, null], 
	[null, <draconicevolution:wyvern_sword>, null, null, <draconicevolution:draconic_energy_core>, null, null, null, null], 
	[<draconicevolution:awakened_core>, null, null, null, null, null, null, null, null]
]);

#DracAxe
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_axe>, [
	[null, null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null, null, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <draconicevolution:draconic_energy_core>, <ore:ingotDraconiumAwakened>, null, null, null], 
	[<ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:blockDraconiumAwakened>, <draconicevolution:wyvern_axe>, <ore:blockDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null, null, null], 
	[null, <ore:ingotDraconiumAwakened>, <draconicevolution:draconic_energy_core>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null, null, null], 
	[null, null, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, null, null, null, null], 
	[null, null, null, <ore:ingotDraconiumAwakened>, null, <draconicevolution:awakened_core>, null, null, null], 
	[null, null, null, null, null, null, <ore:ingotOrichalcos>, null, null], 
	[null, null, null, null, null, null, null, <ore:ingotOrichalcos>, null], 
	[null, null, null, null, null, null, null, null, <draconicevolution:awakened_core>]
]);

#DracShowel
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_shovel>, [
	[null, null, null, null, null, null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>], 
	[null, null, null, null, null, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:blockDraconiumAwakened>, <ore:ingotDraconiumAwakened>], 
	[null, null, null, null, <ore:ingotDraconiumAwakened>, <draconicevolution:draconic_energy_core>, <draconicevolution:wyvern_shovel>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>], 
	[null, null, null, null, null, <ore:ingotDraconiumAwakened>, <draconicevolution:draconic_energy_core>, <ore:ingotDraconiumAwakened>, null], 
	[null, null, null, null, <draconicevolution:awakened_core>, null, <ore:ingotDraconiumAwakened>, null, null], 
	[null, null, null, <ore:blockDraconiumAwakened>, null, null, null, null, null], 
	[null, null, <ore:blockDraconiumAwakened>, null, null, null, null, null, null], 
	[null, <ore:blockDraconiumAwakened>, null, null, null, null, null, null, null], 
	[<draconicevolution:awakened_core>, null, null, null, null, null, null, null, null]
]);

#DracPickaxe
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_pick>, [
	[null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null], 
	[<ore:ingotDraconiumAwakened>, <draconicevolution:draconic_energy_core>, <ore:ingotOrichalcos>, <ore:ingotOrichalcos>, <draconicevolution:wyvern_pick>, <ore:ingotOrichalcos>, <ore:ingotOrichalcos>, <draconicevolution:draconic_energy_core>, <ore:ingotDraconiumAwakened>], 
	[<ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>], 
	[<ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null, null, <ore:blockDraconiumAwakened>, null, null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>], 
	[null, null, null, null, <draconicevolution:awakened_core>, null, null, null, null], 
	[null, null, null, null, <ore:blockDraconiumAwakened>, null, null, null, null], 
	[null, null, null, null, <ore:blockDraconiumAwakened>, null, null, null, null], 
	[null, null, null, null, <ore:blockDraconiumAwakened>, null, null, null, null], 
	[null, null, null, null, <draconicevolution:awakened_core>, null, null, null, null]
]);

#PowerStaff
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_staff_of_power>, [
	[null, null, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, null, null], 
	[null, <ore:blockDraconiumAwakened>, <draconicevolution:draconic_axe>, <extrabotany:blockorichalcos>, <draconicevolution:draconic_pick>, <extrabotany:blockorichalcos>, <draconicevolution:draconic_shovel>, <ore:blockDraconiumAwakened>, null], 
	[<ore:blockDraconiumAwakened>, <ore:blockStellarAlloy>, <draconicevolution:awakened_core>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <draconicevolution:awakened_core>, <ore:blockStellarAlloy>, <ore:blockDraconiumAwakened>], 
	[<ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, null, <draconicevolution:draconic_energy_core>, null, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>], 
	[null, null, null, null, <ore:blockDraconiumAwakened>, null, null, null, null], 
	[null, null, null, null, <ore:blockDraconiumAwakened>, null, null, null, null], 
	[null, null, null, null, <draconicevolution:awakened_core>, null, null, null, null], 
	[null, null, null, null, <draconicevolution:draconic_sword>, null, null, null, null], 
	[null, null, null, null, <draconicevolution:draconic_energy_core>, null, null, null, null]
]);

#WywernSword
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:wyvern_sword>, [
	[null, null, null, null, null, <draconicevolution:draconium_block:1>, <draconicevolution:wyvern_energy_core>], 
	[null, null, null, null, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>], 
	[null, null, null, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, null], 
	[null, <ore:blockDraconium>, <draconicevolution:draconium_block:1>, <draconicevolution:wyvern_energy_core>, <draconicevolution:draconium_block:1>, null, null], 
	[null, null, <draconicevolution:wyvern_core>, <draconicevolution:draconium_block:1>, null, null, null], 
	[null, <bloodmagic:bound_sword>, null, <ore:blockDraconium>, null, null, null], 
	[<draconicevolution:wyvern_core>, null, null, null, null, null, null]
]);

#WywernPickaxe
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:wyvern_pick>, [
	[null, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <bloodmagic:bound_pickaxe>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, null], 
	[<draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <draconicevolution:wyvern_energy_core>, <draconicevolution:draconium_block:1>, <draconicevolution:wyvern_energy_core>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>], 
	[null, null, null, <draconicevolution:wyvern_core>, null, null, null], 
	[null, null, null, <ore:blockDraconium>, null, null, null], 
	[null, null, null, <ore:blockDraconium>, null, null, null], 
	[null, null, null, <ore:blockDraconium>, null, null, null], 
	[null, null, null, <draconicevolution:wyvern_core>, null, null, null]
]);

#WywernAxe
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:wyvern_axe>, [
	[null, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, null, null, null], 
	[<draconicevolution:draconium_block:1>, <draconicevolution:wyvern_energy_core>, <bloodmagic:bound_axe>, <draconicevolution:draconium_block:1>, null, null, null], 
	[null, <draconicevolution:draconium_block:1>, <draconicevolution:wyvern_energy_core>, <draconicevolution:draconium_block:1>, null, null, null], 
	[null, null, <draconicevolution:draconium_block:1>, <draconicevolution:wyvern_core>, null, null, null], 
	[null, null, null, null, <ore:blockDraconium>, null, null], 
	[null, null, null, null, null, <ore:blockDraconium>, null], 
	[null, null, null, null, null, null, <draconicevolution:wyvern_core>]
]);

#WywernShowel
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:wyvern_shovel>, [
	[null, null, null, null, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>], 
	[null, null, null, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>], 
	[null, null, null, <draconicevolution:wyvern_energy_core>, <bloodmagic:bound_shovel>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>], 
	[null, null, null, <draconicevolution:wyvern_core>, <draconicevolution:wyvern_energy_core>, <draconicevolution:draconium_block:1>, null], 
	[null, null, <ore:blockDraconium>, null, null, null, null], 
	[null, <ore:blockDraconium>, null, null, null, null, null], 
	[<draconicevolution:wyvern_core>, null, null, null, null, null, null]
]);

#ChaosTinyCrystal
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:chaos_shard:2>, [
	[null, null, null, null, null, <ore:ingotRefinedObsidian>, <ore:ingotLumium>], 
	[null, null, <ore:nuggetNetherStar>, <ore:ingotCrystallineAlloy>, <ore:ingotBronze>, <ore:ingotElectrum>, <ore:ingotPigiron>], 
	[null, null, <ore:gemDiamond>, <ore:craftingSunnarium>, <ore:ingotRefinedGlowstone>, <ore:plateCarbon>, null], 
	[null, <ore:ingotPlutonium>, <ore:ingotVoid>, <ore:blockDraconium>, <ore:gemIridium>, <ore:dropRoyalJelly>, null], 
	[null, <ore:plateAdvancedAlloy>, <ore:blockCoal>, <ore:ingotTerrasteel>, <ore:ingotEvilMetal>, <ore:ingotSignalum>, null], 
	[<ore:ingotConstantan>, <ore:gemEmerald>, <ore:itemInfinityGoop>, <ore:ingotInvar>, null, null, null], 
	[<ore:ingotThaumium>, <ore:ingotBrass>, null, null, null, null, null]
]);

#DracHeart
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:dragon_heart>, [
	[null, null, null, null, null, null, null, null, null], 
	[null, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, null, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, <draconicevolution:draconium_block:1>, null], 
	[<draconicevolution:draconium_block:1>, <ore:blockSignalum>, <ore:blockSignalum>, <ore:blockSignalum>, <draconicevolution:draconium_block:1>, <ore:blockSignalum>, <ore:blockSignalum>, <ore:blockSignalum>, <draconicevolution:draconium_block:1>], 
	[<draconicevolution:draconium_block:1>, <ore:blockSignalum>, <ore:ingotManyullyn>, <ore:ingotManyullyn>, <ore:blockSignalum>, <ore:ingotManyullyn>, <ore:ingotManyullyn>, <ore:blockSignalum>, <draconicevolution:draconium_block:1>], 
	[<draconicevolution:draconium_block:1>, <ore:blockSignalum>, <ore:ingotManyullyn>, <tconstruct:materials:15>, <ore:ingotManyullyn>, <tconstruct:materials:15>, <ore:ingotManyullyn>, <ore:blockSignalum>, <draconicevolution:draconium_block:1>], 
	[null, <draconicevolution:draconium_block:1>, <ore:blockSignalum>, <ore:ingotManyullyn>, <tconstruct:materials:15>, <ore:ingotManyullyn>, <ore:blockSignalum>, <draconicevolution:draconium_block:1>, null], 
	[null, null, <draconicevolution:draconium_block:1>, <ore:blockSignalum>, <ore:ingotManyullyn>, <ore:blockSignalum>, <draconicevolution:draconium_block:1>, null, null], 
	[null, null, null, <draconicevolution:draconium_block:1>, <ore:blockSignalum>, <draconicevolution:draconium_block:1>, null, null, null], 
	[null, null, null, null, <draconicevolution:draconium_block:1>, null, null, null, null]
]);

#DracEnergyCore
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_energy_core>, [
	[null, null, null, null, null, null, null, null, null], 
	[null, null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:gaiaIngot>, <ore:blockEnderium>, <botania:storage:1>, <ore:blockEnderium>, <ore:gaiaIngot>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:blockEnderium>, <draconicevolution:wyvern_core>, <ore:blockVoid>, <draconicevolution:draconic_core>, <ore:blockEnderium>, <ore:ingotDraconiumAwakened>, null], 
	[null, null, <botania:storage:1>, <ore:blockVoid>, <draconicevolution:wyvern_energy_core>, <ore:blockVoid>, <botania:storage:1>, null, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:blockEnderium>, <draconicevolution:draconic_core>, <ore:blockVoid>, <draconicevolution:wyvern_core>, <ore:blockEnderium>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:gaiaIngot>, <ore:blockEnderium>, <botania:storage:1>, <ore:blockEnderium>, <ore:gaiaIngot>, <ore:ingotDraconiumAwakened>, null], 
	[null, null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null, null], 
	[null, null, null, null, null, null, null, null, null]
]);

#Wywern EnergyCore
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:wyvern_energy_core>, [
	[<draconicevolution:draconium_block:1>, <ore:blockElectrum>, <botania:storage:4>, <ore:blockElectrum>, <draconicevolution:draconium_block:1>], 
	[<ore:blockElectrum>, <bloodmagic:slate:2>, <ore:blockSignalum>, <bloodmagic:slate:2>, <ore:blockElectrum>], 
	[<botania:storage:4>, <ore:blockSignalum>, <draconicevolution:draconic_core>, <ore:blockSignalum>, <botania:storage:4>], 
	[<ore:blockElectrum>, <bloodmagic:slate:2>, <ore:blockSignalum>, <bloodmagic:slate:2>, <ore:blockElectrum>], 
	[<draconicevolution:draconium_block:1>, <ore:blockElectrum>, <botania:storage:4>, <ore:blockElectrum>, <draconicevolution:draconium_block:1>]
]);

#ChaosCore
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:chaotic_core>, [
	[<draconicevolution:chaos_shard:1>, <draconicevolution:chaos_shard:1>, null, null, null, null, null, <draconicevolution:chaos_shard:1>, <draconicevolution:chaos_shard:1>], 
	[<draconicevolution:chaos_shard:1>, <ore:blockEvilMetal>, <draconicevolution:chaos_shard:2>, null, null, null, <draconicevolution:chaos_shard:2>, <ore:blockEvilMetal>, <draconicevolution:chaos_shard:1>], 
	[null, <draconicevolution:chaos_shard:2>, <draconicevolution:awakened_core>, <ore:nuggetCosmicNeutronium>, <ic2:crafting:38>, <ore:nuggetCosmicNeutronium>, <draconicevolution:awakened_core>, <draconicevolution:chaos_shard:2>, null], 
	[null, null, <ore:nuggetCosmicNeutronium>, <ore:blockDraconiumAwakened>, <draconicevolution:wyvern_core>, <ore:blockDraconiumAwakened>, <ore:nuggetCosmicNeutronium>, null, null], 
	[null, null, <ic2:crafting:38>, <draconicevolution:wyvern_core>, <thaumcraft:primordial_pearl>.anyDamage(), <draconicevolution:wyvern_core>, <ic2:crafting:38>, null, null], 
	[null, null, <ore:nuggetCosmicNeutronium>, <ore:blockDraconiumAwakened>, <draconicevolution:wyvern_core>, <ore:blockDraconiumAwakened>, <ore:nuggetCosmicNeutronium>, null, null], 
	[null, <draconicevolution:chaos_shard:2>, <draconicevolution:awakened_core>, <ore:nuggetCosmicNeutronium>, <ic2:crafting:38>, <ore:nuggetCosmicNeutronium>, <draconicevolution:awakened_core>, <draconicevolution:chaos_shard:2>, null], 
	[<draconicevolution:chaos_shard:1>, <ore:blockEvilMetal>, <draconicevolution:chaos_shard:2>, null, null, null, <draconicevolution:chaos_shard:2>, <ore:blockEvilMetal>, <draconicevolution:chaos_shard:1>], 
	[<draconicevolution:chaos_shard:1>, <draconicevolution:chaos_shard:1>, null, null, null, null, null, <draconicevolution:chaos_shard:1>, <draconicevolution:chaos_shard:1>]
]);

#DracCore
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:awakened_core>, [
	[<avaritia:resource:2>, <ore:ingotDraconiumAwakened>, null, null, <bloodmagic:slate:4>, null, null, <ore:ingotDraconiumAwakened>, <avaritia:resource:2>], 
	[<ore:ingotDraconiumAwakened>, <ore:blockNetherStar>, <ore:ingotDraconiumAwakened>, <draconicevolution:draconium_block:1>, null, <draconicevolution:draconium_block:1>, <ore:ingotDraconiumAwakened>, <ore:blockNetherStar>, <ore:ingotDraconiumAwakened>], 
	[null, <ore:ingotDraconiumAwakened>, <ore:ingotKnightslime>, <ore:ingotVoid>, <draconicevolution:draconium_block:1>, <ore:ingotVoid>, <ore:ingotKnightslime>, <ore:ingotDraconiumAwakened>, null], 
	[null, <draconicevolution:draconium_block:1>, <ore:ingotVoid>, <bloodarsenal:blood_diamond:3>, <advanced_solar_panels:crafting:13>, <bloodarsenal:blood_diamond:3>, <ore:ingotVoid>, <draconicevolution:draconium_block:1>, null], 
	[<bloodmagic:slate:4>, null, <draconicevolution:draconium_block:1>, <advanced_solar_panels:crafting:13>, <draconicevolution:wyvern_core>, <advanced_solar_panels:crafting:13>, <draconicevolution:draconium_block:1>, null, <bloodmagic:slate:4>], 
	[null, <draconicevolution:draconium_block:1>, <ore:ingotVoid>, <bloodarsenal:blood_diamond:3>, <advanced_solar_panels:crafting:13>, <bloodarsenal:blood_diamond:3>, <ore:ingotVoid>, <draconicevolution:draconium_block:1>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:ingotKnightslime>, <ore:ingotVoid>, <draconicevolution:draconium_block:1>, <ore:ingotVoid>, <ore:ingotKnightslime>, <ore:ingotDraconiumAwakened>, null], 
	[<ore:ingotDraconiumAwakened>, <ore:blockNetherStar>, <ore:ingotDraconiumAwakened>, <draconicevolution:draconium_block:1>, null, <draconicevolution:draconium_block:1>, <ore:ingotDraconiumAwakened>, <ore:blockNetherStar>, <ore:ingotDraconiumAwakened>], 
	[<avaritia:resource:2>, <ore:ingotDraconiumAwakened>, null, null, <bloodmagic:slate:4>, null, null, <ore:ingotDraconiumAwakened>, <avaritia:resource:2>]
]);

#WywernCore
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:wyvern_core>, [
	[null, <draconicevolution:draconium_block:1>, <ore:circuitUltimate>, <ore:ingotEvilMetal>, <ore:circuitUltimate>, <draconicevolution:draconium_block:1>, null], 
	[<draconicevolution:draconium_block:1>, <ore:ingotThaumium>, <mekanismgenerators:reactor:1>, <bloodmagic:slate:4>, <mekanismgenerators:reactor:1>, <ore:ingotThaumium>, <draconicevolution:draconium_block:1>], 
	[<ore:circuitUltimate>, <mekanismgenerators:reactor:1>, <gravisuite:crafting:2>, <advanced_solar_panels:crafting:5>, <gravisuite:crafting:2>, <mekanismgenerators:reactor:1>, <ore:circuitUltimate>], 
	[<ore:ingotEvilMetal>, <draconicevolution:draconic_core>, <advanced_solar_panels:crafting:5>, <bloodarsenal:blood_diamond:3>, <advanced_solar_panels:crafting:5>, <draconicevolution:draconic_core>, <ore:ingotEvilMetal>], 
	[<ore:circuitUltimate>, <mekanismgenerators:reactor:1>, <gravisuite:crafting:2>, <advanced_solar_panels:crafting:5>, <gravisuite:crafting:2>, <mekanismgenerators:reactor:1>, <ore:circuitUltimate>], 
	[<draconicevolution:draconium_block:1>, <ore:ingotThaumium>, <mekanismgenerators:reactor:1>, <bloodmagic:slate:4>, <mekanismgenerators:reactor:1>, <ore:ingotThaumium>, <draconicevolution:draconium_block:1>], 
	[null, <draconicevolution:draconium_block:1>, <ore:circuitUltimate>, <ore:ingotEvilMetal>, <ore:circuitUltimate>, <draconicevolution:draconium_block:1>, null]
]);

#BaseCore
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_core>, [
	[<mekanism:basicblock:8>, <ore:blockDraconium>, <ore:circuitUltimate>, <ore:blockDraconium>, <mekanism:basicblock:8>], 
	[<ore:blockDraconium>, <ore:ingotTerrasteel>, <bloodmagic:slate:2>, <ore:ingotTerrasteel>, <ore:blockDraconium>], 
	[<ore:circuitUltimate>, <ore:ingotBrass>, <thaumictinkerer:kamiresource:2>, <ore:ingotBrass>, <ore:circuitUltimate>], 
	[<ore:blockDraconium>, <ore:ingotTerrasteel>, <bloodmagic:slate:2>, <ore:ingotTerrasteel>, <ore:blockDraconium>], 
	[<mekanism:basicblock:8>, <ore:blockDraconium>, <ore:circuitUltimate>, <ore:blockDraconium>, <mekanism:basicblock:8>]
]);

#ChaosStabilyzer
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicadditions:chaos_stabilizer_core>, [
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, null, null, null], 
	[null, null, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, null, null], 
	[null, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, null], 
	[null, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, <draconicevolution:chaotic_core>, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, null], 
	[null, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, null], 
	[null, null, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, null, null], 
	[null, null, null, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, null, null, null], 
	[null, null, null, null, null, null, null, null, null]
]);

#Magnet
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:magnet>, [
	[null, null, <ore:blockLudicrite>, null, null], 
	[null, <ore:blockLudicrite>, null, null, null], 
	[<ore:blockLudicrite>, <gravisuite:crafting:4>, null, null, <ore:blockLudicrite>], 
	[<draconicevolution:draconium_block:1>, <draconicevolution:wyvern_energy_core>, <gravisuite:crafting:4>, <ore:blockLudicrite>, null], 
	[null, <draconicevolution:draconium_block:1>, <ore:blockLudicrite>, null, null]
]);

#DracMagnet
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:magnet:1>, [
	[null, null, null, <ore:blockDraconiumAwakened>, null, null, null], 
	[null, null, <ore:blockDraconiumAwakened>, <advanced_solar_panels:crafting:13>, null, null, null], 
	[null, <ore:blockDraconiumAwakened>, <advanced_solar_panels:crafting:13>, null, null, null, null], 
	[<ore:blockDraconiumAwakened>, <gravisuite:crafting:2>, <ore:blockStellarAlloy>, null, null, <advanced_solar_panels:crafting:13>, <ore:blockDraconiumAwakened>], 
	[<ore:ingotOrichalcos>, <draconicevolution:draconic_energy_core>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <advanced_solar_panels:crafting:13>, <ore:blockDraconiumAwakened>, null], 
	[null, <draconicevolution:magnet>, <draconicevolution:draconic_energy_core>, <gravisuite:crafting:2>, <ore:blockDraconiumAwakened>, null, null], 
	[null, null, <ore:ingotOrichalcos>, <ore:blockDraconiumAwakened>, null, null, null]
]);

#DracBoots
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_boots>, [
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, <ore:ingotOrichalcos>, <draconicevolution:awakened_core>, <ore:ingotVoid>, <thaumictinkerer:kami_boots>, <ore:ingotVoid>, <draconicevolution:awakened_core>, <ore:ingotOrichalcos>, null], 
	[null, <ore:ingotDraconiumAwakened>, <thaumicaugmentation:void_boots>, <draconicevolution:draconic_energy_core>, null, <draconicevolution:draconic_energy_core>, <mysticalagriculture:supremium_boots>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotVoid>, <ore:blockVoid>, <ore:ingotOrichalcos>, null, <ore:ingotOrichalcos>, <ore:blockVoid>, <ore:ingotVoid>, null], 
	[null, <ore:blockDraconiumAwakened>, <tconstruct:slime_boots>, <ore:blockDraconiumAwakened>, null, <ore:blockDraconiumAwakened>, <ic2:quantum_boots:26>, <ore:blockDraconiumAwakened>, null], 
	[null, <ore:blockDraconiumAwakened>, <botania:terrasteelboots>, <ore:blockDraconiumAwakened>, null, <ore:blockDraconiumAwakened>, <minecraft:diamond_boots>, <ore:blockDraconiumAwakened>, null]
]);

#DracPants
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_legs>, [
	[null, <ore:ingotDraconiumAwakened>, <draconicevolution:awakened_core>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <draconicevolution:awakened_core>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <thaumcraft:void_robe_legs>, <minecraft:diamond_leggings>, <ic2:quantum_leggings:26>, <mysticalagriculture:supremium_leggings>, <botania:terrasteellegs>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <draconicevolution:awakened_core>, <draconicevolution:draconic_energy_core>, <ore:ingotOrichalcos>, <draconicevolution:draconic_energy_core>, <draconicevolution:awakened_core>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <thaumictinkerer:kami_legs>, <ore:blockVoid>, null, <ore:blockVoid>, <thaumictinkerer:kami_legs>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, <ore:ingotVoid>, null, <ore:ingotVoid>, <ore:ingotDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotVoid>, null, <ore:ingotVoid>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:blockDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:blockDraconiumAwakened>, null, <ore:blockDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:blockDraconiumAwakened>, null], 
	[null, <ore:blockDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:blockDraconiumAwakened>, null, <ore:blockDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:blockDraconiumAwakened>, null], 
	[null, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, null, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, null]
]);

#DracChestplate
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_chest>, [
	[null, <ore:blockVoid>, <ore:ingotVoid>, null, null, null, <ore:ingotVoid>, <ore:blockVoid>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotVoid>, null, <ore:ingotVoid>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <draconicevolution:draconic_energy_core>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <draconicevolution:draconic_energy_core>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <thaumcraft:void_robe_chest>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ic2:quantum_chestplate:26>, <gravisuite:gravichestplate>, <mysticalagriculture:supremium_chestplate>, <ore:blockDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:blockDraconiumAwakened>, <thaumictinkerer:kami_chest>, <botania:terrasteelchest>, <thaumictinkerer:kami_chest>, <ore:blockDraconiumAwakened>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:blockDraconiumAwakened>, <minecraft:diamond_chestplate>, <ore:blockDraconiumAwakened>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:ingotOrichalcos>, <draconicevolution:awakened_core>, <ore:blockDraconiumAwakened>, <draconicevolution:awakened_core>, <ore:ingotOrichalcos>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <ore:blockVoid>, <ore:ingotVoid>, <ore:blockVoid>, <ore:ingotVoid>, <ore:blockVoid>, <ore:ingotDraconiumAwakened>, null]
]);

#DracHelmet
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicevolution:draconic_helm>, [
	[null, <ore:ingotVoid>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:blockVoid>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <ore:ingotVoid>, null], 
	[null, <ore:blockDraconiumAwakened>, <thaumcraft:void_robe_helm>, <botania:terrasteelhelm>, <ic2:quantum_helmet:26>, <mysticalagriculture:supremium_helmet>, <minecraft:diamond_helmet>, <ore:blockDraconiumAwakened>, null], 
	[null, <ore:ingotDraconiumAwakened>, <draconicevolution:awakened_core>, <thaumictinkerer:kami_helm>, <ore:blockVoid>, <thaumictinkerer:kami_helm>, <draconicevolution:awakened_core>, <ore:ingotDraconiumAwakened>, null], 
	[null, <ore:ingotVoid>, <draconicevolution:draconic_energy_core>, null, null, null, <draconicevolution:draconic_energy_core>, <ore:ingotVoid>, null], 
	[null, <ore:blockVoid>, <draconicevolution:draconic_energy_core>, null, null, null, <draconicevolution:draconic_energy_core>, <ore:blockVoid>, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null]
]);


#Chaotic Staff
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicadditions:chaotic_staff_of_power>.withTag({}), [
	[null, null, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <draconicevolution:chaotic_core>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, null, null], 
	[null, <draconicevolution:chaos_shard>, <extrabotany:blockorichalcos>, <draconicevolution:chaotic_core>, <extrabotany:blockorichalcos>, <draconicevolution:chaotic_core>, <extrabotany:blockorichalcos>, <draconicevolution:chaos_shard>, null], 
	[<draconicevolution:chaos_shard>, <ore:blockVoid>, <draconicadditions:chaotic_energy_core>, <ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>, <draconicadditions:chaotic_energy_core>, <ore:blockVoid>, <draconicevolution:chaos_shard>], 
	[<draconicevolution:chaos_shard:2>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, null, <draconicadditions:chaotic_energy_core>, null, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard:2>], 
	[null, null, null, null, <draconicevolution:draconic_staff_of_power>, null, null, null, null], 
	[null, null, null, null, <draconicevolution:awakened_core>, null, null, null, null], 
	[null, null, null, null, <draconicevolution:chaotic_core>, null, null, null, null], 
	[null, null, null, null, <draconicevolution:awakened_core>, null, null, null, null], 
	[null, null, null, null, <draconicadditions:chaotic_energy_core>, null, null, null, null]
]);

#Chaotic Boots
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicadditions:chaotic_boots>, [
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:draconic_energy_core>, <ore:blockCosmicNeutronium>, <draconicevolution:draconic_boots>, <ore:blockCosmicNeutronium>, <draconicevolution:draconic_energy_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <extrabotany:blockorichalcos>, <draconicevolution:chaotic_core>, <draconicevolution:chaos_shard>, null, <draconicevolution:chaos_shard>, <draconicevolution:chaotic_core>, <extrabotany:blockorichalcos>, null], 
	[null, <draconicevolution:awakened_core>, <draconicadditions:chaotic_energy_core>, <draconicevolution:chaos_shard>, null, <draconicevolution:chaos_shard>, <draconicadditions:chaotic_energy_core>, <draconicevolution:awakened_core>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:chaotic_core>, <draconicevolution:chaos_shard>, null, <draconicevolution:chaos_shard>, <draconicevolution:chaotic_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <draconicadditions:chaotic_energy_core>, null, <draconicadditions:chaotic_energy_core>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, null]
]);

#Chaotic Pants
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicadditions:chaotic_legs>, [
	[null, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, null], 
	[null, <draconicevolution:chaos_shard>, <extrabotany:blockorichalcos>, <draconicadditions:chaotic_energy_core>, <draconicevolution:draconic_legs>, <draconicadditions:chaotic_energy_core>, <extrabotany:blockorichalcos>, <draconicevolution:chaos_shard>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:draconic_energy_core>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <draconicevolution:draconic_energy_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:chaotic_core>, <draconicevolution:chaos_shard>, null, <draconicevolution:chaos_shard>, <draconicevolution:chaotic_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicadditions:chaotic_energy_core>, <draconicevolution:chaos_shard:1>, null, <draconicevolution:chaos_shard:1>, <draconicadditions:chaotic_energy_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>, null, <ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>, null, <ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <draconicevolution:chaos_shard:1>, <draconicevolution:chaotic_core>, <ore:blockCosmicNeutronium>, null, <ore:blockCosmicNeutronium>, <draconicevolution:chaotic_core>, <draconicevolution:chaos_shard:1>, null], 
	[null, <draconicevolution:chaos_shard:1>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard:1>, null, <draconicevolution:chaos_shard:1>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard:1>, null]
]);

#Chaotic Chestplate
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicadditions:chaotic_chest>, [
	[null, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, null, null, null, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, null], 
	[null, <ore:blockCosmicNeutronium>, <ore:blockVoid>, <draconicevolution:chaos_shard>, null, <draconicevolution:chaos_shard>, <ore:blockVoid>, <ore:blockCosmicNeutronium>, null], 
	[null, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, <draconicevolution:awakened_core>, <draconicevolution:chaos_shard>, <draconicevolution:awakened_core>, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, null], 
	[null, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, <draconicevolution:chaotic_core>, <ore:blockDraconiumAwakened>, <draconicevolution:chaotic_core>, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, null], 
	[null, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <draconicevolution:draconic_chest>, <ore:blockDraconiumAwakened>, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, null], 
	[null, <draconicevolution:chaos_shard>, <draconicevolution:awakened_core>, <draconicadditions:chaotic_energy_core>, <ore:blockDraconiumAwakened>, <draconicadditions:chaotic_energy_core>, <draconicevolution:awakened_core>, <draconicevolution:chaos_shard>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:chaotic_core>, <draconicevolution:awakened_core>, <ore:blockDraconiumAwakened>, <draconicevolution:awakened_core>, <draconicevolution:chaotic_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <ore:blockCosmicNeutronium>, <extrabotany:blockorichalcos>, <draconicadditions:chaotic_energy_core>, <ore:blockVoid>, <draconicadditions:chaotic_energy_core>, <extrabotany:blockorichalcos>, <ore:blockCosmicNeutronium>, null], 
	[null, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, null]
]);

#Chaotic Helmet
mods.extendedcrafting.TableCrafting.addShaped(0, <draconicadditions:chaotic_helm>, [
	[null, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, <draconicevolution:chaos_shard>, null], 
	[null, <draconicevolution:chaos_shard>, <draconicadditions:chaotic_energy_core>, <draconicevolution:chaotic_core>, <draconicevolution:draconic_helm>, <draconicevolution:chaotic_core>, <draconicadditions:chaotic_energy_core>, <draconicevolution:chaos_shard>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:chaotic_core>, <extrabotany:blockorichalcos>, <ore:blockCosmicNeutronium>, <extrabotany:blockorichalcos>, <draconicevolution:chaotic_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>, null, <ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>, null], 
	[null, <ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>, null, <ore:blockCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockCosmicNeutronium>, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null], 
	[null, null, null, null, null, null, null, null, null]
]);
mods.thaumcraft.ArcaneWorkbench.removeRecipe(<thaumictinkerer:magnet>);

mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe(
    "TT_THAUMIC_MAGNETS",
    "TT_THAUMIC_MAGNETS",
    200,
    [
        <aspect:aer>, <aspect:ordo>, <aspect:terra>, <aspect:perditio>
    ],
    <thaumictinkerer:mob_magnet>,
    [
        [null, <thaumcraft:ingot>, null],
        [<thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "aer"}]}), <thaumcraft:ingot>, <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "terra"}]})],
        [<thaumcraft:log_greatwood>, <advanced_solar_panels:crafting:13>, <thaumcraft:log_greatwood>]
    ]
);

val pearl = <thaumcraft:primordial_pearl>;

recipes.addShapeless("pearl_repair", pearl,
    [pearl.anyDamage().onlyDamaged().noReturn().marked("one"), pearl.anyDamage().onlyDamaged().noReturn().marked("two")],

    function(out, ins, cInfo) {
        val durability1 = pearl.maxDamage - ins.one.damage;
        val durability2 = pearl.maxDamage - ins.two.damage;

        return pearl.withDamage(max(0, pearl.maxDamage - (durability1 + durability2)));
    },
    null
);

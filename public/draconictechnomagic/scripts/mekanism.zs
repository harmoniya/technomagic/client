recipes.remove(<mekanism:machineblock:4>);
recipes.remove(<mekanism:filterupgrade>);
recipes.remove(<mekanism:mufflingupgrade>);
recipes.remove(<mekanism:energytablet>);
recipes.remove(<mekanism:electricbow>);
recipes.remove(<mekanism:configurator>);
recipes.remove(<mekanism:energyupgrade>);
recipes.remove(<mekanism:speedupgrade>);
recipes.remove(<mekanism:basicblock:8>);
recipes.remove(<mekanism:atomicdisassembler>);
recipes.remove(<mekanism:basicblock:6>.withTag({tier: 0}));
recipes.remove(<mekanism:networkreader>);
recipes.remove(<mekanism:obsidiantnt>);
recipes.remove(<mekanism:cardboardbox>);
recipes.remove(<mekanism:machineblock2:15>);
recipes.remove(<mekanism:robit>);
recipes.remove(<mekanismgenerators:reactor>);
recipes.remove(<mekanism:machineblock2:14>);
recipes.remove(<mekanism:machineblock2:13>);
mods.mekanism.infuser.removeRecipe(<mekanism:enrichedalloy>);

mods.mekanism.infuser.addRecipe("REDSTONE", 10, <minecraft:iron_ingot>, <mekanism:enrichedalloy>);

recipes.addShaped(<mekanism:filterupgrade>, [
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>],
	[<tconstruct:seared>, <minecraft:paper>, <tconstruct:seared>],
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>]
]);

recipes.addShaped(<mekanism:mufflingupgrade>, [
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>],
	[<tconstruct:seared>, <minecraft:wool>, <tconstruct:seared>],
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>]
]);

val anyOrb =
	<bloodmagic:blood_orb>.withTag({orb: "bloodmagic:weak"}) |
	<bloodmagic:blood_orb>.withTag({orb: "bloodmagic:apprentice"}) |
	<bloodmagic:blood_orb>.withTag({orb: "bloodmagic:magician"}) |
	<bloodmagic:blood_orb>.withTag({orb: "bloodmagic:master"}) |
	<bloodmagic:blood_orb>.withTag({orb: "bloodmagic:archmage"});

recipes.addShaped(<mekanism:energytablet>, [
	[<mekanism:controlcircuit>, <mekanism:controlcircuit>, <mekanism:controlcircuit>],
	[<mekanism:controlcircuit>, <mekanism:enrichedalloy>, <mekanism:controlcircuit>],
	[<mekanism:enrichedalloy>, anyOrb, <mekanism:enrichedalloy>]
]);

recipes.addShaped(<mekanism:electricbow>, [
	[null, null, null],
	[<mekanism:energytablet>, <bloodmagic:sentient_bow>, <mekanism:energytablet>],
	[null, null, null]
]);

recipes.addShaped(<mekanism:configurator>, [
	[null, null, null],
	[<mekanism:energytablet>, <bloodmagic:sacrificial_dagger>, <mekanism:energytablet>],
	[null, null, null]
]);

recipes.addShaped(<mekanism:energyupgrade>, [
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>],
	[<tconstruct:seared>, <bloodmagic:blood_rune:6>, <tconstruct:seared>],
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>]
]);

recipes.addShaped(<mekanism:speedupgrade>, [
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>],
	[<tconstruct:seared>, <bloodmagic:blood_rune:1>, <tconstruct:seared>],
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>]
]);

recipes.addShaped(<mekanism:basicblock:8>, [
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>],
	[<tconstruct:seared>, <bloodmagic:slate:2>, <tconstruct:seared>],
	[<ore:ingotSteel>, <tconstruct:seared>, <ore:ingotSteel>]
]);

#Atomic Dismember
recipes.addShaped(<mekanism:atomicdisassembler>, [
	[<mekanism:atomicalloy>, <mekanism:speedupgrade>, <mekanism:atomicalloy>],
	[<enderio:item_basic_capacitor:2>, <mekanism:energytablet>, <enderio:item_basic_capacitor:2>],
	[<mekanism:speedupgrade>, <mekanism:electrolyticcore>, <mekanism:speedupgrade>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mekanismgenerators:reactor>, [
	[<ore:compressed8xCobblestone>, <extrabees:honey_comb:21>, <astralsorcery:itemcraftingcomponent:4>, <ore:blockLudicrite>, <gravisuite:crafting:2>, <ore:blockLudicrite>, <astralsorcery:itemcraftingcomponent:4>, <extrabees:honey_comb:21>, <ore:compressed8xCobblestone>],
	[<extrabees:honey_comb:21>, <ic2:te:85>, <advanced_solar_panels:crafting:13>, <mekanismgenerators:reactor:1>, <draconicevolution:awakened_core>, <mekanismgenerators:reactor:1>, <advanced_solar_panels:crafting:13>, <ic2:te:85>, <extrabees:honey_comb:21>],
	[<astralsorcery:itemcraftingcomponent:4>, <advanced_solar_panels:crafting:13>, <ore:craftingMolecularTransformer>, <ic2:iridium_reflector>, <ic2:te:22>, <ic2:iridium_reflector>, <ore:craftingMolecularTransformer>, <advanced_solar_panels:crafting:13>, <astralsorcery:itemcraftingcomponent:4>],
	[<ore:blockLudicrite>, <mekanismgenerators:reactor:1>, <ic2:iridium_reflector>, <astralsorcery:itemcraftingcomponent:4>, <extendedcrafting:singularity:3>, <astralsorcery:itemcraftingcomponent:4>, <ic2:iridium_reflector>, <mekanismgenerators:reactor:1>, <ore:blockLudicrite>],
	[<gravisuite:crafting:2>, <draconicevolution:awakened_core>, <ic2:te:22>, <extendedcrafting:singularity:35>, <appliedenergistics2:creative_energy_cell>, <extendedcrafting:singularity:35>, <ic2:te:22>, <draconicevolution:awakened_core>, <gravisuite:crafting:2>],
	[<ore:blockLudicrite>, <mekanismgenerators:reactor:1>, <ic2:iridium_reflector>, <astralsorcery:itemcraftingcomponent:4>, <extendedcrafting:singularity:3>, <astralsorcery:itemcraftingcomponent:4>, <ic2:iridium_reflector>, <mekanismgenerators:reactor:1>, <ore:blockLudicrite>],
	[<astralsorcery:itemcraftingcomponent:4>, <advanced_solar_panels:crafting:13>, <ore:craftingMolecularTransformer>, <ic2:iridium_reflector>, <ic2:te:22>, <ic2:iridium_reflector>, <ore:craftingMolecularTransformer>, <advanced_solar_panels:crafting:13>, <astralsorcery:itemcraftingcomponent:4>],
	[<extrabees:honey_comb:21>, <ic2:te:85>, <advanced_solar_panels:crafting:13>, <mekanismgenerators:reactor:1>, <draconicevolution:awakened_core>, <mekanismgenerators:reactor:1>, <advanced_solar_panels:crafting:13>, <ic2:te:85>, <extrabees:honey_comb:21>],
	[<ore:compressed8xCobblestone>, <extrabees:honey_comb:21>, <astralsorcery:itemcraftingcomponent:4>, <ore:blockLudicrite>, <gravisuite:crafting:2>, <ore:blockLudicrite>, <astralsorcery:itemcraftingcomponent:4>, <extrabees:honey_comb:21>, <ore:compressed8xCobblestone>]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mekanism:machineblock2:14>, [
	[null, null, <advanced_solar_panels:crafting:6>, <advanced_solar_panels:crafting:6>, <advanced_solar_panels:crafting:6>, null, null],
	[null, <mekanismgenerators:reactor:1>, <ic2:iridium_reflector>, <ore:blockStellarAlloy>, <ic2:iridium_reflector>, <mekanismgenerators:reactor:1>, null],
	[<advanced_solar_panels:crafting:6>, <ic2:iridium_reflector>, <thaumcraft:hand_mirror>, <ore:craftingMTCore>, <thaumcraft:hand_mirror>, <ic2:iridium_reflector>, <advanced_solar_panels:crafting:6>],
	[<advanced_solar_panels:crafting:6>, <ore:blockStellarAlloy>, <ore:craftingMTCore>, <ore:ingotOrichalcos>, <ore:craftingMTCore>, <ore:blockStellarAlloy>, <advanced_solar_panels:crafting:6>],
	[<advanced_solar_panels:crafting:6>, <ic2:iridium_reflector>, <thaumcraft:hand_mirror>, <ore:craftingMTCore>, <thaumcraft:hand_mirror>, <ic2:iridium_reflector>, <advanced_solar_panels:crafting:6>],
	[null, <mekanismgenerators:reactor:1>, <ic2:iridium_reflector>, <ore:blockStellarAlloy>, <ic2:iridium_reflector>, <mekanismgenerators:reactor:1>, null],
	[null, null, <advanced_solar_panels:crafting:6>, <advanced_solar_panels:crafting:6>, <advanced_solar_panels:crafting:6>, null, null]
]);

mods.extendedcrafting.TableCrafting.addShaped(0, <mekanism:machineblock2:13>, [
	[null, null, null, null, null, null, null],
	[<mekanismgenerators:reactor:1>, <mekanismgenerators:reactor:1>, <ic2:te:41>, null, null, null, null],
	[<ore:blockStellarAlloy>, <ic2:te:80>, <draconicevolution:wyvern_core>, <mekanismgenerators:reactor:1>, <mekanismgenerators:reactor:1>, <mekanismgenerators:reactor:1>, <extrautils2:decorativeglass:5>],
	[<draconicevolution:energy_infuser>, <draconicevolution:wyvern_core>, <ic2:mining_laser:26>, <advanced_solar_panels:crafting:5>, <ic2:mining_laser:26>, <extrautils2:decorativeglass:5>, <extrautils2:decorativeglass:5>],
	[<ore:blockStellarAlloy>, <ic2:te:80>, <draconicevolution:wyvern_core>, <mekanismgenerators:reactor:1>, <mekanismgenerators:reactor:1>, <mekanismgenerators:reactor:1>, <extrautils2:decorativeglass:5>],
	[<mekanismgenerators:reactor:1>, <mekanismgenerators:reactor:1>, <ic2:te:41>, null, null, null, null],
	[null, null, null, null, null, null, null]
]);

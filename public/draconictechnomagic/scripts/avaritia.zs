recipes.remove(<extendedcrafting:table_advanced>);
recipes.remove(<extendedcrafting:table_elite>);
recipes.remove(<extendedcrafting:table_ultimate>);
recipes.remove(<avaritia:extreme_crafting_table>);
recipes.remove(<extendedcrafting:compressor>);
recipes.remove(<avaritia:neutron_collector>);
recipes.remove(<denseneutroncollectors:compressed_neutron_collector>);
recipes.remove(<denseneutroncollectors:double_compressed_neutron_collector>);
recipes.remove(<denseneutroncollectors:triple_compressed_neutron_collector>);


mods.avaritia.ExtremeCrafting.remove(<bloodmagic:sacrificial_dagger:1>.withTag({sacrifice: 0 as byte}));
mods.avaritia.ExtremeCrafting.remove(<extrautils2:passivegenerator:6>);
mods.avaritia.ExtremeCrafting.remove(<appliedenergistics2:creative_energy_cell>);
mods.avaritia.ExtremeCrafting.remove(<botania:pool:1>);
mods.avaritia.ExtremeCrafting.remove(<thermalexpansion:capacitor:32000>.withTag({Energy: 25000000}));
mods.avaritia.ExtremeCrafting.remove(<avaritia:neutron_collector>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:resource:5>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:resource:6>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:cosmic_meatballs>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:ultimate_stew>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_sword>);
mods.avaritia.ExtremeCrafting.remove(<avaritia:infinity_pickaxe>.withTag({ench: [{lvl: 10 as short, id: 35 as short}]}));
mods.extendedcrafting.CombinationCrafting.remove(<extendedcrafting:singularity_ultimate>);

recipes.remove(<bloodmagic:sacrificial_dagger:1>.withTag({sacrifice: 0 as byte}));
recipes.remove(<bloodmagic:sacrificial_dagger:1>);
recipes.remove(<extrautils2:passivegenerator:6>);


#try
recipes.removeByRecipeName("bloodmagic:sacrificial_dagger:1");
recipes.removeByRecipeName("extrautils2:passivegenerator:6");
recipes.removeByRecipeName("appliedenergistics2:creative_energy_cell");
recipes.removeByRecipeName("botania:pool:1");
recipes.removeByRecipeName("thermalexpansion:capacitor:32000");

#Singularities
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:1>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:2>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:3>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:4>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:5>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:6>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:7>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:16>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:17>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:18>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:19>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:20>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:21>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:22>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:23>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:24>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:25>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:26>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:27>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:28>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:29>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:30>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:31>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:32>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:33>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:34>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:35>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:48>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:49>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:50>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:64>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:65>);
mods.extendedcrafting.CompressionCrafting.remove(<extendedcrafting:singularity:66>);
#Singularities
#  																				  Item-Count       Energy
#mods.extendedcrafting.CompressionCrafting.addRecipe(<output>, <input>, inputCount, <catalyst>, rfCost);

mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity>, <minecraft:coal>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:25>, <thermalfoundation:material:133>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:5>, <minecraft:gold_ingot>,10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:2>, <minecraft:dye:4>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:7>, <minecraft:emerald>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:16>, <thermalfoundation:material:132>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:17>, <ic2:ingot:2>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:19>, <forestry:ingot_bronze>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:4>, <minecraft:glowstone>, 4000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:3>, <minecraft:redstone>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:1>, <minecraft:iron_ingot>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:22>, <ic2:ingot:4>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:6>, <minecraft:diamond>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:23>, <ic2:ingot:3>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:18>, <ic2:ingot:6>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:32>, <advanced_solar_panels:crafting:11>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:32>, <ic2:ingot:8>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:49>, <thermalfoundation:material:166>, 4000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:50>, <thermalfoundation:material:167>, 4000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:26>, <thermalfoundation:material:164>, 4000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:48>, <thermalfoundation:material:165>, 4000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:35>, <ic2:misc_resource:1>, 8000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:66>, <tconstruct:ingots:2>, 8000, <mekanism:controlcircuit:3>, 2000000, 100000);
mods.extendedcrafting.CompressionCrafting.addRecipe(<extendedcrafting:singularity:24>, <ic2:ingot:5>, 10000, <mekanism:controlcircuit:3>, 2000000, 100000);

#END

#Ultimate Singularity
mods.extendedcrafting.TableCrafting.addShaped(0, <extendedcrafting:singularity_ultimate>, [
	[<thaumictinkerer:kamiresource:2>, <ore:ingotAstralStarmetal>, null, <ore:ingotSupremium>, <gravisuite:crafting:2>, <ore:ingotSupremium>, null, <ore:ingotAstralStarmetal>, <thaumictinkerer:kamiresource:2>],
	[<ore:ingotAstralStarmetal>, <draconicevolution:chaos_shard:1>, <ore:blockVoid>, <extendedcrafting:singularity:19>, <extendedcrafting:singularity:17>, <extendedcrafting:singularity:16>, <ore:blockVoid>, <draconicevolution:chaos_shard:1>, <ore:ingotAstralStarmetal>],
	[null, <ore:blockVoid>, <extendedcrafting:singularity:4>, <draconicevolution:draconium_block:1>, <ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquidfusionfuel", Amount: 1000}}), <draconicevolution:draconium_block:1>, <extendedcrafting:singularity:7>, <ore:blockVoid>, null],
	[<ore:ingotSupremium>, <extendedcrafting:singularity:3>, <draconicevolution:draconium_block:1>, <botania:storage:1>, <ore:gaiaIngot>, <botania:storage:1>, <draconicevolution:draconium_block:1>, <extendedcrafting:singularity:2>, <ore:ingotSupremium>],
	[<gravisuite:crafting:2>, <extendedcrafting:singularity:1>, <ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquidfusionfuel", Amount: 1000}}), <ore:gaiaIngot>, <ore:ingotSupremium>, <ore:gaiaIngot>, <ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquidfusionfuel", Amount: 1000}}), <extendedcrafting:singularity:5>, <gravisuite:crafting:2>],
	[<ore:ingotSupremium>, <extendedcrafting:singularity:22>, <draconicevolution:draconium_block:1>, <botania:storage:1>, <ore:gaiaIngot>, <botania:storage:1>, <draconicevolution:draconium_block:1>, <extendedcrafting:singularity:25>, <ore:ingotSupremium>],
	[null, <ore:blockVoid>, <extendedcrafting:singularity:6>, <draconicevolution:draconium_block:1>, <ic2:fluid_cell>.withTag({Fluid: {FluidName: "liquidfusionfuel", Amount: 1000}}), <draconicevolution:draconium_block:1>, <extendedcrafting:singularity>, <ore:blockVoid>, null],
	[<ore:ingotAstralStarmetal>, <draconicevolution:chaos_shard:1>, <ore:blockVoid>, <extendedcrafting:singularity:23>, <extendedcrafting:singularity:18>, <extendedcrafting:singularity:32>, <ore:blockVoid>, <draconicevolution:chaos_shard:1>, <ore:ingotAstralStarmetal>],
	[<thaumictinkerer:kamiresource:2>, <ore:ingotAstralStarmetal>, null, <ore:ingotSupremium>, <gravisuite:crafting:2>, <ore:ingotSupremium>, null, <ore:ingotAstralStarmetal>, <thaumictinkerer:kamiresource:2>]
]);

#Ultimate Crafting DELETED
/*
mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:extreme_crafting_table>, [
	[<ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <ic2:crafting:4>, <bloodarsenal:blood_diamond:3>, <ic2:crafting:4>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>],
	[<ore:blockCrystalMatrix>, <ore:blockCrystallinePinkSlime>, <ore:itemRubber>, <ore:blockCrystallinePinkSlime>, <ore:itemRubber>, <ore:blockCrystallinePinkSlime>, <ore:blockCrystalMatrix>],
	[<ic2:crafting:4>, <ore:itemRubber>, <avaritia:double_compressed_crafting_table>, <ore:circuitUltimate>, <avaritia:double_compressed_crafting_table>, <ore:itemRubber>, <ic2:crafting:4>],
	[<bloodarsenal:blood_diamond:3>, <ore:blockCrystallinePinkSlime>, <ore:circuitUltimate>, <draconicevolution:draconic_core>, <ore:circuitUltimate>, <ore:blockCrystallinePinkSlime>, <bloodarsenal:blood_diamond:3>],
	[<ic2:crafting:4>, <ore:itemRubber>, <avaritia:double_compressed_crafting_table>, <ore:circuitUltimate>, <avaritia:double_compressed_crafting_table>, <ore:itemRubber>, <ic2:crafting:4>],
	[<ore:blockCrystalMatrix>, <ore:blockCrystallinePinkSlime>, <ore:itemRubber>, <ore:blockCrystallinePinkSlime>, <ore:itemRubber>, <ore:blockCrystallinePinkSlime>, <ore:blockCrystalMatrix>],
	[<ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <ic2:crafting:4>, <bloodarsenal:blood_diamond:3>, <ic2:crafting:4>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>]
]);
*/

#Catalyst
mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:resource:5>, [
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, <avaritia:ultimate_stew>, <ic2:crafting:38>, <avaritia:ultimate_stew>, null, null, null],
	[null, null, <avaritia:cosmic_meatballs>, <ore:ingotCosmicNeutronium>, <extendedcrafting:singularity:50>, <ore:ingotCosmicNeutronium>, <avaritia:cosmic_meatballs>, null, null],
	[null, <avaritia:ultimate_stew>, <ore:ingotCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockDraconiumAwakened>, <draconicevolution:awakened_core>, <ore:ingotCosmicNeutronium>, <avaritia:ultimate_stew>, null],
	[null, <ic2:crafting:38>, <extendedcrafting:singularity:26>, <ore:blockDraconiumAwakened>, <extendedcrafting:singularity_ultimate>, <ore:blockDraconiumAwakened>, <extendedcrafting:singularity:49>, <ic2:crafting:38>, null],
	[null, <avaritia:ultimate_stew>, <ore:ingotCosmicNeutronium>, <draconicevolution:awakened_core>, <ore:blockDraconiumAwakened>, <draconicevolution:awakened_core>, <ore:ingotCosmicNeutronium>, <avaritia:ultimate_stew>, null],
	[null, null, <avaritia:cosmic_meatballs>, <ore:ingotCosmicNeutronium>, <extendedcrafting:singularity:48>, <ore:ingotCosmicNeutronium>, <avaritia:cosmic_meatballs>, null, null],
	[null, null, null, <avaritia:ultimate_stew>, <ic2:crafting:38>, <avaritia:ultimate_stew>, null, null, null],
	[null, null, null, null, null, null, null, null, null]
]);

#Ingot
mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:resource:6>, [
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null, null],
	[<ore:blockCrystalMatrix>, <ore:blockDraconiumAwakened>, <ore:blockCrystalMatrix>, <ore:compressed3xDustBedrock>, <avaritia:resource:5>, <ore:compressed3xDustBedrock>, <ore:blockCrystalMatrix>, <ore:blockDraconiumAwakened>, <ore:blockCrystalMatrix>],
	[<ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>],
	[<ore:blockCrystalMatrix>, <ore:blockDraconiumAwakened>, <ore:blockCrystalMatrix>, <ore:compressed3xDustBedrock>, <avaritia:resource:5>, <ore:compressed3xDustBedrock>, <ore:blockCrystalMatrix>, <ore:blockDraconiumAwakened>, <ore:blockCrystalMatrix>],
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null, null]
]);

#MeatBalls
mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:cosmic_meatballs>*8, [
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null, null],
	[null, null, <avaritia:resource:2>, <minecraft:chicken>, <minecraft:porkchop>, <minecraft:beef>, <avaritia:resource:2>, null, null],
	[null, null, <minecraft:cooked_fish:1>, null, null, null, <minecraft:fish>, null, null],
	[null, null, <minecraft:fish:3>, null, <ore:nuggetCosmicNeutronium>, null, <minecraft:fish:2>, null, null],
	[null, null, <minecraft:cooked_fish>, null, null, null, <minecraft:fish:1>, null, null],
	[null, null, <avaritia:resource:2>,<minecraft:cooked_chicken>, <minecraft:cooked_porkchop>, <minecraft:cooked_beef>, <avaritia:resource:2>, null, null],
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null, null]
]);

#UltSteawe
mods.extendedcrafting.TableCrafting.addShaped(4, <avaritia:ultimate_stew>*16, [
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, null, null, null, null, null],
	[<botania:manaresource:14>, <extrabees:honey_comb:23>, <thaumicbases:briar_seedbag>, <extrabees:honey_comb:8>, <minecraft:golden_carrot>, <extrabees:honey_comb:7>, <thaumicbases:briar_seedbag>, <extrabees:honey_comb:10>, <botania:manaresource:14>],
	[<ore:gaiaIngot>, <ore:ingotPigiron>, <forestry:bee_combs:2>, <ore:cropPumpkin>, <forestry:bee_combs:15>, <ore:cropMelon>, <forestry:bee_combs:5>, <ore:ingotPigiron>, <ore:gaiaIngot>],
	[<ore:gaiaIngot>, <minecraft:red_mushroom>, <ore:cropCarrot>, <forestry:bee_combs:6>, <ore:cropCactus>, <forestry:bee_combs:16>, <ore:cropPotato>, <minecraft:brown_mushroom>, <ore:gaiaIngot>],
	[<ore:gaiaIngot>, <forestry:fruits:0>, <ore:dropHoney>, <ore:cropWheat>, <forestry:bee_combs:7>, <ore:cropBeetroot>, <ore:dropHoneydew>, <forestry:fruits:6>, <ore:gaiaIngot>],
	[null, <ore:gaiaIngot>, <forestry:fruits:1>, <forestry:fruits:2>, <extratrees:food:14>, <forestry:fruits:4>, <forestry:fruits:5>, <ore:gaiaIngot>, null],
	[null, null, <avaritia:resource:2>, <avaritia:resource:2>, <avaritia:resource:2>, <avaritia:resource:2>, <avaritia:resource:2>, null, null]
]);

#EnderstPearl
mods.extendedcrafting.TableCrafting.addShaped(0, <ic2:crafting:38>*2, [
	[null, null, null, <ore:ingotStellarAlloy>, <ore:ingotStellarAlloy>, <ore:ingotStellarAlloy>, null, null, null],
	[null, null, <ore:ingotStellarAlloy>, <ore:blockCrudeSteel>, <ore:bEnderAirBottle>, <ore:blockCrudeSteel>, <ore:ingotStellarAlloy>, null, null],
	[null, <ore:ingotStellarAlloy>, <ore:ingotAstralStarmetal>, <ore:blockEndSteel>, <ore:ingotBrickNetherGlazed>, <ore:blockEndSteel>, <astralsorcery:itemcraftingcomponent:4>, <ore:ingotStellarAlloy>, null],
	[<ore:ingotStellarAlloy>, <ore:blockCrudeSteel>, <ore:blockEndSteel>, <ore:blockVibrantAlloy>, <ore:ingotCosmicNeutronium>, <ore:blockVibrantAlloy>, <ore:blockEndSteel>, <ore:blockCrudeSteel>, <ore:ingotStellarAlloy>],
	[<ore:ingotStellarAlloy>, <ore:bEnderAirBottle>, <ore:ingotBrickNetherGlazed>, <ore:ingotCosmicNeutronium>, <ore:netherStar>, <ore:ingotCosmicNeutronium>, <ore:ingotBrickNetherGlazed>, <ore:bEnderAirBottle>, <ore:ingotStellarAlloy>],
	[<ore:ingotStellarAlloy>, <ore:blockCrudeSteel>, <ore:blockEndSteel>, <ore:blockVibrantAlloy>, <ore:ingotCosmicNeutronium>, <ore:blockVibrantAlloy>, <ore:blockEndSteel>, <ore:blockCrudeSteel>, <ore:ingotStellarAlloy>],
	[null, <ore:ingotStellarAlloy>, <astralsorcery:itemcraftingcomponent:4>, <ore:blockEndSteel>, <ore:ingotBrickNetherGlazed>, <ore:blockEndSteel>, <ore:ingotAstralStarmetal>, <ore:ingotStellarAlloy>, null],
	[null, null, <ore:ingotStellarAlloy>, <ore:blockCrudeSteel>, <ore:bEnderAirBottle>, <ore:blockCrudeSteel>, <ore:ingotStellarAlloy>, null, null],
	[null, null, null, <ore:ingotStellarAlloy>, <ore:ingotStellarAlloy>, <ore:ingotStellarAlloy>, null, null, null]
]);

#Sword
mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:infinity_sword>, [
	[null, null, null, null, null, null, null, <ore:ingotInfinity>, <avaritia:cosmic_meatballs>],
	[null, null, null, null, null, null, <ore:ingotInfinity>, <bloodarsenal:blood_diamond:3>, <ore:ingotInfinity>],
	[null, null, null, null, null, <ore:ingotInfinity>, <avaritia:ultimate_stew>, <ore:ingotInfinity>, null],
	[null, null, null, null, <ore:ingotInfinity>, <avaritia:ultimate_stew>, <ore:ingotInfinity>, null, null],
	[null, <ore:blockCosmicNeutronium>, null, <ore:ingotInfinity>, <bloodarsenal:blood_diamond:3>, <ore:ingotInfinity>, null, null, null],
	[null, <bloodarsenal:stasis_sword>.withTag({activated: 0 as byte}), <ore:blockCosmicNeutronium>, <avaritia:cosmic_meatballs>, <ore:ingotInfinity>, null, null, null, null],
	[null, <ic2:crafting:38>, <extrabotany:firstfractal>, <ore:blockCosmicNeutronium>, null, null, null, null, null],
	[<avaritia:resource:5>, <draconicevolution:draconic_sword>, <ic2:crafting:38>, <thaumictinkerer:ichorium_sword_adv>, <ore:blockCosmicNeutronium>, null, null, null, null],
	[<ore:blockCosmicNeutronium>, <avaritia:resource:5>, null, null, null, null, null, null, null]
]);

#Pickaxe
mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:infinity_pickaxe>, [
	[null, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, <ore:ingotInfinity>, null],
	[<ore:ingotInfinity>, <bloodarsenal:blood_diamond:3>, <industrialforegoing:laser_drill>, <mysticalagriculture:supremium_pickaxe>, <industrialforegoing:laser_drill>, <botania:terrapick>, <industrialforegoing:laser_drill>, <bloodarsenal:blood_diamond:3>, <ore:ingotInfinity>],
	[<ore:ingotInfinity>, null, null, null, <draconicadditions:chaotic_staff_of_power>, null, null, null, <ore:ingotInfinity>],
	[null, null, null, null, <industrialforegoing:laser_base>, null, null, null, null],
	[null, null, null, null, <industrialforegoing:laser_drill>, null, null, null, null],
	[null, null, null, null, <ore:blockCosmicNeutronium>, null, null, null, null],
	[null, null, null, null, <ore:blockCosmicNeutronium>, null, null, null, null],
	[null, null, null, null, <ore:blockCosmicNeutronium>, null, null, null, null],
	[null, null, null, null, <draconicevolution:chaotic_core>, null, null, null, null]
]);

#Crafting 5x5
recipes.addShaped(<extendedcrafting:table_advanced>,([
[ <botania:manaresource:14>, <enderio:item_alloy_ingot:6>, <botania:manaresource:14>],
[ <extrautils2:opinium:3>, <mekanism:controlcircuit:2>, <extrautils2:opinium:3>],
[ <thaumcraft:ingot:2>, <mekanism:basicblock:8>, <thaumcraft:ingot:2>]
]));

#Crafting 7x7
mods.extendedcrafting.TableCrafting.addShaped(0, <extendedcrafting:table_elite>, [
	[<bloodarsenal:blood_diamond:3>, <ore:blockStellarAlloy>, <draconicevolution:draconic_core>, <ore:blockStellarAlloy>, <bloodarsenal:blood_diamond:3>],
	[<ore:blockEndSteel>, <ore:ingotOrichalcos>, <thaumcraft:metal_thaumium>, <ore:ingotOrichalcos>, <ore:blockEndSteel>],
	[<industrialforegoing:plastic>, <draconicevolution:wyvern_energy_core>, <ore:craftingMolecularTransformer>, <draconicevolution:wyvern_energy_core>, <industrialforegoing:plastic>],
	[<ore:blockEndSteel>, <ore:blockNetherStar>, <thaumcraft:metal_thaumium>, <ore:blockNetherStar>, <ore:blockEndSteel>],
	[<ore:circuitUltimate>, <ore:blockStellarAlloy>, <draconicevolution:draconic_core>, <ore:blockStellarAlloy>, <ore:circuitUltimate>]
]);

#Crafting 9x9
mods.extendedcrafting.TableCrafting.addShaped(0, <extendedcrafting:table_ultimate>, [
	[<ore:blockVoid>, <botania:storage:1>, <advanced_solar_panels:crafting:9>, <draconicevolution:wyvern_core>, <advanced_solar_panels:crafting:9>, <botania:storage:1>, <ore:blockVoid>],
	[<botania:storage:1>, <ore:blockStellarAlloy>, <advanced_solar_panels:crafting:13>, <ore:skullSentientEnder>, <advanced_solar_panels:crafting:13>, <ore:blockStellarAlloy>, <botania:storage:1>],
	[<ore:ingotOrichalcos>, <ore:blockPulsatingIron>, <threng:material:14>, <ore:ingotCosmicNeutronium>, <threng:material:14>, <ore:blockPulsatingIron>, <ore:ingotOrichalcos>],
	[<draconicevolution:wyvern_core>, <ore:skullSentientEnder>, <ore:ingotCosmicNeutronium>, <ore:machineBlockAdvanced>, <ore:ingotCosmicNeutronium>, <ore:skullSentientEnder>, <draconicevolution:wyvern_core>],
	[<ore:ingotOrichalcos>, <ore:blockPulsatingIron>, <threng:material:14>, <ore:ingotCosmicNeutronium>, <threng:material:14>, <ore:blockPulsatingIron>, <ore:ingotOrichalcos>],
	[<botania:storage:1>, <ore:blockStellarAlloy>, <advanced_solar_panels:crafting:13>, <ore:skullSentientEnder>, <advanced_solar_panels:crafting:13>, <ore:blockStellarAlloy>, <botania:storage:1>],
	[<ore:blockVoid>, <botania:storage:1>, <advanced_solar_panels:crafting:9>, <draconicevolution:wyvern_core>, <advanced_solar_panels:crafting:9>, <botania:storage:1>, <ore:blockVoid>]
]);

#CreativeEnergyStore
mods.extendedcrafting.TableCrafting.addShaped(0, <mekanism:energycube>.withTag({tier: 4, mekData: {energyStored: 1.7976931348623157E308}}), [
	[<ore:blockInfinity>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <mekanism:energycube>, <enderio:item_capacitor_stellar>, <mekanism:energycube>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockInfinity>],
	[<ore:blockCosmicNeutronium>, <ore:ingotInfinity>, <ore:blockSupremiumEssence>, <ore:blockCosmicNeutronium>, <appliedenergistics2:creative_energy_cell>, <ore:blockCosmicNeutronium>, <ore:blockSupremiumEssence>, <ore:ingotInfinity>, <ore:blockCosmicNeutronium>],
	[<ore:blockCosmicNeutronium>, <ore:blockSupremiumEssence>, <draconicadditions:chaotic_energy_core>, <avaritia:resource:5>, <draconicevolution:chaotic_core>, <avaritia:resource:5>, <draconicadditions:chaotic_energy_core>, <ore:blockSupremiumEssence>, <ore:blockCosmicNeutronium>],
	[<mekanism:energycube>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <hrm-industrial-plus:gods_sp:6>, <ore:ingotInfinity>, <hrm-industrial-plus:gods_sp:6>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <mekanism:energycube>],
	[<enderio:item_capacitor_stellar>, <appliedenergistics2:creative_energy_cell>, <draconicevolution:chaotic_core>, <ore:ingotInfinity>, <thermalexpansion:capacitor:32000>, <ore:ingotInfinity>, <draconicevolution:chaotic_core>, <appliedenergistics2:creative_energy_cell>, <enderio:item_capacitor_stellar>],
	[<mekanism:energycube>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <hrm-industrial-plus:gods_sp:6>, <ore:ingotInfinity>, <hrm-industrial-plus:gods_sp:6>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <mekanism:energycube>],
	[<ore:blockCosmicNeutronium>, <ore:blockSupremiumEssence>, <draconicadditions:chaotic_energy_core>, <avaritia:resource:5>, <draconicevolution:chaotic_core>, <avaritia:resource:5>, <draconicadditions:chaotic_energy_core>, <ore:blockSupremiumEssence>, <ore:blockCosmicNeutronium>],
	[<ore:blockCosmicNeutronium>, <ore:ingotInfinity>, <ore:blockSupremiumEssence>, <ore:blockCosmicNeutronium>, <appliedenergistics2:creative_energy_cell>, <ore:blockCosmicNeutronium>, <ore:blockSupremiumEssence>, <ore:ingotInfinity>, <ore:blockCosmicNeutronium>],
	[<ore:blockInfinity>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <mekanism:energycube>, <enderio:item_capacitor_stellar>, <mekanism:energycube>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <ore:blockInfinity>]
]);

#EnergyCell
mods.extendedcrafting.TableCrafting.addShaped(0, <appliedenergistics2:creative_energy_cell>, [
	[<threng:material:14>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <appliedenergistics2:dense_energy_cell>, <appliedenergistics2:material:47>, <appliedenergistics2:dense_energy_cell>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <threng:material:14>],
	[<ore:blockCosmicNeutronium>, <appliedenergistics2:material:47>, <bloodmagic:slate:4>, <threng:material:6>, <ore:compressed8xCobblestone>, <threng:material:6>, <bloodmagic:slate:4>, <appliedenergistics2:material:47>, <ore:blockCosmicNeutronium>],
	[<ore:blockCosmicNeutronium>, <bloodmagic:slate:4>, <extracells:storage.component:3>, <appliedenergistics2:dense_energy_cell>, <draconicevolution:awakened_core>, <appliedenergistics2:dense_energy_cell>, <extracells:storage.component:3>, <bloodmagic:slate:4>, <ore:blockCosmicNeutronium>],
	[<appliedenergistics2:dense_energy_cell>, <threng:material:6>, <appliedenergistics2:dense_energy_cell>, <ore:ingotCosmicNeutronium>, <threng:material:14>, <ore:ingotCosmicNeutronium>, <appliedenergistics2:dense_energy_cell>, <threng:material:6>, <appliedenergistics2:dense_energy_cell>],
	[<appliedenergistics2:material:47>, <ore:compressed8xCobblestone>, <draconicevolution:awakened_core>, <threng:material:14>, <appliedenergistics2:dense_energy_cell>, <threng:material:14>, <draconicevolution:awakened_core>, <ore:compressed8xCobblestone>, <appliedenergistics2:material:47>],
	[<appliedenergistics2:dense_energy_cell>, <threng:material:6>, <appliedenergistics2:dense_energy_cell>, <ore:ingotCosmicNeutronium>, <threng:material:14>, <ore:ingotCosmicNeutronium>, <appliedenergistics2:dense_energy_cell>, <threng:material:6>, <appliedenergistics2:dense_energy_cell>],
	[<ore:blockCosmicNeutronium>, <bloodmagic:slate:4>, <extracells:storage.component:3>, <appliedenergistics2:dense_energy_cell>, <draconicevolution:awakened_core>, <appliedenergistics2:dense_energy_cell>, <extracells:storage.component:3>, <bloodmagic:slate:4>, <ore:blockCosmicNeutronium>],
	[<ore:blockCosmicNeutronium>, <appliedenergistics2:material:47>, <bloodmagic:slate:4>, <threng:material:6>, <ore:compressed8xCobblestone>, <threng:material:6>, <bloodmagic:slate:4>, <appliedenergistics2:material:47>, <ore:blockCosmicNeutronium>],
	[<threng:material:14>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <appliedenergistics2:dense_energy_cell>, <appliedenergistics2:material:47>, <appliedenergistics2:dense_energy_cell>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <threng:material:14>]
]);

#ManaPool
mods.extendedcrafting.TableCrafting.addShaped(0, <botania:pool:1>, [
	[<draconicadditions:chaotic_energy_core>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <extrabotany:blockorichalcos>, <extrabotany:blockorichalcos>, <extrabotany:blockorichalcos>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <draconicadditions:chaotic_energy_core>],
	[<ore:blockCosmicNeutronium>, <ore:blockInfinity>, <ore:runeLustB>, <mekanism:polyethene:2>, <bloodmagic:slate:4>, <mekanism:polyethene:2>, <ore:runeGluttonyB>, <ore:blockInfinity>, <ore:blockCosmicNeutronium>],
	[<ore:blockCosmicNeutronium>, <ore:runeManaB>, <botania:pool:3>, <ore:ingotInfinity>, <draconicevolution:chaotic_core>, <ore:ingotInfinity>, <botania:pool:3>, <ore:runeGreedB>, <ore:blockCosmicNeutronium>],
	[<extrabotany:blockorichalcos>, <mekanism:polyethene:2>, <ore:ingotInfinity>, <bloodarsenal:blood_diamond:3>, <ore:seedsTier5>, <bloodarsenal:blood_diamond:3>, <ore:ingotInfinity>, <mekanism:polyethene:2>, <extrabotany:blockorichalcos>],
	[<extrabotany:blockorichalcos>, <bloodmagic:slate:4>, <draconicevolution:chaotic_core>, <ore:seedsTier3>, <extracells:storage.component:3>, <ore:seedsTier4>, <draconicevolution:chaotic_core>, <bloodmagic:slate:4>, <extrabotany:blockorichalcos>],
	[<extrabotany:blockorichalcos>, <mekanism:polyethene:2>, <ore:ingotInfinity>, <bloodarsenal:blood_diamond:3>, <ore:seedsTier2>, <bloodarsenal:blood_diamond:3>, <ore:ingotInfinity>, <mekanism:polyethene:2>, <extrabotany:blockorichalcos>],
	[<ore:blockCosmicNeutronium>, <ore:runePrideB>, <botania:pool:3>, <ore:ingotInfinity>, <draconicevolution:chaotic_core>, <ore:ingotInfinity>, <botania:pool:3>, <ore:runeSlothB>, <ore:blockCosmicNeutronium>],
	[<ore:blockCosmicNeutronium>, <ore:blockInfinity>, <ore:runeEnvyB>, <mekanism:polyethene:2>, <bloodmagic:slate:4>, <mekanism:polyethene:2>, <ore:runeWrathB>, <ore:blockInfinity>, <ore:blockCosmicNeutronium>],
	[<draconicadditions:chaotic_energy_core>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <extrabotany:blockorichalcos>, <extrabotany:blockorichalcos>, <extrabotany:blockorichalcos>, <ore:blockCosmicNeutronium>, <ore:blockCosmicNeutronium>, <draconicadditions:chaotic_energy_core>]
]);

#Capasitor
mods.extendedcrafting.TableCrafting.addShaped(0, <thermalexpansion:capacitor:32000>.withTag({Energy: 0}), [
	[<thermaldynamics:duct_0:5>, <avaritia:resource:5>, <ic2:crafting:38>, <ore:ingotInfinity>, <avaritia:cosmic_meatballs>, <ore:ingotInfinity>, <ic2:crafting:38>, <avaritia:resource:5>, <thermaldynamics:duct_0:5>],
	[<avaritia:resource:5>, <avaritia:ultimate_stew>, <ore:blockCrystallinePinkSlime>, <ore:blockCosmicNeutronium>, <ore:blockInfinity>, <ore:blockCosmicNeutronium>, <ore:blockCrystallinePinkSlime>, <avaritia:ultimate_stew>, <avaritia:resource:5>],
	[<ic2:crafting:38>, <ore:blockCrystallinePinkSlime>, <draconicevolution:chaotic_core>, <ore:blockCrystallinePinkSlime>, <extrabotany:blockorichalcos>, <ore:blockCrystallinePinkSlime>, <draconicevolution:chaotic_core>, <ore:blockCrystallinePinkSlime>, <ic2:crafting:38>],
	[<ore:ingotInfinity>, <ore:blockCosmicNeutronium>, <ore:blockCrystallinePinkSlime>, <ore:blockMelodicAlloy>, <ore:circuitUltimate>, <ore:blockMelodicAlloy>, <ore:blockCrystallinePinkSlime>, <ore:blockCosmicNeutronium>, <ore:ingotInfinity>],
	[<avaritia:cosmic_meatballs>, <ore:blockInfinity>, <extrabotany:blockorichalcos>, <ore:circuitUltimate>, <hrm-industrial-plus:gods_sp:6>, <ore:circuitUltimate>, <extrabotany:blockorichalcos>, <ore:blockInfinity>, <avaritia:cosmic_meatballs>],
	[<ore:ingotInfinity>, <ore:blockCosmicNeutronium>, <ore:blockCrystallinePinkSlime>, <ore:blockMelodicAlloy>, <ore:circuitUltimate>, <ore:blockMelodicAlloy>, <ore:blockCrystallinePinkSlime>, <ore:blockCosmicNeutronium>, <ore:ingotInfinity>],
	[<ic2:crafting:38>, <ore:blockCrystallinePinkSlime>, <draconicevolution:chaotic_core>, <ore:blockCrystallinePinkSlime>, <extrabotany:blockorichalcos>, <ore:blockCrystallinePinkSlime>, <draconicevolution:chaotic_core>, <ore:blockCrystallinePinkSlime>, <ic2:crafting:38>],
	[<avaritia:resource:5>, <avaritia:ultimate_stew>, <ore:blockCrystallinePinkSlime>, <ore:blockCosmicNeutronium>, <ore:blockInfinity>, <ore:blockCosmicNeutronium>, <ore:blockCrystallinePinkSlime>, <avaritia:ultimate_stew>, <avaritia:resource:5>],
	[<thermaldynamics:duct_0:5>, <avaritia:resource:5>, <ic2:crafting:38>, <ore:ingotInfinity>, <avaritia:cosmic_meatballs>, <ore:ingotInfinity>, <ic2:crafting:38>, <avaritia:resource:5>, <thermaldynamics:duct_0:5>]
]);

#CreativeMill
mods.extendedcrafting.TableCrafting.addShaped(0, <extrautils2:passivegenerator:6>, [
	[null, <draconicevolution:wyvern_energy_core>, <ore:compressed7xCobblestone>, <ore:compressed7xCobblestone>, <extrautils2:passivegenerator:3>, <ore:compressed7xCobblestone>, <ore:compressed7xCobblestone>, <draconicevolution:wyvern_energy_core>, null],
	[<draconicevolution:wyvern_energy_core>, <extrautils2:passivegenerator:3>, <extrautils2:passivegenerator:2>, <avaritia:resource:5>, <ore:ingotCosmicNeutronium>, <avaritia:resource:5>, <extrautils2:passivegenerator:2>, <extrautils2:passivegenerator:3>, <draconicevolution:wyvern_energy_core>],
	[<ore:compressed7xCobblestone>, <extrautils2:passivegenerator:2>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <bloodmagic:slate:2>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <extrautils2:passivegenerator:2>, <ore:compressed7xCobblestone>],
	[<ore:compressed7xCobblestone>, <avaritia:resource:5>, <ore:nuggetCosmicNeutronium>, <draconicevolution:wyvern_core>, <extrautils2:passivegenerator:8>, <draconicevolution:wyvern_core>, <ore:nuggetCosmicNeutronium>, <avaritia:resource:5>, <ore:compressed7xCobblestone>],
	[<extrautils2:passivegenerator:3>, <ore:ingotCosmicNeutronium>, <bloodmagic:slate:2>, <extrautils2:passivegenerator:8>, <ore:ingotInfinity>, <extrautils2:passivegenerator:8>, <bloodmagic:slate:2>, <ore:ingotCosmicNeutronium>, <extrautils2:passivegenerator:3>],
	[<ore:compressed7xCobblestone>, <avaritia:resource:5>, <ore:nuggetCosmicNeutronium>, <draconicevolution:wyvern_core>, <extrautils2:passivegenerator:8>, <draconicevolution:wyvern_core>, <ore:nuggetCosmicNeutronium>, <avaritia:resource:5>, <ore:compressed7xCobblestone>],
	[<ore:compressed7xCobblestone>, <extrautils2:passivegenerator:2>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <bloodmagic:slate:2>, <ore:nuggetCosmicNeutronium>, <ore:nuggetCosmicNeutronium>, <extrautils2:passivegenerator:2>, <ore:compressed7xCobblestone>],
	[<draconicevolution:wyvern_energy_core>, <extrautils2:passivegenerator:3>, <extrautils2:passivegenerator:2>, <avaritia:resource:5>, <ore:ingotCosmicNeutronium>, <avaritia:resource:5>, <extrautils2:passivegenerator:2>, <extrautils2:passivegenerator:3>, <draconicevolution:wyvern_energy_core>],
	[null, <draconicevolution:wyvern_energy_core>, <ore:compressed7xCobblestone>, <ore:compressed7xCobblestone>, <extrautils2:passivegenerator:3>, <ore:compressed7xCobblestone>, <ore:compressed7xCobblestone>, <draconicevolution:wyvern_energy_core>, null]
]);

#Neutronium Collector
mods.extendedcrafting.TableCrafting.addShaped(0, <avaritia:neutron_collector>, [
	[<ore:blockStellarAlloy>, <ore:blockDraconiumAwakened>, <ore:blockCrystalMatrix>, <ore:blockEnergeticSilver>, <ore:blockCrystalMatrix>, <ore:blockDraconiumAwakened>, <ore:blockStellarAlloy>],
	[<ore:blockDraconiumAwakened>, <thaumictinkerer:kamiresource:2>, <ore:ingotPhotonium>, <astralsorcery:itemcraftingcomponent:4>, <ore:ingotPhotonium>, <bloodarsenal:blood_diamond:3>, <ore:blockDraconiumAwakened>],
	[<ore:blockCrystalMatrix>, <ore:ingotPhotonium>, <extracells:storage.component:3>, <bloodmagic:slate:4>, <extracells:storage.component:3>, <ore:ingotPhotonium>, <ore:blockCrystalMatrix>],
	[<ore:blockVividAlloy>, <astralsorcery:itemcraftingcomponent:4>, <bloodmagic:slate:4>, <draconicevolution:wyvern_core>, <bloodmagic:slate:4>, <astralsorcery:itemcraftingcomponent:4>, <ore:blockVividAlloy>],
	[<ore:blockCrystalMatrix>, <ore:ingotPhotonium>, <extracells:storage.component:3>, <bloodmagic:slate:4>, <extracells:storage.component:3>, <ore:ingotPhotonium>, <ore:blockCrystalMatrix>],
	[<ore:blockDraconiumAwakened>, <bloodarsenal:blood_diamond:3>, <ore:ingotPhotonium>, <astralsorcery:itemcraftingcomponent:4>, <ore:ingotPhotonium>, <thaumictinkerer:kamiresource:2>, <ore:blockDraconiumAwakened>],
	[<ore:blockStellarAlloy>, <ore:blockDraconiumAwakened>, <ore:blockCrystalMatrix>, <ore:blockEnergeticSilver>, <ore:blockCrystalMatrix>, <ore:blockDraconiumAwakened>, <ore:blockStellarAlloy>]
]);

#Compressor
mods.extendedcrafting.TableCrafting.addShaped(0, <extendedcrafting:compressor>, [
	[<ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <ore:blockCrystallineAlloy>, <ore:blockCrystallinePinkSlime>, <ore:blockCrystallinePinkSlime>, <ore:blockCrystallinePinkSlime>, <ore:blockCrystallineAlloy>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>],
	[<ore:blockCrystalMatrix>, <ore:compressed3xDustBedrock>, <avaritia:resource:2>, <avaritia:cosmic_meatballs>, <avaritia:ultimate_stew>, <avaritia:cosmic_meatballs>, <avaritia:resource:2>, <ore:compressed3xDustBedrock>, <ore:blockCrystalMatrix>],
	[<ore:blockCrystallineAlloy>, <avaritia:resource:2>, <ore:blockCrystallineAlloy>, <draconicevolution:wyvern_core>, <mekanism:machineblock:7>, <draconicevolution:wyvern_core>, <ore:blockCrystallineAlloy>, <avaritia:resource:2>, <ore:blockCrystallineAlloy>],
	[<ore:blockCrystallinePinkSlime>, <avaritia:cosmic_meatballs>, <draconicevolution:wyvern_core>, <ore:ingotCosmicNeutronium>, <ic2:crafting:38>, <ore:ingotCosmicNeutronium>, <draconicevolution:wyvern_core>, <avaritia:cosmic_meatballs>, <ore:blockCrystallinePinkSlime>],
	[<ore:blockCrystallinePinkSlime>, <avaritia:ultimate_stew>, <mekanism:machineblock:7>, <ic2:crafting:38>, <draconicevolution:awakened_core>, <ic2:crafting:38>, <mekanism:machineblock:7>, <avaritia:ultimate_stew>, <ore:blockCrystallinePinkSlime>],
	[<ore:blockCrystallinePinkSlime>, <avaritia:cosmic_meatballs>, <draconicevolution:wyvern_core>, <ore:ingotCosmicNeutronium>, <ic2:crafting:38>, <ore:ingotCosmicNeutronium>, <draconicevolution:wyvern_core>, <avaritia:cosmic_meatballs>, <ore:blockCrystallinePinkSlime>],
	[<ore:blockCrystallineAlloy>, <avaritia:resource:2>, <ore:blockCrystallineAlloy>, <draconicevolution:wyvern_core>, <mekanism:machineblock:7>, <draconicevolution:wyvern_core>, <ore:blockCrystallineAlloy>, <avaritia:resource:2>, <ore:blockCrystallineAlloy>],
	[<ore:blockCrystalMatrix>, <ore:compressed3xDustBedrock>, <avaritia:resource:2>, <avaritia:cosmic_meatballs>, <avaritia:ultimate_stew>, <avaritia:cosmic_meatballs>, <avaritia:resource:2>, <ore:compressed3xDustBedrock>, <ore:blockCrystalMatrix>],
	[<ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>, <ore:blockCrystallineAlloy>, <ore:blockCrystallinePinkSlime>, <ore:blockCrystallinePinkSlime>, <ore:blockCrystallinePinkSlime>, <ore:blockCrystallineAlloy>, <ore:blockCrystalMatrix>, <ore:blockCrystalMatrix>]
]);

#Inf Dagger
mods.extendedcrafting.TableCrafting.addShaped(0, <bloodmagic:sacrificial_dagger:1>.withTag({sacrifice: 0 as byte}), [
	[null, null, null, null, null, null, null, <bloodarsenal:blood_diamond:3>, <bloodarsenal:blood_diamond:3>],
	[null, null, null, null, null, null, <bloodarsenal:blood_diamond:3>, <bloodarsenal:slate:4>, <bloodarsenal:blood_diamond:3>],
	[null, null, null, null, null, <bloodarsenal:blood_diamond:3>, <ore:blockBloodInfusedIron>, <bloodarsenal:blood_diamond:3>, null],
	[null, null, null, null, <bloodarsenal:blood_diamond:3>, <bloodarsenal:slate:4>, <bloodarsenal:blood_diamond:3>, null, null],
	[<ore:blockCosmicNeutronium>, null, null, <bloodarsenal:blood_diamond:3>, <ore:blockBloodInfusedIron>, <bloodarsenal:blood_diamond:3>, null, null, null],
	[null, <ore:blockCosmicNeutronium>, <bloodarsenal:blood_diamond:3>, <bloodarsenal:slate:4>, <bloodarsenal:blood_diamond:3>, null, null, null, null],
	[null, <bloodarsenal:glass_sacrificial_dagger>, <thaumcraft:primordial_pearl>.anyDamage(), <bloodarsenal:blood_diamond:3>, null, null, null, null, null],
	[null, <avaritia:resource:5>, <bloodarsenal:glass_sacrificial_dagger>, <ore:blockCosmicNeutronium>, null, null, null, null, null],
	[<bloodarsenal:glass_sacrificial_dagger>, null, null, null, <ore:blockCosmicNeutronium>, null, null, null, null]
]);

#Inf Building Tool
mods.extendedcrafting.TableCrafting.addShaped(0, <extrautils2:itemcreativebuilderswand>, [
	[null, null, null, null, <ore:blockManyullyn>, <ore:blockManyullyn>, <ore:blockManyullyn>],
	[null, null, null, <ore:blockManyullyn>, <extrautils2:itembuilderswand>, <ore:blockStellarAlloy>, <ore:blockManyullyn>],
	[null, null, null, <ore:blockManyullyn>, <extracells:storage.component:3>, <extrautils2:itembuilderswand>, <ore:blockManyullyn>],
	[null, null, null, <ore:ingotCosmicNeutronium>, <ore:blockManyullyn>, <ore:blockManyullyn>, null],
	[null, null, <ore:blockManyullyn>, null, null, null, null],
	[null, <ore:ingotCosmicNeutronium>, null, null, null, null, null],
	[<ore:blockManyullyn>, null, null, null, null, null, null]
]);

#Creative Modificator
mods.extendedcrafting.TableCrafting.addShaped(0, <tconstruct:materials:50>, [
	[null, null, null, null, null, null, null, null, null],
	[null, null, null, null, <tconstruct:materials:13>, null, null, null, null],
	[null, null, <ore:ingotCobalt>, <tconstruct:materials:14>, <tconstruct:materials:18>, <tconstruct:materials:14>, <ore:ingotArdite>, null, null],
	[null, null, <tconstruct:materials:14>, <ore:ingotCobalt>, <tconstruct:materials:18>, <ore:ingotArdite>, <tconstruct:materials:14>, null, null],
	[null, <tconstruct:materials:12>, <tconstruct:materials:1>, <tconstruct:materials:1>, <draconicevolution:draconic_core>, <tconstruct:materials:2>, <tconstruct:materials:2>, <tconstruct:materials:12>, null],
	[null, null, <tconstruct:materials:14>, <ore:ingotKnightslime>, <ore:ingotManyullyn>, <ore:ingotPigiron>, <tconstruct:materials:14>, null, null],
	[null, null, <ore:ingotKnightslime>, <tconstruct:materials:14>, <ore:ingotManyullyn>, <tconstruct:materials:14>, <ore:ingotPigiron>, null, null],
	[null, null, null, null, <tconstruct:materials:13>, null, null, null, null],
	[null, null, null, null, null, null, null, null, null]
]);

#Autoctaft 5x5
mods.extendedcrafting.TableCrafting.addShaped(0, <packagedexcrafting:advanced_crafter>, [
	[<ic2:iridium_reflector>, <ore:blockGold>, <bloodmagic:slate:4>, <ore:blockGold>, <ic2:iridium_reflector>],
	[<ore:blockGold>, <ore:gaiaIngot>, <ore:circuitUltimate>, <ore:gaiaIngot>, <ore:blockGold>],
	[<bloodmagic:slate:4>, <ore:circuitUltimate>, <tconstruct:materials:16>, <ore:circuitUltimate>, <bloodmagic:slate:4>],
	[<ore:blockGold>, <ore:gaiaIngot>, <ore:circuitUltimate>, <ore:gaiaIngot>, <ore:blockGold>],
	[<ic2:iridium_reflector>, <ore:blockGold>, <bloodmagic:slate:4>, <ore:blockGold>, <ic2:iridium_reflector>]
]);

#Autoctaft 7x7
mods.extendedcrafting.TableCrafting.addShaped(0, <packagedexcrafting:elite_crafter>, [
	[<ore:circuitUltimate>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <draconicevolution:wyvern_energy_core>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <ore:circuitUltimate>],
	[<ore:blockStellarAlloy>, <ore:ingotPigiron>, <bloodmagic:slate:4>, <ore:itemRubber>, <bloodmagic:slate:4>, <ore:ingotPigiron>, <ore:blockStellarAlloy>],
	[<ore:blockStellarAlloy>, <bloodmagic:slate:4>, <ore:ingotOrichalcos>, <bloodarsenal:blood_diamond:3>, <ore:ingotOrichalcos>, <bloodmagic:slate:4>, <ore:blockStellarAlloy>],
	[<draconicevolution:wyvern_energy_core>, <ore:itemRubber>, <bloodarsenal:blood_diamond:3>, <appliedenergistics2:controller>, <bloodarsenal:blood_diamond:3>, <ore:itemRubber>, <draconicevolution:wyvern_energy_core>],
	[<ore:blockStellarAlloy>, <bloodmagic:slate:4>, <ore:ingotOrichalcos>, <bloodarsenal:blood_diamond:3>, <ore:ingotOrichalcos>, <bloodmagic:slate:4>, <ore:blockStellarAlloy>],
	[<ore:blockStellarAlloy>, <ore:ingotPigiron>, <bloodmagic:slate:4>, <ore:itemRubber>, <bloodmagic:slate:4>, <ore:ingotPigiron>, <ore:blockStellarAlloy>],
	[<ore:circuitUltimate>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <draconicevolution:wyvern_energy_core>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <ore:circuitUltimate>]
]);

#Autoctaft 9x9
mods.extendedcrafting.TableCrafting.addShaped(0, <packagedexcrafting:ultimate_crafter>, [
	[<draconicevolution:wyvern_core>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <bloodmagic:slate:4>, <draconicevolution:wyvern_energy_core>, <bloodmagic:slate:4>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <draconicevolution:wyvern_core>],
	[<ore:ingotCosmicNeutronium>, <ore:blockStellarAlloy>, <ore:ingotCrystallinePinkSlime>, <ore:ingotCrystallinePinkSlime>, <bloodmagic:slate:4>, <ore:ingotCrystallinePinkSlime>, <ore:ingotCrystallinePinkSlime>, <ore:blockStellarAlloy>, <ore:ingotCosmicNeutronium>],
	[<ore:ingotCosmicNeutronium>, <ore:ingotCrystallinePinkSlime>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <ic2:crafting:38>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <ore:ingotCrystallinePinkSlime>, <ore:ingotCosmicNeutronium>],
	[<bloodmagic:slate:4>, <ore:ingotCrystallinePinkSlime>, <ore:blockStellarAlloy>, <ore:essenceSupremium>, <threng:material:14>, <ore:essenceSupremium>, <ore:blockStellarAlloy>, <ore:ingotCrystallinePinkSlime>, <bloodmagic:slate:4>],
	[<draconicevolution:wyvern_energy_core>, <bloodmagic:slate:4>, <ic2:crafting:38>, <threng:material:14>, <extrabotany:blockorichalcos>, <threng:material:14>, <ic2:crafting:38>, <bloodmagic:slate:4>, <draconicevolution:wyvern_energy_core>],
	[<bloodmagic:slate:4>, <ore:ingotCrystallinePinkSlime>, <ore:blockStellarAlloy>, <ore:essenceSupremium>, <threng:material:14>, <ore:essenceSupremium>, <ore:blockStellarAlloy>, <ore:ingotCrystallinePinkSlime>, <bloodmagic:slate:4>],
	[<ore:ingotCosmicNeutronium>, <ore:ingotCrystallinePinkSlime>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <ic2:crafting:38>, <ore:blockStellarAlloy>, <ore:blockStellarAlloy>, <ore:ingotCrystallinePinkSlime>, <ore:ingotCosmicNeutronium>],
	[<ore:ingotCosmicNeutronium>, <ore:blockStellarAlloy>, <ore:ingotCrystallinePinkSlime>, <ore:ingotCrystallinePinkSlime>, <bloodmagic:slate:4>, <ore:ingotCrystallinePinkSlime>, <ore:ingotCrystallinePinkSlime>, <ore:blockStellarAlloy>, <ore:ingotCosmicNeutronium>],
	[<draconicevolution:wyvern_core>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <bloodmagic:slate:4>, <draconicevolution:wyvern_energy_core>, <bloodmagic:slate:4>, <ore:ingotCosmicNeutronium>, <ore:ingotCosmicNeutronium>, <draconicevolution:wyvern_core>]
]);

#Dense Neutron Collector
mods.extendedcrafting.TableCrafting.addShaped(0, <denseneutroncollectors:compressed_neutron_collector>, [
	[<ore:blockCosmicNeutronium>, <ore:blockDraconiumAwakened>, <avaritia:ultimate_stew>, <extendedcrafting:singularity:7>, <ic2:crafting:38>, <extendedcrafting:singularity:7>, <avaritia:ultimate_stew>, <ore:blockDraconiumAwakened>, <ore:blockCosmicNeutronium>],
	[<ore:blockDraconiumAwakened>, <extracells:storage.component:3>, <ore:blockDraconiumAwakened>, <ore:gaiaIngot>, <botania:storage:1>, <ore:gaiaIngot>, <ore:blockDraconiumAwakened>, <extracells:storage.component:3>, <ore:blockDraconiumAwakened>],
	[<avaritia:ultimate_stew>, <ore:blockDraconiumAwakened>, <appliedenergistics2:material:47>, <ore:ingotCosmicNeutronium>, <avaritia:neutron_collector>, <ore:ingotCosmicNeutronium>, <appliedenergistics2:material:47>, <ore:blockDraconiumAwakened>, <avaritia:ultimate_stew>],
	[<extendedcrafting:singularity:7>, <ore:gaiaIngot>, <avaritia:neutron_collector>, <extendedcrafting:singularity_ultimate>, <draconicevolution:draconic_energy_core>, <extendedcrafting:singularity_ultimate>, <avaritia:neutron_collector>, <ore:gaiaIngot>, <extendedcrafting:singularity:7>],
	[<ic2:crafting:38>, <botania:storage:1>, <ore:nuggetCosmicNeutronium>, <draconicevolution:draconic_energy_core>, <draconicevolution:wyvern_core>, <draconicevolution:draconic_energy_core>, <ore:nuggetCosmicNeutronium>, <botania:storage:1>, <ic2:crafting:38>],
	[<extendedcrafting:singularity:7>, <ore:gaiaIngot>, <avaritia:neutron_collector>, <extendedcrafting:singularity_ultimate>, <draconicevolution:draconic_energy_core>, <extendedcrafting:singularity_ultimate>, <avaritia:neutron_collector>, <ore:gaiaIngot>, <extendedcrafting:singularity:7>],
	[<avaritia:ultimate_stew>, <ore:blockDraconiumAwakened>, <appliedenergistics2:material:47>, <ore:ingotCosmicNeutronium>, <avaritia:neutron_collector>, <ore:ingotCosmicNeutronium>, <appliedenergistics2:material:47>, <ore:blockDraconiumAwakened>, <avaritia:ultimate_stew>],
	[<ore:blockDraconiumAwakened>, <extracells:storage.component:3>, <ore:blockDraconiumAwakened>, <ore:gaiaIngot>, <botania:storage:1>, <ore:gaiaIngot>, <ore:blockDraconiumAwakened>, <extracells:storage.component:3>, <ore:blockDraconiumAwakened>],
	[<ore:blockCosmicNeutronium>, <ore:blockDraconiumAwakened>, <avaritia:ultimate_stew>, <extendedcrafting:singularity:7>, <ic2:crafting:38>, <extendedcrafting:singularity:7>, <avaritia:ultimate_stew>, <ore:blockDraconiumAwakened>, <ore:blockCosmicNeutronium>]
]);

#Denser Neutron Collector
mods.extendedcrafting.TableCrafting.addShaped(0, <denseneutroncollectors:double_compressed_neutron_collector>, [
	[<draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, <ic2:crafting:38>, <advanced_solar_panels:crafting:13>, <tconstruct:materials:50>, <advanced_solar_panels:crafting:13>, <ic2:crafting:38>, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>],
	[<ore:blockDraconiumAwakened>, <extrabotany:blockorichalcos>, <ore:blockCosmicNeutronium>, <thaumictinkerer:ichor_block>, <extracells:storage.component:3>, <thaumictinkerer:ichor_block>, <ore:blockCosmicNeutronium>, <extrabotany:blockorichalcos>, <ore:blockDraconiumAwakened>],
	[<ic2:crafting:38>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, <ore:compressed8xCobblestone>, <denseneutroncollectors:compressed_neutron_collector>, <ore:compressed8xCobblestone>, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <ic2:crafting:38>],
	[<advanced_solar_panels:crafting:13>, <thaumictinkerer:ichor_block>, <denseneutroncollectors:compressed_neutron_collector>, <avaritia:resource:5>, <draconicadditions:chaotic_energy_core>, <avaritia:resource:5>, <denseneutroncollectors:compressed_neutron_collector>, <thaumictinkerer:ichor_block>, <advanced_solar_panels:crafting:13>],
	[<tconstruct:materials:50>, <extracells:storage.component:3>, <ore:compressed6xNetherrack>, <draconicadditions:chaotic_energy_core>, <draconicevolution:awakened_core>, <draconicadditions:chaotic_energy_core>, <ore:compressed6xNetherrack>, <extracells:storage.component:3>, <tconstruct:materials:50>],
	[<advanced_solar_panels:crafting:13>, <thaumictinkerer:ichor_block>, <denseneutroncollectors:compressed_neutron_collector>, <avaritia:resource:5>, <draconicadditions:chaotic_energy_core>, <avaritia:resource:5>, <denseneutroncollectors:compressed_neutron_collector>, <thaumictinkerer:ichor_block>, <advanced_solar_panels:crafting:13>],
	[<ic2:crafting:38>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, <ore:compressed8xCobblestone>, <denseneutroncollectors:compressed_neutron_collector>, <ore:compressed8xCobblestone>, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <ic2:crafting:38>],
	[<ore:blockDraconiumAwakened>, <extrabotany:blockorichalcos>, <ore:blockCosmicNeutronium>, <thaumictinkerer:ichor_block>, <extracells:storage.component:3>, <thaumictinkerer:ichor_block>, <ore:blockCosmicNeutronium>, <extrabotany:blockorichalcos>, <ore:blockDraconiumAwakened>],
	[<draconicevolution:chaos_shard>, <ore:blockDraconiumAwakened>, <ic2:crafting:38>, <advanced_solar_panels:crafting:13>, <tconstruct:materials:50>, <advanced_solar_panels:crafting:13>, <ic2:crafting:38>, <ore:blockDraconiumAwakened>, <draconicevolution:chaos_shard>]
]);

#Densest Neutron Collector
mods.extendedcrafting.TableCrafting.addShaped(0, <denseneutroncollectors:triple_compressed_neutron_collector>, [
	[<ore:ingotOrichalcos>, <ore:compressed3xDustBedrock>, <bloodarsenal:slate:4>, <bloodarsenal:blood_diamond:3>, <bloodarsenal:slate:4>, <bloodarsenal:blood_diamond:3>, <bloodarsenal:slate:4>, <ore:compressed3xDustBedrock>, <ore:ingotOrichalcos>],
	[<ore:compressed3xDustBedrock>, <ore:blockSupremium>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, <ore:blockBloodInfusedIron>, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <ore:blockSupremium>, <ore:compressed3xDustBedrock>],
	[<bloodarsenal:blood_diamond:3>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <ore:blockSupremium>, <denseneutroncollectors:double_compressed_neutron_collector>, <ore:blockSupremium>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <bloodarsenal:blood_diamond:3>],
	[<ore:blockCosmicNeutronium>, <ore:blockBloodInfusedIron>, <denseneutroncollectors:double_compressed_neutron_collector>, <ore:ingotInfinity>, <draconicevolution:chaotic_core>, <ore:ingotInfinity>, <denseneutroncollectors:double_compressed_neutron_collector>, <ore:blockBloodInfusedIron>, <ore:blockCosmicNeutronium>],
	[<draconicevolution:chaos_shard>, <draconicadditions:chaotic_energy_core>, <ore:blockSupremium>, <draconicevolution:chaotic_core>, <appliedenergistics2:creative_energy_cell>, <draconicevolution:chaotic_core>, <ore:blockSupremium>, <draconicadditions:chaotic_energy_core>, <draconicevolution:chaos_shard>],
	[<ore:blockCosmicNeutronium>, <ore:blockBloodInfusedIron>, <denseneutroncollectors:double_compressed_neutron_collector>, <ore:ingotInfinity>, <draconicevolution:chaotic_core>, <ore:ingotInfinity>, <denseneutroncollectors:double_compressed_neutron_collector>, <ore:blockBloodInfusedIron>, <ore:blockCosmicNeutronium>],
	[<bloodarsenal:blood_diamond:3>, <ore:blockCosmicNeutronium>, <avaritia:resource:5>, <ore:blockSupremium>, <denseneutroncollectors:double_compressed_neutron_collector>, <ore:blockSupremium>, <avaritia:resource:5>, <ore:blockCosmicNeutronium>, <bloodarsenal:blood_diamond:3>],
	[<ore:compressed3xDustBedrock>, <ore:blockSupremium>, <ore:blockCosmicNeutronium>, <draconicevolution:chaos_shard>, <ore:blockBloodInfusedIron>, <draconicevolution:chaos_shard>, <ore:blockCosmicNeutronium>, <ore:blockSupremium>, <ore:compressed3xDustBedrock>],
	[<ore:ingotOrichalcos>, <ore:compressed3xDustBedrock>, <bloodarsenal:slate:4>, <bloodarsenal:blood_diamond:3>, <bloodarsenal:slate:4>, <bloodarsenal:blood_diamond:3>, <bloodarsenal:slate:4>, <ore:compressed3xDustBedrock>, <ore:ingotOrichalcos>]
]);

recipes.addShaped(<extrabotany:material:3>, [
	[<avaritia:ultimate_stew>, <ic2:nuclear:10>, <avaritia:ultimate_stew>],
	[<minecraft:totem_of_undying>, <draconicevolution:wyvern_core>, <minecraft:totem_of_undying>],
	[<advanced_solar_panels:crafting:8>, <bloodarsenal:blood_infused_iron_block>, <advanced_solar_panels:crafting:8>]
]);

mods.botania.ElvenTrade.addRecipe([<extrabotany:spiritfuel>], [<extrabotany:nightmarefuel>]);

# Gaia Spirit
mods.botania.RuneAltar.addRecipe(<botania:manaresource:5>*8, [
	<avaritia:resource:3>, <extrabotany:material>, <extrabotany:material>,
	<extrabotany:material>, <botania:manaresource:9>, <botania:manaresource:8>
	], 50000);

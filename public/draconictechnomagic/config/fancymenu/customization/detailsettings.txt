type = menu

customization-meta {
  identifier = net.optifine.gui.GuiDetailSettingsOF
  randomgroup = 1
  renderorder = foreground
  randommode = false
  randomonlyfirsttime = false
}

customization {
  path = config/fancymenu/assets/fon.png
  action = texturizebackground
}

customization {
  keepaspectratio = false
  action = backgroundoptions
}


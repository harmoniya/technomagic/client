type = menu

customization-meta {
  identifier = net.minecraft.client.gui.GuiVideoSettings
  randomgroup = 1
  renderorder = foreground
  randommode = false
  randomonlyfirsttime = false
}

customization {
  path = config/fancymenu/assets/fon.png
  action = texturizebackground
}

customization {
  keepaspectratio = false
  action = backgroundoptions
}

